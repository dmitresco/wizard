window.Sharing = {
    f: function(url) {
        socialGoal('facebook');
        return this._share("https://www.facebook.com/sharer.php?app_id=955357184504374&u=" + (encodeURIComponent(url)));
    },
    t: function(url) {
        socialGoal('twitter');
        return this._share("http://twitter.com/intent/tweet?url=" + (encodeURIComponent(url)));
    },
    v: function(url) {
        socialGoal('vkontakte');
        return this._share('http://vkontakte.ru/share.php?' + ("url=" + (encodeURIComponent(url))));
    },
    _share: function(url, callback) {
        var self;
        self = this;
        this.window = window.open(url, 'Sharing', 'width=534,height=279');
        clearInterval(this.interval);
        return this.interval = setInterval((function() {
            if (self.window.closed) {
                clearInterval(self.interval);
                if (callback) {
                    return callback();
                }
            }
        }), 500);
    }
};

function socialGoal (social) {
    hlf.goal({
        ga: 'hl_wizard_social_btn.click',
        yam: 'hl_wizard_social_btn-click',
        as: 'hl_wizard_social_btn-click'
    }, {social: social});

    woopra.track('hl_wizard_social_btn_click', {
        social: social
    });
};

$(document).ready(function () {
    $('.social a').each (function () {

        var container = $(this);

        window.socialAnimation = function(container) {
            var global_frames, run;

            global_frames = [
                {
                    elem: container.find('.b-share-icon_vkontakte'),
                    method: 'addClass',
                    value: 'animating',
                    delay: 500
                }, {
                    elem: container.find('.b-share-icon_facebook'),
                    method: 'addClass',
                    value: 'animating',
                    delay: 500
                }, {
                    elem: container.find('.b-share-icon_twitter'),
                    method: 'addClass',
                    value: 'animating',
                    delay: 500
                }, {
                    elem: container.find('.b-share-icon_vkontakte'),
                    method: 'removeClass',
                    value: 'animating',
                    delay: 500
                }, {
                    elem: container.find('.b-share-icon_facebook'),
                    method: 'removeClass',
                    value: 'animating',
                    delay: 500
                }, {
                    elem: container.find('.b-share-icon_twitter'),
                    method: 'removeClass',
                    value: 'animating',
                    delay: 500
                }
            ];
            run = function(global_frames) {
                if ($('html').hasClass('no-isdesktop')) return false;
                var frame;
                if (!(global_frames.length > 0)) {
                    return;
                }
                frame = global_frames.shift();
                frame.elem[frame.method](frame.value);
                return setTimeout((function() {
                    return run(global_frames);
                }), frame.delay);
            };
            run(global_frames);
        };
        return setInterval((function() {
            socialAnimation(container);
        }), 4000);
    });
});