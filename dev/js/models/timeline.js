var timeline = Class({
    constructor: function () {
        this.$timelineParent = $('[data-role=time-line]');
        this.init();
    },

    events: function () {
        wizard.e.on ('slideChange', _.bind(function (data) {
            var _this = this;
            _.defer(_.bind(function () {
                var $nextDot = _this.$timelineParent.find('[data-value="' + data.nextSlide + '"]');

                _this.$timelineParent.find('.timeline-item_active').removeClass('timeline-item_active');
                $nextDot.parent().addClass('timeline-item_active');
            }), this);
        }, this));

        $('[data-role=timeline-dot]').on ('click', function() {
            var $this = $(this),
                $value = $this.data('value'),
                $name = $this.data('name');

            wizard.e.trigger ('timelineClick', [{'slideNext': $value}]);

            hlf.goal({
                ga: 'hl_wizard_timeline_link.click',
                yam: 'hl_wizard_timeline_link-click',
                as: 'hl_wizard_timeline_link-click'
            }, {name: $name});

            woopra.track('wizard_timeline_link_click', {
                name: $name
            });

            return false;
        });
    },

    init: function () {
        this.events();
    }
});