var ballon = Class({
    constructor: function () {
        this.$el1 = $('[data-role=boy-ballon1]');
        this.$el2 = $('[data-role=boy-ballon2]');

        this.$el1Text = $('[data-role=ballon-text1]');
        this.$el2Text = $('[data-role=ballon-text2]');
        this.loadingPhrase =
        {
            1: 'По вашим параметрам мы нашли один самый лучший отель.',
            2: 'По вашим параметрам мы нашли два самых лучших отеля. Выбирайте!',
            3: 'По вашим параметрам мы нашли три самых лучших отеля. Выбирайте!'
        };
        this.init();
    },

    events: function () {
        var _this = this;
        wizard.e.on ('slideChange', _.bind(function (data) {
            _.defer(function () {
                _this.insertText(_this.getPhrases(data.nextSlide));
            });
        }, this));

        wizard.e.on ('hotelsAppended', _.bind(function (data) {
            _.defer(_.bind(function () {
                _this.insertText([_this.loadingPhrase[data.cnt]]);
            }), this);
        }, this));

        wizard.e.on ('needProposal', _.bind(function () {
            this.$el1.addClass ('hidden');
            this.$el2.addClass ('hidden');
        }, this));
    },

    init: function () {
        this.events();
    },

    getPhrases: function (num) {
        var phrases1 = [
            'Я подберу отели специально для вашего путешествия',
            'Выберите дату заезда в отель',
            'А теперь — дату выезда',
            'Продолжим',
            '<span>' + window.searchForm.controls.guests.config.adultsTitle(window.searchForm.controls.guests.config.adults) + ', ' +
            window.searchForm.controls.guests.config.childrenTitle(window.searchForm.controls.guests.config.children) + '</span>.<br/> Круто, погнали дальше.',
            'Отлично! ' + (wizard.filter.typeTrip == 1 ? 'Бизнес-трип' : 'Отдых и туризм') + ', записал',
            '',
            'Секундочку. Подбираем отели <br/>по вашим параметрам...'
        ];
        var phrases2 = [
            '',
            '',
            'Мы посчитаем количество ночей',
            'Теперь нужно узнать, сколько человек едет в путешествие',
            'Теперь — вопросы про отель',
            'Нужно уточнить ещё пару деталей',
            'И последний шаг',
            ''
        ];

        return [phrases1[num], phrases2[num]];
    },

    insertText: function (phrases) {
        if (phrases[0]) {
            this.$el1Text.html(phrases[0]);
        }
        if (phrases[1]) {
            this.$el2Text.html(phrases[1]);
        }
    }
});