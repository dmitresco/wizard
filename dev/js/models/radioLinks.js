var radioLinks = Class({
    constructor: function () {
        this.init();
    },

    events: function () {
        $('[data-role=radio-link]').on ('click', function () {

            var $this = $(this),
                $parent = $this.parents('[data-role=radio-links-group]'),
                $state = $this.data('active'),
                $value = $this.data('value');

            if (!$state) {
                wizard.filter.typeTrip = $value;

                $parent.find('[data-role=radio-link]')
                    .removeClass('active')
                    .data('active', 0);

                $this
                    .addClass('active')
                    .data('active', 1);
            } else {
                $this
                    .removeClass('active')
                    .data('active', 0);

                wizard.filter.typeTrip = 0;
            }
            wizard.e.trigger ('radioLink-Click');

            hlf.goal({
                ga: 'hl_wizard_trip_type.choice',
                yam: 'hl_wizard_trip_type-choice',
                as: 'hl_wizard_trip_type-choice'
            },{type: $value == 1 ? 'BusinessTrip' : 'Travel'});

            woopra.track('wizard_trip_type', {
                type: $value == 1 ? 'BusinessTrip' : 'Travel'
            });

            return false;
        });
    },

    init: function () {
        this.events();
    }
});