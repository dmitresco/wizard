var priceRange = Class({
    constructor: function () {
        this.el = document.getElementById('range');
        this.skipValues = [
            document.getElementById('range-min'),
            document.getElementById('range-max')
        ];

        this.init();
    },

    events: function () {
        this.el.noUiSlider.on('update', _.bind(function( values, handle ) {
            this.skipValues[handle].innerHTML = values[handle];
        },this));

        $('[data-role=price-next-btn]').on ('click', _.bind(function() {
            wizard.filter.priceRange[0] = parseInt(this.el.noUiSlider.get()[0].replace(/ /g,''));
            wizard.filter.priceRange[1] = parseInt(this.el.noUiSlider.get()[1].replace(/ /g,''));
            wizard.e.trigger ('priceNextBtn-Click');

            hlf.goal({
                ga: 'hl_wizard_price_range.choice',
                yam: 'hl_wizard_price_range-choice',
                as: 'hl_wizard_price_range-choice'
            },{priceRange: wizard.filter.priceRange});

            woopra.track('wizard_price_range', {
                priceRangeMin: wizard.filter.priceRange[0],
                priceRangeMax: wizard.filter.priceRange[1]
            });

            return false;
        }, this));
    },

    init: function () {
        noUiSlider.create(this.el, {
            connect: true,
            start: [wizard.filter.priceRange[0], wizard.filter.priceRange[1]],
            range: {
                'min': [ wizard.filter.priceRange[0] ],
                '50%': [ Math.ceil((wizard.filter.priceRange[1] - wizard.filter.priceRange[0]) / 25 + wizard.filter.priceRange[0]) ],
                '75%': [ Math.ceil((wizard.filter.priceRange[1] - wizard.filter.priceRange[0]) / 10 + wizard.filter.priceRange[0]) ],
                'max': [ wizard.filter.priceRange[1] ]
            },
            step: 1,
            format: wNumb ({
                decimals: false,
                mark: false,
                thousand: ' ',
                postfix: ' руб.'
            })
        });
        this.events();
    }
});