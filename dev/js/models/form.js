FormSearch = function (context) {

    window.searchForm = hlf.form ({
        id: context,
        controls: {
            destination: hlf.ac ({
                type: 'location',
                locale: 'ru-ru',
                autoFocus: false,
                limit: 4,
                needLocationPhotos: window.isMobile ? false : true,
                locationPhotoSize: wizard.retina ? '240x100' : '120x50',
                onlyLocations: true,
                className: 'page-location ui-front' + (window.isMobile ? ' page-location_mobile' : ''),
                placeholder: 'Название города',
                goalUseInput: {
                    ga: 'hl_wizard_search_form.goalUseInput',
                    yam: 'hl_wizard_search_form-goalUseInput',
                    as: 'hl_wizard_search_form-goalUseInput'
                },
                goalAcSelect: {
                    ga: 'hl_wizard_search_form.goalAcSelect',
                    yam: 'hl_wizard_search_form-goalAcSelect',
                    as: 'hl_wizard_search_form-goalAcSelect'
                },
                goalAcSelectType: {
                    ga: 'hl_wizard_search_form.goalAcSelectCategory',
                    yam: 'hl_wizard_search_form-goalAcSelectCategory',
                    as: 'hl_wizard_search_form-goalAcSelectCategory'
                },
                onSelect: function (data, locationId) {
                    wizard.e.trigger ('acSelect', [locationId]);
                    woopra.track('wizard_ac_select', {
                        locationId: locationId
                    });
                }
            }),

            checkIn: hlf.calendar ({
                required: true,
                name: 'checkIn',
                relationCalendar: 'checkOut',
                relationSuperior: true,
                relationAutoSet: true,
                relationAutoShow: true,
                inline: true,
                locale: 'ru-RU',
                mobileMode: false,
                months: 6,
                min: -1,
                goalSelectDate: {
                    ga: 'hl_wizard_search_form.checkInSelectDate',
                    yam: 'hl_wizard_search_form-checkInSelectDate',
                    as: 'hl_wizard_search_form-checkInSelectDate'
                },
                onSelect: function (date, formatDate) {
                    wizard.e.trigger ('calendarCheckInSelect', [window.searchForm.controls.checkIn.getParams().checkIn]);
                    woopra.track('wizard_checkin_select', {
                        date: formatDate
                    });
                }
            }),

            checkOut: hlf.calendar({
                required: true,
                name: 'checkOut',
                relationCalendar: 'checkIn',
                relationSuperior: false,
                relationAutoSet: true,
                relationAutoShow: true,
                locale: 'ru-RU',
                mobileMode: false,
                months: 6,
                inline: true,
                min: 0,
                goalSelectDate: {
                    ga: 'hl_wizard_search_form.checkOutSelectDate',
                    yam: 'hl_wizard_search_form-checkOutSelectDate',
                    as: 'hl_wizard_search_form-checkOutSelectDate'
                },
                onSelect: function (date, formatDate) {
                    wizard.e.trigger ('calendarCheckOutSelect', [window.searchForm.controls.checkOut.getParams().checkOut]);
                    wizard.request.nights = days(new Date(window.searchForm.controls.checkIn.getParams().checkIn), new Date(window.searchForm.controls.checkOut.getParams().checkOut));
                    woopra.track('wizard_checkout_select', {
                        date: formatDate
                    });
                }
            }),

            guests: hlf.guests({
                adults:  2,
                children: [],
                titlesPosInside: true,
                adultsTitle: function(adults) {
                    var text = adults + ' Взрослый';
                    if (adults > 1) {
                        text = adults + ' Взрослых'
                    }
                    return text;
                },
                childrenTitle: function(children) {
                    var text = 'Без детей';
                    var length = children.length;

                    if (length == 1) {
                        text = length + ' Ребёнок'
                    }
                    if (length > 1) {
                        text = length + ' Детей'
                    }
                    return text;
                },
                summary: function () {
                    return ' ';
                },
                childAge: '',
                childrenListTitle: 'Укажите возраст детей (от 0 до 17 лет)',
                childValSep: window.isMobile ? false : true,
                decControlContent: '<svg class="icon icon_minusSmall"><use xlink:href="#icon_minusSmall"></use></svg>',
                incControlContent: '<svg class="icon icon_plusSmall"><use xlink:href="#icon_plusSmall"></use></svg>',
                decControlContentChld: '<svg class="icon icon_minusSmall"><use xlink:href="#icon_minusSmall"></use></svg>',
                incControlContentChld: '<svg class="icon icon_plusSmall"><use xlink:href="#icon_plusSmall"></use></svg>',
                goalOpen: {
                    ga: 'hl_wizard_search_form.guests',
                    yam: 'hl_wizard_search_form-guests',
                    as: 'hl_wizard_search_form-guests'
                }
            })
        }
    });

};

function days (checkIn, checkOut) {
    return Math.round(Math.abs((checkIn - checkOut)/24*60*60*1000));
};