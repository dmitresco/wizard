var content_slider = Class({

    constructor: function (el) {
        this.el = el;
        this.init();
    },

    events: function () {

        $(window.searchForm.controls.destination.input).on ('focus', function () {
            _.defer(function () {
                $('html,body').scrollTop($('#destination').offset().top - 10);
            })
        });

        // slick event before change slide
        $('.content-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
            wizard.e.trigger ('slideChange', [{nextSlide: nextSlide}]);
            if (nextSlide == 7) {
                wizard.e.trigger ('nextIsFinalSlide');
            }
        });

        this.el.on('afterChange', _.bind(function(event, slick, currentSlide){
            if (currentSlide == 7) {
                wizard.e.trigger ('finalSlideLoaded');
                wizard.finalSlideLoaded = true;
            }

            if (!window.isMobile) {
                $.datepicker.setDefaults($.datepicker.regional['ru-RU']);

                switch (currentSlide - 1) {
                    case 0:
                        this.insertTextToTimline(0, window.searchForm.controls.destination.config.text.split(',')[0]);
                        break;
                    case 1:
                        this.insertTextToTimline(1,
                            $.datepicker.formatDate('d M', new Date(window.searchForm.controls.checkIn.getParams().checkIn)) +
                            '<br/>' +
                            $.datepicker.formatDate('DD', new Date(window.searchForm.controls.checkIn.getParams().checkIn))
                        );
                        break;
                    case 2:
                        this.insertTextToTimline(2,
                            $.datepicker.formatDate('d M', new Date(window.searchForm.controls.checkOut.getParams().checkOut)) +
                            '<br/>' +
                            $.datepicker.formatDate('DD', new Date(window.searchForm.controls.checkOut.getParams().checkOut))
                        );
                        break;
                    case 3:
                        var html = window.searchForm.controls.guests.config.adultsTitle(window.searchForm.controls.guests.config.adults)
                            + '<br/>'
                            + window.searchForm.controls.guests.config.childrenTitle(window.searchForm.controls.guests.config.children);
                        this.insertTextToTimline(3, html);
                        break;
                    case 4:
                        var html = 'Не выбрано';
                        if (wizard.filter.typeTrip == 1) {
                            html = 'Бизнес-трип';
                        } else if (wizard.filter.typeTrip == 2) {
                            html = 'Отдых и туризм';
                        }
                        this.insertTextToTimline(4, html);
                        break;
                    case 5:
                        var text = wizard.filter.amenities.length ? 'Выбрано ' + wizard.filter.amenities.length : 'Не выбраны';
                        this.insertTextToTimline(5, text);
                        break;
                    case 6:
                        var html = 'от ' + wizard.filter.priceRange[0] + ' руб.<br/>до ' + wizard.filter.priceRange[1] + ' руб.';
                        this.insertTextToTimline(6, html);
                        break;
                    case 7:
                        this.insertTextToTimline(7, wizard.filter.typeTrip);
                        break;
                }
            }
        }, this));

        wizard.e.on ('acSelect', _.bind(function (locationId) {
            this.slideNext();

            window.searchForm.controls.destination.input.blur();
            wizard.request.locationId = locationId;
        }, this));

        var _this = this;

        $('[data-role=city-popular]').on ('click', function () {
           var $this = $(this),
               $id = $this.data('id'),
               $text = $this.data('text');

            _this.slideNext();

            window.searchForm.controls.destination.input.val($text);
            window.searchForm.controls.destination.config.id = $id;
            window.searchForm.controls.destination.config.text = $text;
            wizard.request.locationId = $id;

            hlf.goal({
                ga: 'hl_wizard_popular_city.click',
                yam: 'hl_wizard_popular_city-click',
                as: 'hl_wizard_popular_city-click'
            },{id: $id});

            woopra.track('wizard_popular_city', {
                id: $id
            });

            return false;
        });

        wizard.e.on ('calendarCheckInSelect', _.bind(function (date) {
            wizard.request.checkIn = date;
            this.slideNext();

            setTimeout(_.bind(function () {
                $('.hasDatepicker').datepicker('refresh');
            }, this), 400);

        }, this));

        wizard.e.on ('calendarCheckOutSelect', _.bind(function (date) {
            wizard.request.checkOut = date;
            this.slideNext();

            setTimeout(_.bind(function () {
                $('.hasDatepicker').datepicker('refresh');
            }, this), 400);
        }, this));

        wizard.e.on ('timelineClick', _.bind(function (data) {
            this.slideGoTo(data.slideNext);
        }, this));

        wizard.e.on ('radioLink-Click', _.bind(function () {
            this.slideNext();
        }, this));

        wizard.e.on ('priceNextBtn-Click', _.bind(function () {
            this.slideNext();
        }, this));

        $('[data-role=guests-next-btn]').on ('click', _.bind(function() {
            wizard.request.adults = window.searchForm.controls.guests.config.adults;
            wizard.request.children = window.searchForm.controls.guests.config.children;
            this.slideNext();

            wizard.e.trigger ('guestsSelect');

            hlf.goal({
                ga: 'hl_wizard_guests_btn.click',
                yam: 'hl_wizard_guests_btn-click',
                as: 'hl_wizard_guests_btn-click'
            });

            woopra.track('hl_wizard_guests_btn_click');

            return false;
        }, this));

        $('[data-role=type-next-btn]').on ('click', _.bind(function() {
            this.slideNext();
            return false;
        }, this));

        $('[data-role=amenities-next-btn]').on ('click', _.bind(function() {
            this.slideNext();

            hlf.goal({
                ga: 'hl_wizard_amenities.choice',
                yam: 'hl_wizard_amenities-choice',
                as: 'hl_wizard_amenities-choice'
            },{amenities: wizard.filter.amenities.length ? wizard.filter.amenities : 0});

            woopra.track('wizard_amenities', {
                amenities: wizard.filter.amenities.length ? wizard.filter.amenities : 0
            });

            return false;
        }, this));

        $('[data-role=reload-btn]').on ('click', _.bind(function() {
            var $place = $(this).data('place');
            location.reload();

            hlf.goal({
                ga: 'hl_wizard_reload_btn.click',
                yam: 'hl_wizard_reload_btn-click',
                as: 'hl_wizard_reload_btn-click'
            }, {place: $place});

            woopra.track('wizard_reload_btn', {
                place: $place
            });

            return false;
        }, this));
    },

    insertTextToTimline: function (num, html) {
        $('[data-value=' + num + ']')
            .addClass ('passed')
            .find ('[data-role=tl-text]').html (html);

        setTimeout(function () {
            $('[data-value=' + num + ']').addClass ('textShow');
        }, 300);
    },

    init: function () {
        this.events();

        var slick_element = this.el;

        _.defer (function () {
            slick_element.slick({
                dots: false,
                accessibility: false,
                infinite: false,
                speed: 400,
                mobileFirst: false,
                swipe: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                touchThreshold: 8,
                cssEase: 'ease-in-out',
                touchMove: false,
                waitForAnimate: false,
                fade: false,
                arrows: false
            });
        });

    },

    slideNext: function () {
        this.el.slick('slickNext');
    },

    slideGoTo: function (slideNext) {
        this.el.slick('slickGoTo', slideNext);
    }
});