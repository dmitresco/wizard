window.wizard = new function () {
    this.retina = window.devicePixelRatio > 1 ? true : false;
    this.init = function () {
        this.e = new EventEmitter();
    };
    this.currentSlide = 0;
    this.request = {
        locationId: 0,
        checkIn: '',
        checkOut: '',
        adults: 2,
        children: 0,
        nights: 0
    };
    this.filter = {
        typeTrip: 0,
        amenities: [],
        priceRange:
            {
                0: 700,
                1: 200000
            },
        droped: {
            amenitiesDroped: [],
            priceDropedMax: 0,
            priceDropedMin: 0
        }
    };
    this.slideLocked = true;
    this.hotels = {};
    this.finalSlideLoaded = false;
};
