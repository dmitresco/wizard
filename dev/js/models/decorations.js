var decorations = Class({

    constructor: function () {
        this.init();
        this.elPageWrap = $('[data-role=page-wrap]');
    },

    events: function () {
        wizard.e.on ('slideChange', _.bind(function (data) {
            this.bodyClassChange(data.nextSlide);
            if (data.nextSlide != 7) {
                this.wrapClassRemove('page-wrap_hotels-compact page-wrap_boy-hidden page-wrap_hotels-expand');
            }
        }, this));

        wizard.e.on ('hotelsAppended', _.bind(function () {

            this.wrapClassAdd('page-wrap_hotels-compact');

            setTimeout(_.bind(function () {
                this.wrapClassAdd('page-wrap_boy-hidden');
            }, this), 150);

            var _this = this;

            $('[data-role=hotel]').bind ('click', function () {
                var $this = $(this);
                if ($this.data('index') != 2) {
                    $('[data-role=hotels]').prepend ($this);
                }
                _this.wrapClassRemove('page-wrap_hotels-compact page-wrap_boy-hidden');
                _this.wrapClassAdd('page-wrap_hotels-expand');

                _this.photoSlideInit();

                $('[data-role=hotel]').unbind ('click');

                hlf.goal({
                    ga: 'hl_wizard_hotel_expand.click',
                    yam: 'hl_wizard_hotel_expand-click',
                    as: 'hl_wizard_hotel_expand-click'
                }, {type: $this.data('type')});

                woopra.track('wizard_hotel_expand', {
                    type: $this.data('type')
                });


                $('[data-role=book-btn]').on ('click', _.bind(function() {
                    hlf.goal({
                        ga: 'hl_wizard_book_btn.click',
                        yam: 'hl_wizard_book_btn-click',
                        as: 'hl_wizard_book_btn-click'
                    }, {type: $this.data('type')});

                    woopra.track('wizard_book_btn', {
                        type: $this.data('type')
                    });

                    return true;
                }, this));

                $('[data-role=map-link]').on ('click', _.bind(function() {
                    hlf.goal({
                        ga: 'hl_wizard_map.click',
                        yam: 'hl_wizard_map-click',
                        as: 'hl_wizard_map-click'
                    });

                    woopra.track('hl_wizard_map_click');

                    return true;
                }, this));

                return false;
            });
        }, this));
    },

    photoSlideInit: function () {
        $('[data-role=photo-list]').each (function () {
            var $this = $(this);

            $this.owlCarousel ({
                items: 1,
                loop: true,
                nav: true,
                navContainer: $this,
                navText: [
                    '<svg class="icon icon_chevron"><use xlink:href="#icon_chevronL"></use></svg>',
                    '<svg class="icon icon_chevron"><use xlink:href="#icon_chevronR"></use></svg>'
                ],
                navClass: [
                    'hotel-photo-list-nav hotel-photo-list-nav_prev',
                    'hotel-photo-list-nav hotel-photo-list-nav_next'
                ],
                dots: false
            });
            $this.on('changed.owl.carousel', function() {
                hlf.goal({
                    ga: 'hl_wizard_hotel_photos.change',
                    yam: 'hl_wizard_hotel_photos-change',
                    as: 'hl_wizard_hotel_photos-change'
                });

                woopra.track('wizard_hotel_photos_change');
            });
        });
    },

    init: function () {
        this.events();
    },

    bodyClassChange: function (nextSlide) {
        $('body')
            .attr('class','')
            .addClass('body_' + nextSlide);
    },

    wrapClassRemove: function (classes) {
        this.elPageWrap.removeClass(classes);
    },

    wrapClassAdd: function (classes) {
        this.elPageWrap.addClass(classes);
    }
});