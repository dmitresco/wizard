var searchInit = Class({
    constructor: function () {
        this.tpls = {
            'hotel': 'wizard-hotel'
        };
        this.overlayEl = $('[data-role=overlay]');
        this.wrapperEl = $('[data-role=wrapper]');
        this.popupEl = $('[data-role=popup]');
        this.init();
    },

    events: function () {
        wizard.e.on ('nextIsFinalSlide', _.bind(function () {
            $('[data-place=hotels]').html('');
            this.searchRequest();
            woopra.track('wizard_search_init', {
                request: wizard.request
            });
        }, this));

        $('[data-role=popup-close]').on ('click', _.bind(function() {
            this.popupHide('proposal-btn');
        }, this));
    },

    init: function () {
        this.events();
    },

    searchRequest: function () {
        window.time = Math.floor(Date.now() / 1000);
        $.ajax({
            url: 'https://yasen.hotellook.com/adaptors/s_search.json?singleHotel=1',
            data: {
                locationId: wizard.request.locationId,
                checkIn: wizard.request.checkIn,
                checkOut: wizard.request.checkOut,
                adults: wizard.request.adults,
                children: wizard.request.children,
                currency: 'rub',
                async: ''
            },
            dataType: 'jsonp',
            success: _.bind(function(data) {
                if (data) {
                    var time = Math.floor(Date.now() / 1000) - window.time;
                    var sortedByPrice = _.clone(this.sortByPrice(data), true);
                    this.loadHotels(_.keys(sortedByPrice), sortedByPrice);

                    hlf.goal({
                        ga: 'hl_wizard_rooms.loadTime',
                        yam: 'hl_wizard_rooms-loadTime',
                        as: 'hl_wizard_rooms-loadTime'
                    }, {timeSec: time});

                    woopra.track('wizard_rooms_loadTime', {
                        timeSec: time
                    });
                } else {
                    this.popup();
                }
            }, this),
            error: _.bind(function () {
                this.popup();
            }, this)
        });
    },

    loadHotels: function (ids, rooms) {
        window.time2 = Math.floor(Date.now() / 1000);
        $.ajax({
            url: 'https://yasen.hotellook.com/wizard/hotels.json',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify({
                ids: ids,
                currency: 'rub',
                lang: 'ru'
            }),
            success: _.bind(function(data) {
                if (_.size(data)) {
                    var time2 = Math.floor(Date.now() / 1000) - window.time2;
                    var hotels = _.clone(data, true);

                    //do some things with hotels
                    this.hotelModify (hotels, rooms);
                } else {
                    this.popup();
                }
            }, this),
            error: _.bind(function () {
                this.popup();
            }, this)
        });
    },

    hotelModify: function (hotels, rooms) {
        _.each(hotels, function(hotelObj) {
            if (rooms[hotelObj.id]) {
                var room = rooms[hotelObj.id][0];

                //add price of best room to hotel object
                hotelObj.price = Math.round(room.price);

                //add room id to hotel object
                hotelObj.roomId = room.roomId;

                //add gate id to hotel object
                hotelObj.gateId = room.gateId;

                if (_.isUndefined(hotelObj.amenitiesV2.Hotel)) {
                    hotelObj.amenitiesV2.Hotel = {};
                }

                //add breakfast amenity if it's in relative best room
                if (_.size(room.options) && room.options.breakfast) {
                    hotelObj.amenitiesV2.Hotel[_.size(hotelObj.amenitiesV2.Hotel)] = {
                        "name": "Breakfast"
                    }
                }
            }
        });
        this.runFilter(hotels);
    },

    runFilter: function (hotels) {
        this.filter(hotels);
    },

    filter: function (hotels) {
        var hotelsFilteredPrice = [],
            hotelsFilteredAmenities = [],
            hotelsFilteredStars = [];

        //filter by price
        _.each(hotels, function(hotelObj, key) {
            if (hotelObj.price >= wizard.filter.priceRange[0] && hotelObj.price <= wizard.filter.priceRange[1]) {
                hotelsFilteredPrice.push(key);
            }
        });

        //filter by amenities if they are
        if (wizard.filter.amenities.length) {
            var passFlag = {};
            _.each(hotels, function(hotelObj, key) {
                passFlag[key] = 0;

                if (!_.isUndefined(hotelObj.amenitiesV2) && !_.isUndefined(hotelObj.amenitiesV2.Hotel) && _.size(hotelObj.amenitiesV2.Hotel)) {
                    _.each(wizard.filter.amenities, function(amenity) {
                        _.each(hotelObj.amenitiesV2.Hotel, function(hotel_amenity) {
                            if (amenity == _.capitalize(_.words(hotel_amenity.name)[0])) {
                                passFlag[key]++;
                            }
                        });
                    });
                    if (passFlag[key] && passFlag[key] == wizard.filter.amenities.length) {
                        hotelsFilteredAmenities.push(key);
                    }
                }
            });
        } else {
            hotelsFilteredAmenities = _.keys(hotels);
        }

        //filter by stars >= 2
        _.each(hotels, function(hotelObj, key) {
            if (hotelObj.stars && hotelObj.stars >= 2) {
                hotelsFilteredStars.push(key);
            }
        });

        this.intersection(hotels, hotelsFilteredPrice,hotelsFilteredAmenities,hotelsFilteredStars);
    },

    intersection: function (hotels, price, amenities, stars) {
        var intersection = _.intersection(price, amenities, stars),
            mostHotels = {},
            mostHotelsIds = [];

        if (!intersection.length) {
            var max = 200000,
                min = 700;

            if (wizard.filter.amenities.length) {

                wizard.filter.droped.amenitiesDroped.push(_.last(wizard.filter.amenities));

                wizard.filter.amenities.pop();
                this.runFilter(hotels);
                return false;
            }

            if (wizard.filter.priceRange[1] < max) {

                var STEP = 400;

                wizard.filter.droped.priceDropedMax = wizard.filter.droped.priceDropedMax + STEP;

                wizard.filter.priceRange[1] = wizard.filter.priceRange[1] + STEP;
                this.runFilter(hotels);
                return false;
            }

            if (wizard.filter.priceRange[0] > min) {
                var STEP = (wizard.filter.priceRange[0] - 200) < 0 ? 0 : 200;

                wizard.filter.droped.priceDropedMin = wizard.filter.droped.priceDropedMin - STEP;

                wizard.filter.priceRange[0] = wizard.filter.priceRange[0] - STEP;
                this.runFilter(hotels);
                return false;
            }

            this.popup();
        } else {

            mostHotels[0] = hotels[this.getCheapestHotel(hotels, intersection)] || {}; //cheapest
            if (intersection.length > 1) {
                mostHotels[1] = hotels[this.getHighRatingHotel(hotels, intersection, mostHotels[0])] || {}; //highRatingHotel
            }
            if (intersection.length > 2) {
                mostHotels[2] = hotels[this.getBestHotel(hotels, intersection, mostHotels[0], mostHotels[1])] || {}; //bestHotel
            }

            _.each(mostHotels, function (hotel) {
                mostHotelsIds.push(hotel.id);
            });

            $.ajax({
                url: 'https://yasen.hotellook.com/content/hotels.json?ids=' + mostHotelsIds + '&currency=rub&lang=ru',
                dataType: 'jsonp',
                jsonpCallback: 'loadHotels_success_callback',
                cache: true,
                async: true,
                success: _.bind(function (data) {
                    if (_.size(data)) {
                        var mostHotelsDescr = _.clone(data);
                        if (wizard.filter.droped.amenitiesDroped.length || wizard.filter.droped.priceDropedMax || wizard.filter.droped.priceDropedMin) {
                            this.popup(true);
                            wizard.e.trigger ('needProposal');
                        } else {
                            hlf.goal({
                                ga: 'hl_wizard_hotels.found',
                                yam: 'hl_wizard_hotels-found',
                                as: 'hl_wizard_hotels-found'
                            });

                            woopra.track('wizard_hotels_found');
                        }
                        this.drawHotels(mostHotels, mostHotelsDescr);
                    } else {
                        this.popup();
                    }
                }, this),
                error: _.bind(function () {
                    console.log('error');
                    this.popup();
                }, this)
            });
        }
    },

    popup: function (filterDroped) {
        var html = '<p>К сожалению, мы не нашли свободных номеров, <br/>по вашим параметрам.</p>';

        if (filterDroped) {

            $('[data-role=popup]').addClass('popup_proposal');

            html += '<div>Но мы сможем вам кое-что предложить, если ';

            if (wizard.filter.droped.priceDropedMax || wizard.filter.droped.priceDropedMin) {
                html += 'увеличим ценовой диапазон <br/>на <b>' + (wizard.filter.droped.priceDropedMax + wizard.filter.droped.priceDropedMin) + ' рублей </b>';
            }

            if ((wizard.filter.droped.priceDropedMax || wizard.filter.droped.priceDropedMin) && wizard.filter.droped.amenitiesDroped.length) {
                html += ' и ';
            }

            if (wizard.filter.droped.amenitiesDroped.length) {
                html += 'уберём следующие услуги: <br/><div class="popup-amenities"><ul class="amenities-list amenities-list_compact">';

                _.each(wizard.filter.droped.amenitiesDroped, function (amenity) {
                    html += '<li class="amenities-list-item"><svg class="icon icon_am icon_am' + amenity + '"><use xlink:href="#icon_am' + amenity + '"></use></svg></li>';
                });

                html += '</ul></div></div>';
            }

            hlf.goal({
                ga: 'hl_wizard_hotels.proposal',
                yam: 'hl_wizard_hotels-proposal',
                as: 'hl_wizard_hotels-proposal'
            });

            woopra.track('wizard_hotels_proposal');
        } else {
            hlf.goal({
                ga: 'hl_wizard_hotels.notFound',
                yam: 'hl_wizard_hotels-notFound',
                as: 'hl_wizard_hotels-notFound'
            });

            woopra.track('wizard_hotels_notFound');
        }

        $('[data-role=popup]').prepend (html);

        this.popupShow ();
    },

    popupShow: function () {
        this.wrapperEl.addClass ('blured');
        this.popupEl.show();
        this.overlayEl.show();

        this.overlayEl.on ('click', _.bind (function () {
            this.popupHide('overlay');
        }, this));

    },

    popupHide: function (place) {
        this.wrapperEl.removeClass ('blured');
        this.popupEl.hide();
        this.overlayEl.hide();

        hlf.goal({
            ga: 'hl_wizard_popup.hide',
            yam: 'hl_wizard_popup-hide',
            as: 'hl_wizard_popup-hide'
        }, {place: place});

        woopra.track('wizard_popup_hide', {
            place: place
        });
    },

    getCheapestHotel: function (hotels, intersection) {
        var ids = _.clone(intersection, true),
            mostHotelId = ids[0];

        _.each (ids, function(id) {
            if (hotels[id].price < hotels[mostHotelId].price) {
                mostHotelId = id;
            } else if (hotels[id].price == hotels[mostHotelId].price) {
                if (hotels[id].popularity > hotels[mostHotelId].popularity) {
                    mostHotelId = id;
                }
            }
        });

        return mostHotelId;
    },

    getHighRatingHotel: function (hotels, intersection, cheapestHotel) {
        //todo: replace stars with raiting
        var ids = _.clone(intersection, true),
            mostHotelId = ids[0];

        if (mostHotelId == cheapestHotel.id) {
            mostHotelId = ids[1];
        }

        _.each (ids, function(id) {
            if (hotels[id].stars && id != cheapestHotel.id) {
                if (hotels[id].stars > hotels[mostHotelId].stars) {
                    mostHotelId = id;
                } else if (hotels[id].stars == hotels[mostHotelId].stars) {
                    if (hotels[id].price > hotels[mostHotelId].price) {
                        mostHotelId = id;
                    }
                }
            }
        });

        return mostHotelId;
    },

    getBestHotel: function (hotels, intersection, cheapestHotel, highRatingHotel) {
        var ids = _.clone(intersection, true),
            mostHotelId = ids[0];

        if (mostHotelId == cheapestHotel.id) {
            mostHotelId = ids[1];
        }

        if (mostHotelId == highRatingHotel.id) {
            mostHotelId = ids[2];
        }

        _.each (ids, function(id) {
            if (hotels[id].popularity && id != cheapestHotel.id && id != highRatingHotel.id) {
                if (hotels[id].popularity > hotels[mostHotelId].popularity) {
                    mostHotelId = id;
                } else if (hotels[id].popularity == hotels[mostHotelId].popularity) {
                    if (hotels[id].price > hotels[mostHotelId].price) {
                        mostHotelId = id;
                    }
                }
            }
        });

        return mostHotelId;
    },

    drawHotels: function (mostHotels, mostHotelsDescr) {
        var hotels = {},
            data = {},
            name = {},
            description = {},
            amenities_hotel = {},
            amenities_room = {},
            hotels_html = '';

        for (var i = _.size(mostHotels) - 1; i >= 0 ; i--) {
            if (_.size(mostHotels[i].amenitiesV2.Hotel)) {
                amenities_hotel[i] = _.clone(mostHotels[i].amenitiesV2.Hotel, true);

                _.each(amenities_hotel[i], function(amenity) {
                    amenity.short = _.capitalize(_.words(amenity.name)[0]);
                });
            }

            if (_.size(mostHotels[i].amenitiesV2.Room)) {
                amenities_room[i] = _.clone(mostHotels[i].amenitiesV2.Room, true);

                _.each(amenities_room[i], function(amenity) {
                    amenity.short = _.capitalize(_.words(amenity.name)[0]);
                });
            }

            var hotelTypeText = '',
                hotelType = '';

            switch (i) {
                case 0:
                    hotelTypeText = 'Самый дешёвый за';
                    hotelType = 'cheapest';
                    break;
                case 1:
                    hotelTypeText = 'Самый лучший за';
                    hotelType = 'best';
                    break;
                case 2:
                    hotelTypeText = 'С высоким рейтингом за';
                    hotelType = 'highRating';
                    break;
                default:
                    hotelTypeText = 'Самый дешёвый за';
                    hotelType = 'cheapest';
            }

            if (mostHotels[i].localized) {
                if (mostHotels[i].localized['ru'] && mostHotels[i].localized['ru'].name) {
                    name[i] = mostHotels[i].localized['ru'].name;
                } else if (mostHotels[i].localized['en'] && mostHotels[i].localized['en'].name) {
                    name[i] = mostHotels[i].localized['en'].name;
                } else {
                    name[i] = '';
                }
            } else {
                name[i] = '';
            }

            if (mostHotelsDescr[mostHotels[i].id].hotel.localized) {
                if (mostHotelsDescr[mostHotels[i].id].hotel.localized['ru'] && mostHotelsDescr[mostHotels[i].id].hotel.localized['ru'].description) {
                    description[i] = mostHotelsDescr[mostHotels[i].id].hotel.localized['ru'].description;
                } else if (mostHotelsDescr[mostHotels[i].id].hotel.localized['en'] && mostHotelsDescr[mostHotels[i].id].hotel.localized['en'].description) {
                    description[i] = mostHotelsDescr[mostHotels[i].id].hotel.localized['en'].description;
                } else {
                    description[i] = '';
                }
            } else {
                description[i] = '';
            }

            var imgW = window.isMobile ? 600 : 1200,
                imgH = window.isMobile ? 360 : 520,
                lat = mostHotels[i].location.lat ? mostHotels[i].location.lat : 0,
                lon = mostHotels[i].location.lon ? mostHotels[i].location.lon : 0,
                mapLink = 'https://maps.google.com/?q=' + lat + ',' + lon;

            if (device.ios() || device.android()) {
                mapLink = 'geo:' + lat + ',' + lon;
            }

            data[i] = {
                'index': i,
                'hotelTypeText': hotelTypeText,
                'hotelType': hotelType,
                'description': description[i],
                'minPrice': (mostHotels[i].price + '').replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1 "),
                'stars': mostHotels[i].stars ? mostHotels[i].stars : 0,
                'name': name[i],
                'lat': lat,
                'lon': lon,
                'mapLink': mapLink,
                'rating': mostHotels[i].rating ? mostHotels[i].rating / 10 : false,
                'id': mostHotels[i].id,
                'imgW': imgW,
                'imgH': imgH,
                'photoList': this.renderPhotosList(mostHotels[i].id,mostHotels[i].photoCount > 7 ? 7 : mostHotels[i].photoCount, imgW, imgH),
                'mapW': window.isMobile ? 300 : 600,
                'mapH': window.isMobile ? 105 : 150,
                'distance': mostHotels[i].distance ? Math.round(mostHotels[i].distance * 10) / 10 : 0,
                'searchLink': 'https://search.hotellook.com/r?locationId=' + wizard.request.locationId +
                                '&hotelId=' + mostHotels[i].id +
                                '&gateId=' + mostHotels[i].gateId +
                                '&roomId=' + mostHotels[i].roomId +
                                '&checkIn=' + wizard.request.checkIn +
                                '&checkOut=' + wizard.request.checkOut +
                                '&adults=' + wizard.request.adults +
                                '&children=' + wizard.request.children +
                                '&source=' + 'wizard_hotel_page',
                'checkInHour': mostHotels[i].checkIn ? mostHotels[i].checkIn : '',
                'checkOutHour': mostHotels[i].checkOut ? mostHotels[i].checkOut : '',
                'tyReview': (mostHotels[i].trustyou && mostHotels[i].trustyou.hotelSummaryPrimary && mostHotels[i].trustyou.hotelSummaryPrimary.text) ? mostHotels[i].trustyou.hotelSummaryPrimary.text : '',
                'hotel_amenities': _.size(amenities_hotel[i]) ? amenities_hotel[i] : false,
                'room_amenities': _.size(amenities_room[i]) ? amenities_room[i] : false
            };
            hotels[i] = window.isMobile ? _.template($('#tpl_hotel').html()) : _.template($('#tpl_hotel_desktop').html());
            hotels_html += hotels[i](data[i]);
        }

        $('[data-place=hotels]').html(hotels_html);
        wizard.e.trigger ('hotelsAppended', [{cnt:_.size(mostHotels)}]);
    },

    renderPhotosList: function (hotelId, photoCount,imgW, imgH) {
        var photos = [];
        for (var i = 1; i < photoCount; i++) {
            photos.push({
                'i': i,
                'src': 'https://photo.hotellook.com/image_v2/sprite/h' + hotelId + '/' + imgW + 'x' + imgH + '/' + photoCount + '/' + imgW + 'x' + imgH + '.auto'
            });
        }
        return photos;
    },

    sortByPrice: function (rooms) {
        _.each(rooms, function(value) {
            value.sort(function (a, b) {
                var r = a.price - b.price;
                return r;
            });
        });

        return rooms;
    }
});

function loadHotels_success_callback() {}