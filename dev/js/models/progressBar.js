var progressBar = Class({
    constructor: function () {
        this.$el = $('[data-role=progressBar]');
        this.init();
    },

    events: function () {
        var _this = this;
        wizard.e.on ('slideChange', _.bind(function (data) {
            _.defer(_.bind(function () {
                _this.$el
                    .attr('class','')
                    .addClass('progressBar progressBar_' + data.nextSlide);
            }), this);
        }, this));
    },

    init: function () {
        this.events();
    }
});