var ballonMob = Class({
    constructor: function () {
        this.$el1 = $('[data-role=boy-ballon1]');
        this.$el1Text = $('[data-role=ballon-text1]');

        this.phrases = [
            'Куда хотите поехать?',
            'Выберите дату заезда',
            'А теперь — дату выезда',
            'Супер. Сколько ждать гостей?',
            'Выберите тип своего путешествия',
            'Что обязательно должно быть в отеле?',
            'И последний шаг — какой у вас бюджет?',
            'Секундочку. Подбираем отели <br/>по вашим параметрам...'
        ];
        this.loadingPhrase =
        {
            1: 'По вашим параметрам мы нашли один самый лучший отель.',
            2: 'По вашим параметрам мы нашли два самых лучших отеля. Выбирайте!',
            3: 'По вашим параметрам мы нашли три самых лучших отеля. Выбирайте!'
        };
        this.init();
    },

    events: function () {
        var _this = this;
        wizard.e.on ('slideChange', _.bind(function (data) {
            _.defer(function () {
                _this.insertText(_this.phrases[data.nextSlide]);
            });
        }, this));

        wizard.e.on ('hotelsAppended', _.bind(function (data) {
            _.defer(_.bind(function () {
                _this.insertText(_this.loadingPhrase[data.cnt]);
            }), this);
        }, this));

        wizard.e.on ('needProposal', _.bind(function () {
            this.$el1.addClass ('hidden');
        }, this));
    },

    init: function () {
        this.events();
    },

    insertText: function (text) {
        this.$el1Text.html(text);
    }
});