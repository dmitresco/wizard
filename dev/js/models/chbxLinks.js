var chbxLinks = Class({
    constructor: function () {
        this.init();
    },

    events: function () {
        $('[data-role=chbx-link]').on ('click', function () {

            var $this = $(this),
                $name = $this.data('name');

            if (_.includes(wizard.filter.amenities, $name)) {
                $this
                    .removeClass('active')
                    .data('active', 0);

                var index = wizard.filter.amenities.indexOf($name);
                if (index > -1) {
                    wizard.filter.amenities.splice(index, 1);
                }
            } else {
                $this
                    .addClass('active')
                    .data('active', 1);

                wizard.filter.amenities.push($name);
            }
            return false;
        });
    },

    init: function () {
        this.events();
    }
});