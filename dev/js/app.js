$(document).ready(function () {
    $(function() {
        FastClick.attach(document.body);
    });

    window.isMobile = !device.desktop();
    wizard.init();

    FormSearch('search-form');

    new content_slider($('.content-slider'));
    new timeline();
    new progressBar();

    if (window.isMobile) {
        new ballonMob();
    } else {
        new ballon();
    }

    new radioLinks();
    new chbxLinks();
    new priceRange();
    new searchInit();
    new decorations();
});