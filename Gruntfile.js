/*global module:false*/

module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        htmlbuild: {
            dist: {
                src: 'dev/*.html',
                dest: 'production/',
                options: {
                    beautify: true,
                    sections: {
                        layout: {
                            boy: 'dev/html-parts/boy.html',
                            timeline: 'dev/html-parts/timeline.html',
                            social: 'dev/html-parts/social.html',
                            page1: 'dev/html-parts/page1.html',
                            page2: 'dev/html-parts/page2.html',
                            page3: 'dev/html-parts/page3.html',
                            page4: 'dev/html-parts/page4.html',
                            page5: 'dev/html-parts/page5.html',
                            page6: 'dev/html-parts/page6.html',
                            page7: 'dev/html-parts/page7.html',
                            page8: 'dev/html-parts/page8.html',
                            head: 'dev/html-parts/head.html',
                            scripts: 'dev/html-parts/scripts.html',
                            svgs: 'dev/html-parts/svgs.html',
                            svg_hidden: 'production/img/icons/sprite-demo.html',
                            tpl_hotel: 'dev/html-parts/views/hotel.html',
                            tpl_hotel_desktop: 'dev/html-parts/views/hotelDesktop.html'
                        }
                    }
                }
            }
        },

        sass: {
            dist: {
                options: {
                    style: 'compressed'
                },
                files: {
                    'production/css/app.css': 'dev/scss/app.scss'
                }
            }
        },

        concat: {
            js: {
                src: [
                    //'dev/js/libs/*.js',
                    'dev/js/libs/fastclick.js',
                    'dev/js/libs/EventEmitter.js',
                    'dev/js/libs/jsface.js',
                    'dev/js/libs/device.js',
                    'dev/js/libs/hlf.js',
                    'dev/js/libs/nouislider.min.js',
                    'dev/js/libs/wNumb.min.js',
                    'dev/js/libs/owl.carousel.min.js',
                    'dev/js/models/*.js',
                    'dev/js/app.js'
                ],
                dest: 'production/js/compiled/app.compiled.js'
            },
            css: {
                src: [
                    'production/css/libs/**/*.css',
                    'production/css/app.css'
                ],
                dest: 'production/css/compiled/app.compiled.css'
            }
        },

        autoprefixer: {
            css: {
                expand: true,
                flatten: true,
                src: 'production/css/*.css',
                dest: 'production/css/'
            }
        },

        //uglify: {
        //    all: {
        //        files: {
        //            'js/compiled/app.min.js': ['js/compiled/app.js']
        //        }
        //    }
        //},

        watch: {
            css: {
                files: [
                    'dev/scss/**/*.scss'
                ],
                tasks: ['default']
            },
            js: {
                files: [
                    'dev/js/libs/*.js',
                    'dev/js/models/*.js',
                    'dev/js/app.js'
                ],
                tasks: ['default']
            },
            html: {
                files: [
                    'dev/**/*.html'//,
                    //'production/img/icons/sprite-demo.html'
                ],
                tasks: ['default']
            }
        },

        svgstore: {
            options: {
                prefix: 'icon_',
                includedemo : function (svg) {
                    return svg.svg;
                }
            },
            default: {
                files: {
                    'production/img/icons/sprite.svg': ['dev/img/icons/svgs/*.svg']
                }
            }
        }

    });

    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-html-build');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-svgstore');
    //grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('css', [
        'sass',
        'autoprefixer',
        'svgstore'
    ]);

    grunt.registerTask('default', [
        'css',
        'concat',
        'htmlbuild'
        //'uglify'
    ]);

};
