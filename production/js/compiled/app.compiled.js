;(function () {
	'use strict';

	/**
	 * @preserve FastClick: polyfill to remove click delays on browsers with touch UIs.
	 *
	 * @codingstandard ftlabs-jsv2
	 * @copyright The Financial Times Limited [All Rights Reserved]
	 * @license MIT License (see LICENSE.txt)
	 */

	/*jslint browser:true, node:true*/
	/*global define, Event, Node*/


	/**
	 * Instantiate fast-clicking listeners on the specified layer.
	 *
	 * @constructor
	 * @param {Element} layer The layer to listen on
	 * @param {Object} [options={}] The options to override the defaults
	 */
	function FastClick(layer, options) {
		var oldOnClick;

		options = options || {};

		/**
		 * Whether a click is currently being tracked.
		 *
		 * @type boolean
		 */
		this.trackingClick = false;


		/**
		 * Timestamp for when click tracking started.
		 *
		 * @type number
		 */
		this.trackingClickStart = 0;


		/**
		 * The element being tracked for a click.
		 *
		 * @type EventTarget
		 */
		this.targetElement = null;


		/**
		 * X-coordinate of touch start event.
		 *
		 * @type number
		 */
		this.touchStartX = 0;


		/**
		 * Y-coordinate of touch start event.
		 *
		 * @type number
		 */
		this.touchStartY = 0;


		/**
		 * ID of the last touch, retrieved from Touch.identifier.
		 *
		 * @type number
		 */
		this.lastTouchIdentifier = 0;


		/**
		 * Touchmove boundary, beyond which a click will be cancelled.
		 *
		 * @type number
		 */
		this.touchBoundary = options.touchBoundary || 10;


		/**
		 * The FastClick layer.
		 *
		 * @type Element
		 */
		this.layer = layer;

		/**
		 * The minimum time between tap(touchstart and touchend) events
		 *
		 * @type number
		 */
		this.tapDelay = options.tapDelay || 200;

		/**
		 * The maximum time for a tap
		 *
		 * @type number
		 */
		this.tapTimeout = options.tapTimeout || 700;

		if (FastClick.notNeeded(layer)) {
			return;
		}

		// Some old versions of Android don't have Function.prototype.bind
		function bind(method, context) {
			return function() { return method.apply(context, arguments); };
		}


		var methods = ['onMouse', 'onClick', 'onTouchStart', 'onTouchMove', 'onTouchEnd', 'onTouchCancel'];
		var context = this;
		for (var i = 0, l = methods.length; i < l; i++) {
			context[methods[i]] = bind(context[methods[i]], context);
		}

		// Set up event handlers as required
		if (deviceIsAndroid) {
			layer.addEventListener('mouseover', this.onMouse, true);
			layer.addEventListener('mousedown', this.onMouse, true);
			layer.addEventListener('mouseup', this.onMouse, true);
		}

		layer.addEventListener('click', this.onClick, true);
		layer.addEventListener('touchstart', this.onTouchStart, false);
		layer.addEventListener('touchmove', this.onTouchMove, false);
		layer.addEventListener('touchend', this.onTouchEnd, false);
		layer.addEventListener('touchcancel', this.onTouchCancel, false);

		// Hack is required for browsers that don't support Event#stopImmediatePropagation (e.g. Android 2)
		// which is how FastClick normally stops click events bubbling to callbacks registered on the FastClick
		// layer when they are cancelled.
		if (!Event.prototype.stopImmediatePropagation) {
			layer.removeEventListener = function(type, callback, capture) {
				var rmv = Node.prototype.removeEventListener;
				if (type === 'click') {
					rmv.call(layer, type, callback.hijacked || callback, capture);
				} else {
					rmv.call(layer, type, callback, capture);
				}
			};

			layer.addEventListener = function(type, callback, capture) {
				var adv = Node.prototype.addEventListener;
				if (type === 'click') {
					adv.call(layer, type, callback.hijacked || (callback.hijacked = function(event) {
						if (!event.propagationStopped) {
							callback(event);
						}
					}), capture);
				} else {
					adv.call(layer, type, callback, capture);
				}
			};
		}

		// If a handler is already declared in the element's onclick attribute, it will be fired before
		// FastClick's onClick handler. Fix this by pulling out the user-defined handler function and
		// adding it as listener.
		if (typeof layer.onclick === 'function') {

			// Android browser on at least 3.2 requires a new reference to the function in layer.onclick
			// - the old one won't work if passed to addEventListener directly.
			oldOnClick = layer.onclick;
			layer.addEventListener('click', function(event) {
				oldOnClick(event);
			}, false);
			layer.onclick = null;
		}
	}

	/**
	* Windows Phone 8.1 fakes user agent string to look like Android and iPhone.
	*
	* @type boolean
	*/
	var deviceIsWindowsPhone = navigator.userAgent.indexOf("Windows Phone") >= 0;

	/**
	 * Android requires exceptions.
	 *
	 * @type boolean
	 */
	var deviceIsAndroid = navigator.userAgent.indexOf('Android') > 0 && !deviceIsWindowsPhone;


	/**
	 * iOS requires exceptions.
	 *
	 * @type boolean
	 */
	var deviceIsIOS = /iP(ad|hone|od)/.test(navigator.userAgent) && !deviceIsWindowsPhone;


	/**
	 * iOS 4 requires an exception for select elements.
	 *
	 * @type boolean
	 */
	var deviceIsIOS4 = deviceIsIOS && (/OS 4_\d(_\d)?/).test(navigator.userAgent);


	/**
	 * iOS 6.0-7.* requires the target element to be manually derived
	 *
	 * @type boolean
	 */
	var deviceIsIOSWithBadTarget = deviceIsIOS && (/OS [6-7]_\d/).test(navigator.userAgent);

	/**
	 * BlackBerry requires exceptions.
	 *
	 * @type boolean
	 */
	var deviceIsBlackBerry10 = navigator.userAgent.indexOf('BB10') > 0;

	/**
	 * Determine whether a given element requires a native click.
	 *
	 * @param {EventTarget|Element} target Target DOM element
	 * @returns {boolean} Returns true if the element needs a native click
	 */
	FastClick.prototype.needsClick = function(target) {
		switch (target.nodeName.toLowerCase()) {

		// Don't send a synthetic click to disabled inputs (issue #62)
		case 'button':
		case 'select':
		case 'textarea':
			if (target.disabled) {
				return true;
			}

			break;
		case 'input':

			// File inputs need real clicks on iOS 6 due to a browser bug (issue #68)
			if ((deviceIsIOS && target.type === 'file') || target.disabled) {
				return true;
			}

			break;
		case 'label':
		case 'iframe': // iOS8 homescreen apps can prevent events bubbling into frames
		case 'video':
			return true;
		}

		return (/\bneedsclick\b/).test(target.className);
	};


	/**
	 * Determine whether a given element requires a call to focus to simulate click into element.
	 *
	 * @param {EventTarget|Element} target Target DOM element
	 * @returns {boolean} Returns true if the element requires a call to focus to simulate native click.
	 */
	FastClick.prototype.needsFocus = function(target) {
		switch (target.nodeName.toLowerCase()) {
		case 'textarea':
			return true;
		case 'select':
			return !deviceIsAndroid;
		case 'input':
			switch (target.type) {
			case 'button':
			case 'checkbox':
			case 'file':
			case 'image':
			case 'radio':
			case 'submit':
				return false;
			}

			// No point in attempting to focus disabled inputs
			return !target.disabled && !target.readOnly;
		default:
			return (/\bneedsfocus\b/).test(target.className);
		}
	};


	/**
	 * Send a click event to the specified element.
	 *
	 * @param {EventTarget|Element} targetElement
	 * @param {Event} event
	 */
	FastClick.prototype.sendClick = function(targetElement, event) {
		var clickEvent, touch;

		// On some Android devices activeElement needs to be blurred otherwise the synthetic click will have no effect (#24)
		if (document.activeElement && document.activeElement !== targetElement) {
			document.activeElement.blur();
		}

		touch = event.changedTouches[0];

		// Synthesise a click event, with an extra attribute so it can be tracked
		clickEvent = document.createEvent('MouseEvents');
		clickEvent.initMouseEvent(this.determineEventType(targetElement), true, true, window, 1, touch.screenX, touch.screenY, touch.clientX, touch.clientY, false, false, false, false, 0, null);
		clickEvent.forwardedTouchEvent = true;
		targetElement.dispatchEvent(clickEvent);
	};

	FastClick.prototype.determineEventType = function(targetElement) {

		//Issue #159: Android Chrome Select Box does not open with a synthetic click event
		if (deviceIsAndroid && targetElement.tagName.toLowerCase() === 'select') {
			return 'mousedown';
		}

		return 'click';
	};


	/**
	 * @param {EventTarget|Element} targetElement
	 */
	FastClick.prototype.focus = function(targetElement) {
		var length;

		// Issue #160: on iOS 7, some input elements (e.g. date datetime month) throw a vague TypeError on setSelectionRange. These elements don't have an integer value for the selectionStart and selectionEnd properties, but unfortunately that can't be used for detection because accessing the properties also throws a TypeError. Just check the type instead. Filed as Apple bug #15122724.
		if (deviceIsIOS && targetElement.setSelectionRange && targetElement.type.indexOf('date') !== 0 && targetElement.type !== 'time' && targetElement.type !== 'month') {
			length = targetElement.value.length;
			targetElement.setSelectionRange(length, length);
		} else {
			targetElement.focus();
		}
	};


	/**
	 * Check whether the given target element is a child of a scrollable layer and if so, set a flag on it.
	 *
	 * @param {EventTarget|Element} targetElement
	 */
	FastClick.prototype.updateScrollParent = function(targetElement) {
		var scrollParent, parentElement;

		scrollParent = targetElement.fastClickScrollParent;

		// Attempt to discover whether the target element is contained within a scrollable layer. Re-check if the
		// target element was moved to another parent.
		if (!scrollParent || !scrollParent.contains(targetElement)) {
			parentElement = targetElement;
			do {
				if (parentElement.scrollHeight > parentElement.offsetHeight) {
					scrollParent = parentElement;
					targetElement.fastClickScrollParent = parentElement;
					break;
				}

				parentElement = parentElement.parentElement;
			} while (parentElement);
		}

		// Always update the scroll top tracker if possible.
		if (scrollParent) {
			scrollParent.fastClickLastScrollTop = scrollParent.scrollTop;
		}
	};


	/**
	 * @param {EventTarget} targetElement
	 * @returns {Element|EventTarget}
	 */
	FastClick.prototype.getTargetElementFromEventTarget = function(eventTarget) {

		// On some older browsers (notably Safari on iOS 4.1 - see issue #56) the event target may be a text node.
		if (eventTarget.nodeType === Node.TEXT_NODE) {
			return eventTarget.parentNode;
		}

		return eventTarget;
	};


	/**
	 * On touch start, record the position and scroll offset.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onTouchStart = function(event) {
		var targetElement, touch, selection;

		// Ignore multiple touches, otherwise pinch-to-zoom is prevented if both fingers are on the FastClick element (issue #111).
		if (event.targetTouches.length > 1) {
			return true;
		}

		targetElement = this.getTargetElementFromEventTarget(event.target);
		touch = event.targetTouches[0];

		if (deviceIsIOS) {

			// Only trusted events will deselect text on iOS (issue #49)
			selection = window.getSelection();
			if (selection.rangeCount && !selection.isCollapsed) {
				return true;
			}

			if (!deviceIsIOS4) {

				// Weird things happen on iOS when an alert or confirm dialog is opened from a click event callback (issue #23):
				// when the user next taps anywhere else on the page, new touchstart and touchend events are dispatched
				// with the same identifier as the touch event that previously triggered the click that triggered the alert.
				// Sadly, there is an issue on iOS 4 that causes some normal touch events to have the same identifier as an
				// immediately preceeding touch event (issue #52), so this fix is unavailable on that platform.
				// Issue 120: touch.identifier is 0 when Chrome dev tools 'Emulate touch events' is set with an iOS device UA string,
				// which causes all touch events to be ignored. As this block only applies to iOS, and iOS identifiers are always long,
				// random integers, it's safe to to continue if the identifier is 0 here.
				if (touch.identifier && touch.identifier === this.lastTouchIdentifier) {
					event.preventDefault();
					return false;
				}

				this.lastTouchIdentifier = touch.identifier;

				// If the target element is a child of a scrollable layer (using -webkit-overflow-scrolling: touch) and:
				// 1) the user does a fling scroll on the scrollable layer
				// 2) the user stops the fling scroll with another tap
				// then the event.target of the last 'touchend' event will be the element that was under the user's finger
				// when the fling scroll was started, causing FastClick to send a click event to that layer - unless a check
				// is made to ensure that a parent layer was not scrolled before sending a synthetic click (issue #42).
				this.updateScrollParent(targetElement);
			}
		}

		this.trackingClick = true;
		this.trackingClickStart = event.timeStamp;
		this.targetElement = targetElement;

		this.touchStartX = touch.pageX;
		this.touchStartY = touch.pageY;

		// Prevent phantom clicks on fast double-tap (issue #36)
		if ((event.timeStamp - this.lastClickTime) < this.tapDelay) {
			event.preventDefault();
		}

		return true;
	};


	/**
	 * Based on a touchmove event object, check whether the touch has moved past a boundary since it started.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.touchHasMoved = function(event) {
		var touch = event.changedTouches[0], boundary = this.touchBoundary;

		if (Math.abs(touch.pageX - this.touchStartX) > boundary || Math.abs(touch.pageY - this.touchStartY) > boundary) {
			return true;
		}

		return false;
	};


	/**
	 * Update the last position.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onTouchMove = function(event) {
		if (!this.trackingClick) {
			return true;
		}

		// If the touch has moved, cancel the click tracking
		if (this.targetElement !== this.getTargetElementFromEventTarget(event.target) || this.touchHasMoved(event)) {
			this.trackingClick = false;
			this.targetElement = null;
		}

		return true;
	};


	/**
	 * Attempt to find the labelled control for the given label element.
	 *
	 * @param {EventTarget|HTMLLabelElement} labelElement
	 * @returns {Element|null}
	 */
	FastClick.prototype.findControl = function(labelElement) {

		// Fast path for newer browsers supporting the HTML5 control attribute
		if (labelElement.control !== undefined) {
			return labelElement.control;
		}

		// All browsers under test that support touch events also support the HTML5 htmlFor attribute
		if (labelElement.htmlFor) {
			return document.getElementById(labelElement.htmlFor);
		}

		// If no for attribute exists, attempt to retrieve the first labellable descendant element
		// the list of which is defined here: http://www.w3.org/TR/html5/forms.html#category-label
		return labelElement.querySelector('button, input:not([type=hidden]), keygen, meter, output, progress, select, textarea');
	};


	/**
	 * On touch end, determine whether to send a click event at once.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onTouchEnd = function(event) {
		var forElement, trackingClickStart, targetTagName, scrollParent, touch, targetElement = this.targetElement;

		if (!this.trackingClick) {
			return true;
		}

		// Prevent phantom clicks on fast double-tap (issue #36)
		if ((event.timeStamp - this.lastClickTime) < this.tapDelay) {
			this.cancelNextClick = true;
			return true;
		}

		if ((event.timeStamp - this.trackingClickStart) > this.tapTimeout) {
			return true;
		}

		// Reset to prevent wrong click cancel on input (issue #156).
		this.cancelNextClick = false;

		this.lastClickTime = event.timeStamp;

		trackingClickStart = this.trackingClickStart;
		this.trackingClick = false;
		this.trackingClickStart = 0;

		// On some iOS devices, the targetElement supplied with the event is invalid if the layer
		// is performing a transition or scroll, and has to be re-detected manually. Note that
		// for this to function correctly, it must be called *after* the event target is checked!
		// See issue #57; also filed as rdar://13048589 .
		if (deviceIsIOSWithBadTarget) {
			touch = event.changedTouches[0];

			// In certain cases arguments of elementFromPoint can be negative, so prevent setting targetElement to null
			targetElement = document.elementFromPoint(touch.pageX - window.pageXOffset, touch.pageY - window.pageYOffset) || targetElement;
			targetElement.fastClickScrollParent = this.targetElement.fastClickScrollParent;
		}

		targetTagName = targetElement.tagName.toLowerCase();
		if (targetTagName === 'label') {
			forElement = this.findControl(targetElement);
			if (forElement) {
				this.focus(targetElement);
				if (deviceIsAndroid) {
					return false;
				}

				targetElement = forElement;
			}
		} else if (this.needsFocus(targetElement)) {

			// Case 1: If the touch started a while ago (best guess is 100ms based on tests for issue #36) then focus will be triggered anyway. Return early and unset the target element reference so that the subsequent click will be allowed through.
			// Case 2: Without this exception for input elements tapped when the document is contained in an iframe, then any inputted text won't be visible even though the value attribute is updated as the user types (issue #37).
			if ((event.timeStamp - trackingClickStart) > 100 || (deviceIsIOS && window.top !== window && targetTagName === 'input')) {
				this.targetElement = null;
				return false;
			}

			this.focus(targetElement);
			this.sendClick(targetElement, event);

			// Select elements need the event to go through on iOS 4, otherwise the selector menu won't open.
			// Also this breaks opening selects when VoiceOver is active on iOS6, iOS7 (and possibly others)
			if (!deviceIsIOS || targetTagName !== 'select') {
				this.targetElement = null;
				event.preventDefault();
			}

			return false;
		}

		if (deviceIsIOS && !deviceIsIOS4) {

			// Don't send a synthetic click event if the target element is contained within a parent layer that was scrolled
			// and this tap is being used to stop the scrolling (usually initiated by a fling - issue #42).
			scrollParent = targetElement.fastClickScrollParent;
			if (scrollParent && scrollParent.fastClickLastScrollTop !== scrollParent.scrollTop) {
				return true;
			}
		}

		// Prevent the actual click from going though - unless the target node is marked as requiring
		// real clicks or if it is in the whitelist in which case only non-programmatic clicks are permitted.
		if (!this.needsClick(targetElement)) {
			event.preventDefault();
			this.sendClick(targetElement, event);
		}

		return false;
	};


	/**
	 * On touch cancel, stop tracking the click.
	 *
	 * @returns {void}
	 */
	FastClick.prototype.onTouchCancel = function() {
		this.trackingClick = false;
		this.targetElement = null;
	};


	/**
	 * Determine mouse events which should be permitted.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onMouse = function(event) {

		// If a target element was never set (because a touch event was never fired) allow the event
		if (!this.targetElement) {
			return true;
		}

		if (event.forwardedTouchEvent) {
			return true;
		}

		// Programmatically generated events targeting a specific element should be permitted
		if (!event.cancelable) {
			return true;
		}

		// Derive and check the target element to see whether the mouse event needs to be permitted;
		// unless explicitly enabled, prevent non-touch click events from triggering actions,
		// to prevent ghost/doubleclicks.
		if (!this.needsClick(this.targetElement) || this.cancelNextClick) {

			// Prevent any user-added listeners declared on FastClick element from being fired.
			if (event.stopImmediatePropagation) {
				event.stopImmediatePropagation();
			} else {

				// Part of the hack for browsers that don't support Event#stopImmediatePropagation (e.g. Android 2)
				event.propagationStopped = true;
			}

			// Cancel the event
			event.stopPropagation();
			event.preventDefault();

			return false;
		}

		// If the mouse event is permitted, return true for the action to go through.
		return true;
	};


	/**
	 * On actual clicks, determine whether this is a touch-generated click, a click action occurring
	 * naturally after a delay after a touch (which needs to be cancelled to avoid duplication), or
	 * an actual click which should be permitted.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onClick = function(event) {
		var permitted;

		// It's possible for another FastClick-like library delivered with third-party code to fire a click event before FastClick does (issue #44). In that case, set the click-tracking flag back to false and return early. This will cause onTouchEnd to return early.
		if (this.trackingClick) {
			this.targetElement = null;
			this.trackingClick = false;
			return true;
		}

		// Very odd behaviour on iOS (issue #18): if a submit element is present inside a form and the user hits enter in the iOS simulator or clicks the Go button on the pop-up OS keyboard the a kind of 'fake' click event will be triggered with the submit-type input element as the target.
		if (event.target.type === 'submit' && event.detail === 0) {
			return true;
		}

		permitted = this.onMouse(event);

		// Only unset targetElement if the click is not permitted. This will ensure that the check for !targetElement in onMouse fails and the browser's click doesn't go through.
		if (!permitted) {
			this.targetElement = null;
		}

		// If clicks are permitted, return true for the action to go through.
		return permitted;
	};


	/**
	 * Remove all FastClick's event listeners.
	 *
	 * @returns {void}
	 */
	FastClick.prototype.destroy = function() {
		var layer = this.layer;

		if (deviceIsAndroid) {
			layer.removeEventListener('mouseover', this.onMouse, true);
			layer.removeEventListener('mousedown', this.onMouse, true);
			layer.removeEventListener('mouseup', this.onMouse, true);
		}

		layer.removeEventListener('click', this.onClick, true);
		layer.removeEventListener('touchstart', this.onTouchStart, false);
		layer.removeEventListener('touchmove', this.onTouchMove, false);
		layer.removeEventListener('touchend', this.onTouchEnd, false);
		layer.removeEventListener('touchcancel', this.onTouchCancel, false);
	};


	/**
	 * Check whether FastClick is needed.
	 *
	 * @param {Element} layer The layer to listen on
	 */
	FastClick.notNeeded = function(layer) {
		var metaViewport;
		var chromeVersion;
		var blackberryVersion;
		var firefoxVersion;

		// Devices that don't support touch don't need FastClick
		if (typeof window.ontouchstart === 'undefined') {
			return true;
		}

		// Chrome version - zero for other browsers
		chromeVersion = +(/Chrome\/([0-9]+)/.exec(navigator.userAgent) || [,0])[1];

		if (chromeVersion) {

			if (deviceIsAndroid) {
				metaViewport = document.querySelector('meta[name=viewport]');

				if (metaViewport) {
					// Chrome on Android with user-scalable="no" doesn't need FastClick (issue #89)
					if (metaViewport.content.indexOf('user-scalable=no') !== -1) {
						return true;
					}
					// Chrome 32 and above with width=device-width or less don't need FastClick
					if (chromeVersion > 31 && document.documentElement.scrollWidth <= window.outerWidth) {
						return true;
					}
				}

			// Chrome desktop doesn't need FastClick (issue #15)
			} else {
				return true;
			}
		}

		if (deviceIsBlackBerry10) {
			blackberryVersion = navigator.userAgent.match(/Version\/([0-9]*)\.([0-9]*)/);

			// BlackBerry 10.3+ does not require Fastclick library.
			// https://github.com/ftlabs/fastclick/issues/251
			if (blackberryVersion[1] >= 10 && blackberryVersion[2] >= 3) {
				metaViewport = document.querySelector('meta[name=viewport]');

				if (metaViewport) {
					// user-scalable=no eliminates click delay.
					if (metaViewport.content.indexOf('user-scalable=no') !== -1) {
						return true;
					}
					// width=device-width (or less than device-width) eliminates click delay.
					if (document.documentElement.scrollWidth <= window.outerWidth) {
						return true;
					}
				}
			}
		}

		// IE10 with -ms-touch-action: none or manipulation, which disables double-tap-to-zoom (issue #97)
		if (layer.style.msTouchAction === 'none' || layer.style.touchAction === 'manipulation') {
			return true;
		}

		// Firefox version - zero for other browsers
		firefoxVersion = +(/Firefox\/([0-9]+)/.exec(navigator.userAgent) || [,0])[1];

		if (firefoxVersion >= 27) {
			// Firefox 27+ does not have tap delay if the content is not zoomable - https://bugzilla.mozilla.org/show_bug.cgi?id=922896

			metaViewport = document.querySelector('meta[name=viewport]');
			if (metaViewport && (metaViewport.content.indexOf('user-scalable=no') !== -1 || document.documentElement.scrollWidth <= window.outerWidth)) {
				return true;
			}
		}

		// IE11: prefixed -ms-touch-action is no longer supported and it's recomended to use non-prefixed version
		// http://msdn.microsoft.com/en-us/library/windows/apps/Hh767313.aspx
		if (layer.style.touchAction === 'none' || layer.style.touchAction === 'manipulation') {
			return true;
		}

		return false;
	};


	/**
	 * Factory method for creating a FastClick object
	 *
	 * @param {Element} layer The layer to listen on
	 * @param {Object} [options={}] The options to override the defaults
	 */
	FastClick.attach = function(layer, options) {
		return new FastClick(layer, options);
	};


	if (typeof define === 'function' && typeof define.amd === 'object' && define.amd) {

		// AMD. Register as an anonymous module.
		define(function() {
			return FastClick;
		});
	} else if (typeof module !== 'undefined' && module.exports) {
		module.exports = FastClick.attach;
		module.exports.FastClick = FastClick;
	} else {
		window.FastClick = FastClick;
	}
}());

/*!
 * EventEmitter v4.2.7 - git.io/ee
 * Oliver Caldwell
 * MIT license
 * @preserve
 */

(function () {
	'use strict';

	/**
	 * Class for managing events.
	 * Can be extended to provide event functionality in other classes.
	 *
	 * @class EventEmitter Manages event registering and emitting.
	 */
	function EventEmitter() {}

	// Shortcuts to improve speed and size
	var proto = EventEmitter.prototype;
	var exports = this;
	var originalGlobalValue = exports.EventEmitter;

	/**
	 * Finds the index of the listener for the event in it's storage array.
	 *
	 * @param {Function[]} listeners Array of listeners to search through.
	 * @param {Function} listener Method to look for.
	 * @return {Number} Index of the specified listener, -1 if not found
	 * @api private
	 */
	function indexOfListener(listeners, listener) {
		var i = listeners.length;
		while (i--) {
			if (listeners[i].listener === listener) {
				return i;
			}
		}

		return -1;
	}

	/**
	 * Alias a method while keeping the context correct, to allow for overwriting of target method.
	 *
	 * @param {String} name The name of the target method.
	 * @return {Function} The aliased method
	 * @api private
	 */
	function alias(name) {
		return function aliasClosure() {
			return this[name].apply(this, arguments);
		};
	}

	/**
	 * Returns the listener array for the specified event.
	 * Will initialise the event object and listener arrays if required.
	 * Will return an object if you use a regex search. The object contains keys for each matched event. So /ba[rz]/ might return an object containing bar and baz. But only if you have either defined them with defineEvent or added some listeners to them.
	 * Each property in the object response is an array of listener functions.
	 *
	 * @param {String|RegExp} evt Name of the event to return the listeners from.
	 * @return {Function[]|Object} All listener functions for the event.
	 */
	proto.getListeners = function getListeners(evt) {
		var events = this._getEvents();
		var response;
		var key;

		// Return a concatenated array of all matching events if
		// the selector is a regular expression.
		if (evt instanceof RegExp) {
			response = {};
			for (key in events) {
				if (events.hasOwnProperty(key) && evt.test(key)) {
					response[key] = events[key];
				}
			}
		}
		else {
			response = events[evt] || (events[evt] = []);
		}

		return response;
	};

	/**
	 * Takes a list of listener objects and flattens it into a list of listener functions.
	 *
	 * @param {Object[]} listeners Raw listener objects.
	 * @return {Function[]} Just the listener functions.
	 */
	proto.flattenListeners = function flattenListeners(listeners) {
		var flatListeners = [];
		var i;

		for (i = 0; i < listeners.length; i += 1) {
			flatListeners.push(listeners[i].listener);
		}

		return flatListeners;
	};

	/**
	 * Fetches the requested listeners via getListeners but will always return the results inside an object. This is mainly for internal use but others may find it useful.
	 *
	 * @param {String|RegExp} evt Name of the event to return the listeners from.
	 * @return {Object} All listener functions for an event in an object.
	 */
	proto.getListenersAsObject = function getListenersAsObject(evt) {
		var listeners = this.getListeners(evt);
		var response;

		if (listeners instanceof Array) {
			response = {};
			response[evt] = listeners;
		}

		return response || listeners;
	};

	/**
	 * Adds a listener function to the specified event.
	 * The listener will not be added if it is a duplicate.
	 * If the listener returns true then it will be removed after it is called.
	 * If you pass a regular expression as the event name then the listener will be added to all events that match it.
	 *
	 * @param {String|RegExp} evt Name of the event to attach the listener to.
	 * @param {Function} listener Method to be called when the event is emitted. If the function returns true then it will be removed after calling.
	 * @return {Object} Current instance of EventEmitter for chaining.
	 */
	proto.addListener = function addListener(evt, listener) {
		var listeners = this.getListenersAsObject(evt);
		var listenerIsWrapped = typeof listener === 'object';
		var key;

		for (key in listeners) {
			if (listeners.hasOwnProperty(key) && indexOfListener(listeners[key], listener) === -1) {
				listeners[key].push(listenerIsWrapped ? listener : {
					listener: listener,
					once: false
				});
			}
		}

		return this;
	};

	/**
	 * Alias of addListener
	 */
	proto.on = alias('addListener');

	/**
	 * Semi-alias of addListener. It will add a listener that will be
	 * automatically removed after it's first execution.
	 *
	 * @param {String|RegExp} evt Name of the event to attach the listener to.
	 * @param {Function} listener Method to be called when the event is emitted. If the function returns true then it will be removed after calling.
	 * @return {Object} Current instance of EventEmitter for chaining.
	 */
	proto.addOnceListener = function addOnceListener(evt, listener) {
		return this.addListener(evt, {
			listener: listener,
			once: true
		});
	};

	/**
	 * Alias of addOnceListener.
	 */
	proto.once = alias('addOnceListener');

	/**
	 * Defines an event name. This is required if you want to use a regex to add a listener to multiple events at once. If you don't do this then how do you expect it to know what event to add to? Should it just add to every possible match for a regex? No. That is scary and bad.
	 * You need to tell it what event names should be matched by a regex.
	 *
	 * @param {String} evt Name of the event to create.
	 * @return {Object} Current instance of EventEmitter for chaining.
	 */
	proto.defineEvent = function defineEvent(evt) {
		this.getListeners(evt);
		return this;
	};

	/**
	 * Uses defineEvent to define multiple events.
	 *
	 * @param {String[]} evts An array of event names to define.
	 * @return {Object} Current instance of EventEmitter for chaining.
	 */
	proto.defineEvents = function defineEvents(evts) {
		for (var i = 0; i < evts.length; i += 1) {
			this.defineEvent(evts[i]);
		}
		return this;
	};

	/**
	 * Removes a listener function from the specified event.
	 * When passed a regular expression as the event name, it will remove the listener from all events that match it.
	 *
	 * @param {String|RegExp} evt Name of the event to remove the listener from.
	 * @param {Function} listener Method to remove from the event.
	 * @return {Object} Current instance of EventEmitter for chaining.
	 */
	proto.removeListener = function removeListener(evt, listener) {
		var listeners = this.getListenersAsObject(evt);
		var index;
		var key;

		for (key in listeners) {
			if (listeners.hasOwnProperty(key)) {
				index = indexOfListener(listeners[key], listener);

				if (index !== -1) {
					listeners[key].splice(index, 1);
				}
			}
		}

		return this;
	};

	/**
	 * Alias of removeListener
	 */
	proto.off = alias('removeListener');

	/**
	 * Adds listeners in bulk using the manipulateListeners method.
	 * If you pass an object as the second argument you can add to multiple events at once. The object should contain key value pairs of events and listeners or listener arrays. You can also pass it an event name and an array of listeners to be added.
	 * You can also pass it a regular expression to add the array of listeners to all events that match it.
	 * Yeah, this function does quite a bit. That's probably a bad thing.
	 *
	 * @param {String|Object|RegExp} evt An event name if you will pass an array of listeners next. An object if you wish to add to multiple events at once.
	 * @param {Function[]} [listeners] An optional array of listener functions to add.
	 * @return {Object} Current instance of EventEmitter for chaining.
	 */
	proto.addListeners = function addListeners(evt, listeners) {
		// Pass through to manipulateListeners
		return this.manipulateListeners(false, evt, listeners);
	};

	/**
	 * Removes listeners in bulk using the manipulateListeners method.
	 * If you pass an object as the second argument you can remove from multiple events at once. The object should contain key value pairs of events and listeners or listener arrays.
	 * You can also pass it an event name and an array of listeners to be removed.
	 * You can also pass it a regular expression to remove the listeners from all events that match it.
	 *
	 * @param {String|Object|RegExp} evt An event name if you will pass an array of listeners next. An object if you wish to remove from multiple events at once.
	 * @param {Function[]} [listeners] An optional array of listener functions to remove.
	 * @return {Object} Current instance of EventEmitter for chaining.
	 */
	proto.removeListeners = function removeListeners(evt, listeners) {
		// Pass through to manipulateListeners
		return this.manipulateListeners(true, evt, listeners);
	};

	/**
	 * Edits listeners in bulk. The addListeners and removeListeners methods both use this to do their job. You should really use those instead, this is a little lower level.
	 * The first argument will determine if the listeners are removed (true) or added (false).
	 * If you pass an object as the second argument you can add/remove from multiple events at once. The object should contain key value pairs of events and listeners or listener arrays.
	 * You can also pass it an event name and an array of listeners to be added/removed.
	 * You can also pass it a regular expression to manipulate the listeners of all events that match it.
	 *
	 * @param {Boolean} remove True if you want to remove listeners, false if you want to add.
	 * @param {String|Object|RegExp} evt An event name if you will pass an array of listeners next. An object if you wish to add/remove from multiple events at once.
	 * @param {Function[]} [listeners] An optional array of listener functions to add/remove.
	 * @return {Object} Current instance of EventEmitter for chaining.
	 */
	proto.manipulateListeners = function manipulateListeners(remove, evt, listeners) {
		var i;
		var value;
		var single = remove ? this.removeListener : this.addListener;
		var multiple = remove ? this.removeListeners : this.addListeners;

		// If evt is an object then pass each of it's properties to this method
		if (typeof evt === 'object' && !(evt instanceof RegExp)) {
			for (i in evt) {
				if (evt.hasOwnProperty(i) && (value = evt[i])) {
					// Pass the single listener straight through to the singular method
					if (typeof value === 'function') {
						single.call(this, i, value);
					}
					else {
						// Otherwise pass back to the multiple function
						multiple.call(this, i, value);
					}
				}
			}
		}
		else {
			// So evt must be a string
			// And listeners must be an array of listeners
			// Loop over it and pass each one to the multiple method
			i = listeners.length;
			while (i--) {
				single.call(this, evt, listeners[i]);
			}
		}

		return this;
	};

	/**
	 * Removes all listeners from a specified event.
	 * If you do not specify an event then all listeners will be removed.
	 * That means every event will be emptied.
	 * You can also pass a regex to remove all events that match it.
	 *
	 * @param {String|RegExp} [evt] Optional name of the event to remove all listeners for. Will remove from every event if not passed.
	 * @return {Object} Current instance of EventEmitter for chaining.
	 */
	proto.removeEvent = function removeEvent(evt) {
		var type = typeof evt;
		var events = this._getEvents();
		var key;

		// Remove different things depending on the state of evt
		if (type === 'string') {
			// Remove all listeners for the specified event
			delete events[evt];
		}
		else if (evt instanceof RegExp) {
			// Remove all events matching the regex.
			for (key in events) {
				if (events.hasOwnProperty(key) && evt.test(key)) {
					delete events[key];
				}
			}
		}
		else {
			// Remove all listeners in all events
			delete this._events;
		}

		return this;
	};

	/**
	 * Alias of removeEvent.
	 *
	 * Added to mirror the node API.
	 */
	proto.removeAllListeners = alias('removeEvent');

	/**
	 * Emits an event of your choice.
	 * When emitted, every listener attached to that event will be executed.
	 * If you pass the optional argument array then those arguments will be passed to every listener upon execution.
	 * Because it uses `apply`, your array of arguments will be passed as if you wrote them out separately.
	 * So they will not arrive within the array on the other side, they will be separate.
	 * You can also pass a regular expression to emit to all events that match it.
	 *
	 * @param {String|RegExp} evt Name of the event to emit and execute listeners for.
	 * @param {Array} [args] Optional array of arguments to be passed to each listener.
	 * @return {Object} Current instance of EventEmitter for chaining.
	 */
	proto.emitEvent = function emitEvent(evt, args) {
		var listeners = this.getListenersAsObject(evt);
		var listener;
		var i;
		var key;
		var response;

		for (key in listeners) {
			if (listeners.hasOwnProperty(key)) {
				i = listeners[key].length;

				while (i--) {
					// If the listener returns true then it shall be removed from the event
					// The function is executed either with a basic call or an apply if there is an args array
					listener = listeners[key][i];

					if (listener.once === true) {
						this.removeListener(evt, listener.listener);
					}

					response = listener.listener.apply(this, args || []);

					if (response === this._getOnceReturnValue()) {
						this.removeListener(evt, listener.listener);
					}
				}
			}
		}

		return this;
	};

	/**
	 * Alias of emitEvent
	 */
	proto.trigger = alias('emitEvent');

	/**
	 * Subtly different from emitEvent in that it will pass its arguments on to the listeners, as opposed to taking a single array of arguments to pass on.
	 * As with emitEvent, you can pass a regex in place of the event name to emit to all events that match it.
	 *
	 * @param {String|RegExp} evt Name of the event to emit and execute listeners for.
	 * @param {...*} Optional additional arguments to be passed to each listener.
	 * @return {Object} Current instance of EventEmitter for chaining.
	 */
	proto.emit = function emit(evt) {
		var args = Array.prototype.slice.call(arguments, 1);
		return this.emitEvent(evt, args);
	};

	/**
	 * Sets the current value to check against when executing listeners. If a
	 * listeners return value matches the one set here then it will be removed
	 * after execution. This value defaults to true.
	 *
	 * @param {*} value The new value to check for when executing listeners.
	 * @return {Object} Current instance of EventEmitter for chaining.
	 */
	proto.setOnceReturnValue = function setOnceReturnValue(value) {
		this._onceReturnValue = value;
		return this;
	};

	/**
	 * Fetches the current value to check against when executing listeners. If
	 * the listeners return value matches this one then it should be removed
	 * automatically. It will return true by default.
	 *
	 * @return {*|Boolean} The current value to check for or the default, true.
	 * @api private
	 */
	proto._getOnceReturnValue = function _getOnceReturnValue() {
		if (this.hasOwnProperty('_onceReturnValue')) {
			return this._onceReturnValue;
		}
		else {
			return true;
		}
	};

	/**
	 * Fetches the events object and creates one if required.
	 *
	 * @return {Object} The events storage object.
	 * @api private
	 */
	proto._getEvents = function _getEvents() {
		return this._events || (this._events = {});
	};

	/**
	 * Reverts the global {@link EventEmitter} to its previous value and returns a reference to this version.
	 *
	 * @return {Function} Non conflicting EventEmitter class.
	 */
	EventEmitter.noConflict = function noConflict() {
		exports.EventEmitter = originalGlobalValue;
		return EventEmitter;
	};

	// Expose the class either via AMD, CommonJS or the global object
	if (typeof define === 'function' && define.amd) {
		define(function () {
			return EventEmitter;
		});
	}
	else if (typeof module === 'object' && module.exports){
		module.exports = EventEmitter;
	}
	else {
		this.EventEmitter = EventEmitter;
	}
}.call(this));

/*
 * JSFace Object Oriented Programming Library
 * https://github.com/tnhu/jsface
 *
 * Copyright (c) 2009-2013 Tan Nhu
 * Licensed under MIT license (https://github.com/tnhu/jsface/blob/master/LICENSE.txt)
 */
(function(context, OBJECT, NUMBER, LENGTH, toString, undefined, oldClass, jsface) {
  /**
   * Return a map itself or null. A map is a set of { key: value }
   * @param obj object to be checked
   * @return obj itself as a map or false
   */
  function mapOrNil(obj) { return (obj && typeof obj === OBJECT && !(typeof obj.length === NUMBER && !(obj.propertyIsEnumerable(LENGTH))) && obj) || null; }

  /**
   * Return an array itself or null
   * @param obj object to be checked
   * @return obj itself as an array or null
   */
  function arrayOrNil(obj) { return (obj && typeof obj === OBJECT && typeof obj.length === NUMBER && !(obj.propertyIsEnumerable(LENGTH)) && obj) || null; }

  /**
   * Return a function itself or null
   * @param obj object to be checked
   * @return obj itself as a function or null
   */
  function functionOrNil(obj) { return (obj && typeof obj === "function" && obj) || null; }

  /**
   * Return a string itself or null
   * @param obj object to be checked
   * @return obj itself as a string or null
   */
  function stringOrNil(obj) { return (toString.apply(obj) === "[object String]" && obj) || null; }

  /**
   * Return a class itself or null
   * @param obj object to be checked
   * @return obj itself as a class or false
   */
  function classOrNil(obj) { return (functionOrNil(obj) && (obj.prototype && obj === obj.prototype.constructor) && obj) || null; }

  /**
   * Util for extend() to copy a map of { key:value } to an object
   * @param key key
   * @param value value
   * @param ignoredKeys ignored keys
   * @param object object
   * @param iClass true if object is a class
   * @param oPrototype object prototype
   */
  function copier(key, value, ignoredKeys, object, iClass, oPrototype) {
    if ( !ignoredKeys || !ignoredKeys.hasOwnProperty(key)) {
      object[key] = value;
      if (iClass) { oPrototype[key] = value; }                       // class? copy to prototype as well
    }
  }

  /**
   * Extend object from subject, ignore properties in ignoredKeys
   * @param object the child
   * @param subject the parent
   * @param ignoredKeys (optional) keys should not be copied to child
   */
  function extend(object, subject, ignoredKeys) {
    if (arrayOrNil(subject)) {
      for (var len = subject.length; --len >= 0;) { extend(object, subject[len], ignoredKeys); }
    } else {
      ignoredKeys = ignoredKeys || { constructor: 1, $super: 1, prototype: 1, $superp: 1 };

      var iClass     = classOrNil(object),
          isSubClass = classOrNil(subject),
          oPrototype = object.prototype, supez, key, proto;

      // copy static properties and prototype.* to object
      if (mapOrNil(subject)) {
        for (key in subject) {
          copier(key, subject[key], ignoredKeys, object, iClass, oPrototype);
        }
      }

      if (isSubClass) {
        proto = subject.prototype;
        for (key in proto) {
          copier(key, proto[key], ignoredKeys, object, iClass, oPrototype);
        }
      }

      // prototype properties
      if (iClass && isSubClass) { extend(oPrototype, subject.prototype, ignoredKeys); }
    }
  }

  /**
   * Create a class.
   * @param parent parent class(es)
   * @param api class api
   * @return class
   */
  function Class(parent, api) {
    if ( !api) {
      parent = (api = parent, 0);                                     // !api means there's no parent
    }

    var clazz, constructor, singleton, statics, key, bindTo, len, i = 0, p,
        ignoredKeys = { constructor: 1, $singleton: 1, $statics: 1, prototype: 1, $super: 1, $superp: 1, main: 1, toString: 0 },
        plugins     = Class.plugins;

    api         = (typeof api === "function" ? api() : api) || {};             // execute api if it's a function
    constructor = api.hasOwnProperty("constructor") ? api.constructor : 0;     // hasOwnProperty is a must, constructor is special
    singleton   = api.$singleton;
    statics     = api.$statics;

    // add plugins' keys into ignoredKeys
    for (key in plugins) { ignoredKeys[key] = 1; }

    // construct constructor
    clazz  = singleton ? {} : (constructor ? constructor : function(){});

    // determine bindTo: where api should be bound
    bindTo = singleton ? clazz : clazz.prototype;

    // make sure parent is always an array
    parent = !parent || arrayOrNil(parent) ? parent : [ parent ];

    // do inherit
    len = parent && parent.length;
    while (i < len) {
      p = parent[i++];
      for (key in p) {
        if ( !ignoredKeys[key]) {
          bindTo[key] = p[key];
          if ( !singleton) { clazz[key] = p[key]; }
        }
      }
      for (key in p.prototype) { if ( !ignoredKeys[key]) { bindTo[key] = p.prototype[key]; } }
    }

    // copy properties from api to bindTo
    for (key in api) {
      if ( !ignoredKeys[key]) {
        bindTo[key] = api[key];
      }
    }

    // copy static properties from statics to both clazz and bindTo
    for (key in statics) { clazz[key] = bindTo[key] = statics[key]; }

    // if class is not a singleton, add $super and $superp
    if ( !singleton) {
      p = parent && parent[0] || parent;
      clazz.$super  = p;
      clazz.$superp = p && p.prototype ? p.prototype : p;
      bindTo.$class = clazz;
    }

    for (key in plugins) { plugins[key](clazz, parent, api); }                 // pass control to plugins
    if (functionOrNil(api.main)) { api.main.call(clazz, clazz); }              // execute main()
    return clazz;
  }

  /* Class plugins repository */
  Class.plugins = {};

  /* Initialization */
  jsface = {
    Class        : Class,
    extend       : extend,
    mapOrNil     : mapOrNil,
    arrayOrNil   : arrayOrNil,
    functionOrNil: functionOrNil,
    stringOrNil  : stringOrNil,
    classOrNil   : classOrNil
  };

  if (typeof module !== "undefined" && module.exports) {                       // NodeJS/CommonJS
    module.exports = jsface;
  } else {
    oldClass          = context.Class;                                         // save current Class namespace
    context.Class     = Class;                                                 // bind Class and jsface to global scope
    context.jsface    = jsface;
    jsface.noConflict = function() { context.Class = oldClass; };              // no conflict
  }
})(this, "object", "number", "length", Object.prototype.toString);
// Device.js
// (c) 2014 Matthew Hudson
// Device.js is freely distributable under the MIT license.
// For all details and documentation:
// http://matthewhudson.me/projects/device.js/

(function() {

  var device,
    previousDevice,
    addClass,
    documentElement,
    find,
    handleOrientation,
    hasClass,
    orientationEvent,
    removeClass,
    userAgent;

  // Save the previous value of the device variable.
  previousDevice = window.device;

  device = {};

  // Add device as a global object.
  window.device = device;

  // The <html> element.
  documentElement = window.document.documentElement;

  // The client user agent string.
  // Lowercase, so we can use the more efficient indexOf(), instead of Regex
  userAgent = window.navigator.userAgent.toLowerCase();

  // Main functions
  // --------------

  device.ios = function () {
    return device.iphone() || device.ipod() || device.ipad();
  };

  device.iphone = function () {
    return find('iphone');
  };

  device.ipod = function () {
    return find('ipod');
  };

  device.ipad = function () {
    return find('ipad');
  };

  device.android = function () {
    return find('android');
  };

  device.androidPhone = function () {
    return device.android() && find('mobile');
  };

  device.androidTablet = function () {
    return device.android() && !find('mobile');
  };

  device.blackberry = function () {
    return find('blackberry') || find('bb10') || find('rim');
  };

  device.blackberryPhone = function () {
    return device.blackberry() && !find('tablet');
  };

  device.blackberryTablet = function () {
    return device.blackberry() && find('tablet');
  };

  device.windows = function () {
    return find('windows');
  };

  device.windowsPhone = function () {
    return device.windows() && find('phone');
  };

  device.windowsTablet = function () {
    return device.windows() && (find('touch') && !device.windowsPhone());
  };

  device.fxos = function () {
    return (find('(mobile;') || find('(tablet;')) && find('; rv:');
  };

  device.fxosPhone = function () {
    return device.fxos() && find('mobile');
  };

  device.fxosTablet = function () {
    return device.fxos() && find('tablet');
  };

  device.meego = function () {
    return find('meego');
  };

  device.cordova = function () {
    return window.cordova && location.protocol === 'file:';
  };

  device.nodeWebkit = function () {
    return typeof window.process === 'object';
  };

  device.mobile = function () {
    return device.androidPhone() || device.iphone() || device.ipod() || device.windowsPhone() || device.blackberryPhone() || device.fxosPhone() || device.meego();
  };

  device.tablet = function () {
    return device.ipad() || device.androidTablet() || device.blackberryTablet() || device.windowsTablet() || device.fxosTablet();
  };

  device.desktop = function () {
    return !device.tablet() && !device.mobile();
  };

  device.television = function() {
    var i, tvString;

    television = [
      "googletv",
      "viera",
      "smarttv",
      "internet.tv",
      "netcast",
      "nettv",
      "appletv",
      "boxee",
      "kylo",
      "roku",
      "dlnadoc",
      "roku",
      "pov_tv",
      "hbbtv",
      "ce-html"
    ];

    i = 0;
    while (i < television.length) {
      if (find(television[i])) {
        return true;
      }
      i++;
    }
    return false;
  };

  device.portrait = function () {
    return (window.innerHeight / window.innerWidth) > 1;
  };

  device.landscape = function () {
    return (window.innerHeight / window.innerWidth) < 1;
  };

  // Public Utility Functions
  // ------------------------

  // Run device.js in noConflict mode,
  // returning the device variable to its previous owner.
  device.noConflict = function () {
    window.device = previousDevice;
    return this;
  };

  // Private Utility Functions
  // -------------------------

  // Simple UA string search
  find = function (needle) {
    return userAgent.indexOf(needle) !== -1;
  };

  // Check if documentElement already has a given class.
  hasClass = function (className) {
    var regex;
    regex = new RegExp(className, 'i');
    return documentElement.className.match(regex);
  };

  // Add one or more CSS classes to the <html> element.
  addClass = function (className) {
    if (!hasClass(className)) {
      documentElement.className = documentElement.className.trim() + " " + className;
    }
  };

  // Remove single CSS class from the <html> element.
  removeClass = function (className) {
    if (hasClass(className)) {
      documentElement.className = documentElement.className.replace(" " + className, "");
    }
  };

  // HTML Element Handling
  // ---------------------

  // Insert the appropriate CSS class based on the _user_agent.

  if (device.ios()) {
    if (device.ipad()) {
      addClass("ios ipad tablet");
    } else if (device.iphone()) {
      addClass("ios iphone mobile");
    } else if (device.ipod()) {
      addClass("ios ipod mobile");
    }
  } else if (device.android()) {
    if (device.androidTablet()) {
      addClass("android tablet");
    } else {
      addClass("android mobile");
    }
  } else if (device.blackberry()) {
    if (device.blackberryTablet()) {
      addClass("blackberry tablet");
    } else {
      addClass("blackberry mobile");
    }
  } else if (device.windows()) {
    if (device.windowsTablet()) {
      addClass("windows tablet");
    } else if (device.windowsPhone()) {
      addClass("windows mobile");
    } else {
      addClass("desktop");
    }
  } else if (device.fxos()) {
    if (device.fxosTablet()) {
      addClass("fxos tablet");
    } else {
      addClass("fxos mobile");
    }
  } else if (device.meego()) {
    addClass("meego mobile");
  } else if (device.nodeWebkit()) {
    addClass("node-webkit");
  } else if (device.television()) {
    addClass("television");
  } else if (device.desktop()) {
    addClass("desktop");
  }

  if (device.cordova()) {
    addClass("cordova");
  }

  // Orientation Handling
  // --------------------

  // Handle device orientation changes.
  handleOrientation = function () {
    if (device.landscape()) {
      removeClass("portrait");
      addClass("landscape");
    } else {
      removeClass("landscape");
      addClass("portrait");
    }
    return;
  };

  // Detect whether device supports orientationchange event,
  // otherwise fall back to the resize event.
  if (window.hasOwnProperty("onorientationchange")) {
    orientationEvent = "orientationchange";
  } else {
    orientationEvent = "resize";
  }

  // Listen for changes in orientation.
  if (window.addEventListener) {
    window.addEventListener(orientationEvent, handleOrientation, false);
  } else if (window.attachEvent) {
    window.attachEvent(orientationEvent, handleOrientation);
  } else {
    window[orientationEvent] = handleOrientation;
  }

  handleOrientation();

  if (typeof define === 'function' && typeof define.amd === 'object' && define.amd) {
    define(function() {
      return device;
    });
  } else if (typeof module !== 'undefined' && module.exports) {
    module.exports = device;
  } else {
    window.device = device;
  }

}).call(this);

;(function ($, _, context) {
    'use strict';

    var hlf = function() {

        var config = {
            asGoalUrl: (location.protocol == 'file:' ? 'http:' : '') + '//metrics.aviasales.ru',
            mobileMode: mobileDetect(),

        };

        /** Check mobile verison **/

        function mobileDetect(){
            var x = document.createElement('input'); x.setAttribute('type', 'date');
            if (x.type == 'date' && device.mobile()){return true}else{return false}
        }

        /**
         * Check google analytics (GA) exists on page
         * @returns {boolean|*}
         */
        function gaExists() {
            return typeof ga !== 'undefined' && _.isFunction(ga);
        }

        /**
         * Send goal to GA
         * @param goal ['category', 'name']
         * @returns {boolean}
         */
        function gaGoal(goal) {
            if(gaExists() && typeof goal !== 'undefined' && _.isArray(goal) && goal.length) {
                ga('send', 'event', goal[0], goal[1]);
                return true;
            }
            return false;
        }

        /**
         * Return ga linkerParam for cross domain linking
         * @returns string todo wtf? obj needed
         */
        function gaGetLinkerParam() {
            var str = null;
            if(gaExists()) {
                // collect cross domain linker
                ga(function (tracker) {
                    str = tracker.get('linkerParam');
                });
            }
            return str;
        }

        /**
         * Its yandex metrika
         * @type {object}
         */
        var yam = null;

        /**
         * Check yandex metrika exists on page
         * @returns {boolean}
         */
        function yamExists() {
            if(!yam && typeof Ya !== 'undefined') {
                _.each(Ya._metrika.counters, function(v){
                    yam = v;
                    return;
                });
            }
            return !!yam;
        }

        /**
         * Send goal to yam
         * @param goal 'goal-name'
         * @returns {boolean}
         */
        function yamGoal(goal) {
            if(yamExists() && _.isString(goal) && goal.length) {
                yam.reachGoal(goal);
                return true;
            }
            return false;
        }

        /**
         * Send goal to aviasales
         * @param goal 'goal-name'
         * @param d any json
         * @returns {boolean}
         */
        function asGoal(goal, d) {
            if(_.isString(goal) && goal.length) {
                $.ajax({
                    url: config.asGoalUrl,
                    type: 'get',
                    dataType: 'jsonp',
                    data: {
                        goal: goal,
                        data: JSON.stringify(d)
                    }
                });
                return true;
            }
            return false;
        }

        function goal(goals, data) {
            yamGoal(goals.yam || null);
            gaGoal(goals.ga ? goals.ga.split('.') : null);
            asGoal(goals.as || null, data);
        }

        return {
            gaGetLinkerParam: gaGetLinkerParam,
            goal: goal,
            config: config,
            jst: {} // js templates
        }

    }();

    function paramsParse(str) {

        var digitTest = /^\d+$/,
            keyBreaker = /([^\[\]]+)|(\[\])/g,
            plus = /\+/g;

        var data = {},
            pairs = str.split('&'),
            current,
            lastPart = '';

        for (var i = 0; i < pairs.length; i++) {
            current = data;
            var pair = pairs[i].split('=');

            // if we find foo=1+1=2
            if (pair.length != 2) {
                pair = [pair[0], pair.slice(1).join("=")]
            }

            try {
                var key = decodeURIComponent(pair[0].replace(plus, " ")),
                    value = decodeURIComponent(pair[1].replace(plus, " ")),
                    parts = key.match(keyBreaker);
            }
            catch (err) {
                parts=null;
            }

            if (parts !== null) {
                for (var j = 0; j < parts.length - 1; j++) {
                    var part = parts[j];
                    if (!current[part]) {
                        // if what we are pointing to looks like an array
                        current[part] = digitTest.test(parts[j + 1]) || parts[j + 1] == "[]" ? [] : {}
                    }
                    current = current[part];
                }
                lastPart = parts[parts.length - 1];
                if (lastPart == "[]") {
                    current.push(value)
                } else {
                    current[lastPart] = value;
                }
            }
        }

        return data;

    }

    hlf.getTpl = function (name) {
        return _.template(hlf.jst[name + '.jst'].main());
    };

    hlf.getEl = function($c, role, name) {
        var selector = '[hlf-role="' + role + '"]';
        if(name) {
            selector += '[hlf-name="' + name +'"]';
        }
        return $c.find(selector);
    };

    hlf.getContainer = function($c, place, value) {
        return $c.find('[hlf-' + place + '="' + value + '"]');
    };

    hlf.readCookie = function(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    };
    /**
     * GET params
     * @param n
     * @returns {*|null}
     * @constructor
     */
    hlf.GET = function(n) {
        var data = paramsParse(location.search.substring(1));
        return n ? (data[n] || null) : data;
    };

    /**
     * Working with cookie
     * @param n name
     * @returns {T}
     * todo deprecated
     */
    hlf.cookie = function (n) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + n + "=");
        if (parts.length == 2) { // todo its mistake
            return parts.pop().split(";").shift();
        }
    };

    context.hlf = hlf;

})(jQuery, _, this);
this["hlf"] = this["hlf"] || {};
this["hlf"]["jst"] = this["hlf"]["jst"] || {};
this["hlf"]["jst"]["ac.input.jst"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"hlf-input hlf-input--ac\" hlf-role=\"input-wrap\">\n    <input type=\"text\"\n           placeholder=\"<%= placeholder %>\"\n           value=\"<%= text %>\"\n           tabindex=\"<%= tabIndex %>\"\n           hlf-role=\"input\"/>\n    <div class=\"icon-load\" hlf-role=\"loader\"></div>\n    <i class=\"icon-close\" hlf-role=\"close\"></i>\n    <div class=\"hint\" hlf-role=\"hint\"><%= hint %></div>\n</div>";
  },"useData":true};
this["hlf"]["jst"]["ac.samples.jst"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"hlf-input--ac-samples\" hlf-role=\"samples\"><%= samplesText %></div>";
  },"useData":true};
this["hlf"]["jst"]["ac.samplesLink.jst"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<a href=\"#\" hlf-role=\"samples-link\" data-type=\"<%= type %>\" <% if (typeof(latinLocationFullName) !== \"undefined\") { %> data-latinLocationFullName=\"<%= latinLocationFullName %>\"<% } %> data-id=\"<%= id %>\" data-text=\"<%= text %>\"><%= sample %></a>";
  },"useData":true};
this["hlf"]["jst"]["calendar.head.jst"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"ui-datepicker-head\"><%= head %></div>";
  },"useData":true};
this["hlf"]["jst"]["calendar.input.jst"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"hlf-input hlf-input--calendar\" hlf-role=\"input-wrap\">\n    <% if (inline) { %>\n        <div hlf-role=\"input\" /></div>\n    <% } else { %>\n        <input type=\"text\" placeholder=\"<%= placeholder %>\" tabindex=\"<%= tabIndex %>\" hlf-role=\"input\" height=\"60\"  />\n    <% } %>\n    <div class=\"hint\" hlf-role=\"hint\"></div>\n    <div class=\"pseudo-placeholder\" hlf-role=\"placeholder\"><%= placeholder %></div>\n</div>";
  },"useData":true};
this["hlf"]["jst"]["calendar.legend.jst"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"ui-datepicker-legend\">\n    <div class=\"ui-datepicker-legend-head\"><%= legend %></div>\n    <div class=\"ui-datepicker-legend-points\">\n        <div class=\"ui-datepicker-legend-points-line\"></div>\n        <ul class=\"ui-datepicker-legend-points-list\">\n            <% _.each(points, function(point, i) { %>\n            <li class=\"ui-datepicker-legend-points-item ui-datepicker-legend-points-item--<%= i %>\"><%= point %></li>\n            <% }); %>\n        </ul>\n    </div>\n</div>";
  },"useData":true};
this["hlf"]["jst"]["guests.child.jst"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<li class=\"hlf-guests-children-item\" hlf-role=\"child-container\" hlf-name=\"<%= key %>\">\n    <div class=\"hlf-guests-child-age-title\">\n        <%= title %>\n        <div  class=\"hlf-guests-child-age-hint\"><%= hint %></div>\n    </div>\n    <div class=\"hlf-guests-child-age-controls\">\n        <% if (childValSep) { %>\n            <div class=\"hlf-guests-child-age-val\">\n                <span hlf-role=\"child-age\"><%= age %></span>\n            </div>\n            <div class=\"hlf-guests-child-controls\">\n                <a class='hlf-control' href=\"#\" hlf-role=\"child-age-decrement\"><%= decControlContentChld %>︎</a>\n                <a class='hlf-control' href=\"#\" hlf-role=\"child-age-increment\"><%= incControlContentChld %></a>\n            </div>\n        <% } else { %>\n            <a class='hlf-control' href=\"#\" hlf-role=\"child-age-decrement\"><%= decControlContentChld %>︎</a>\n            <div class=\"hlf-guests-child-age-val\">\n                <span hlf-role=\"child-age\"><%= age %></span>\n            </div>\n            <a class='hlf-control' href=\"#\" hlf-role=\"child-age-increment\"><%= incControlContentChld %></a>\n        <% } %>\n    </div>\n</li>";
  },"useData":true};
this["hlf"]["jst"]["guests.container.jst"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"hlf-guests hlf-state--closed\" hlf-role=\"guests\">\n    <a href=\"#\" class=\"hlf-guests-i\" tabindex=\"<%= tabIndex %>\" hlf-role=\"summary\"></a>\n    <div class=\"hlf-guests-dd\" hlf-role=\"controls\">\n        <div class=\"hlf-guests-adults\">\n            <% if (titlesPosInside) { %>\n                <div class=\"hlf-guests-adults-controls\">\n                    <a class='hlf-control' href=\"#\" hlf-role=\"adults-decrement\"><span><%= decControlContent %></span></a>\n                    <div class=\"hlf-guests-adults-val\">\n                        <span hlf-role=\"adults-val\"></span>\n                        <span hlf-role=\"adults-title\" class=\"hlf-guests-adults-title\"></span>\n                    </div>\n                    <a class='hlf-control' href=\"#\" hlf-role=\"adults-increment\"><span><%= incControlContent %></span></a>\n                </div>\n            <% } else { %>\n                <div hlf-role=\"adults-title\" class=\"hlf-guests-adults-title\"></div>\n                <div class=\"hlf-guests-adults-controls\">\n                    <a class='hlf-control' href=\"#\" hlf-role=\"adults-decrement\"><span><%= decControlContent %></span></a>\n                    <div class=\"hlf-guests-adults-val\" hlf-role=\"adults-val\"></div>\n                    <a class='hlf-control' href=\"#\" hlf-role=\"adults-increment\"><span><%= incControlContent %></span></a>\n                </div>\n            <% } %>\n        </div>\n        <div class=\"hlf-guests-children\">\n            <% if (titlesPosInside) { %>\n                <div class=\"hlf-guests-children-controls\">\n                    <a class='hlf-control' href=\"#\" hlf-role=\"children-decrement\"><span><%= decControlContent %></span></a>\n                    <div class=\"hlf-guests-children-val\">\n                        <span hlf-role=\"children-val\"></span>\n                        <span hlf-role=\"children-title\" class=\"hlf-guests-children-title\"></span>\n                    </div>\n                    <a class='hlf-control' href=\"#\" hlf-role=\"children-increment\"><span><%= incControlContent %></span></a>\n                </div>\n            <% } else { %>\n                <div hlf-role=\"children-title\" class=\"hlf-guests-children-title\"></div>\n                <div class=\"hlf-guests-children-controls\">\n                    <a class='hlf-control' href=\"#\" hlf-role=\"children-decrement\"><span><%= decControlContent %></span></a>\n                    <div class=\"hlf-guests-children-val\" hlf-role=\"children-val\"></div>\n                    <a class='hlf-control' href=\"#\" hlf-role=\"children-increment\"><span><%= incControlContent %></span></a>\n                </div>\n            <% } %>\n            <div class=\"hlf-guests-children-list-title\" hlf-role=\"children-list-title\"><%= childrenListTitle %></div>\n            <ul class=\"hlf-guests-children-list\" hlf-role=\"children-list\"></ul>\n        </div>\n    </div>\n</div>";
  },"useData":true};
this["hlf"]["jst"]["noDates.input.jst"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<label hlf-role=\"noDates-input-wrap\">\n    <input type=\"checkbox\" tabindex=\"<%= tabIndex %>\" hlf-role=\"noDates-input\"><%= text %>\n</label>";
  },"useData":true};
this["hlf"]["jst"]["submit.button.jst"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<button tabindex=\"<%= tabIndex %>\" hlf-role=\"button\"><%= text %></button>";
  },"useData":true};
;(function ($, _, hlf) {
    'use strict';

    /**
     * Form constructor
     * @param config
     * @param noMarker bool dont try to find marker
     * @returns {{controls: {}, param: Function}}
     */
    hlf.form = function (config, noMarker) {

        config = _.defaults(config || {}, {
            id: null,
            controls: {},
            params: {},
            target: '_self',
            hash: null,
            goalSubmit: {},
            onSubmit: function() {}
        });

        var $f = $('[hlf-form="' + config.id +'"]'), // todo check availability
            uid = _.uniqueId(), // form uid
            tabIndex = 1, // controls tabIndex counter
            controls = {};

        // draw each control
        _.each(config.controls, function(c, n) {
            controls[n] = c(n, $f, controls, uid + (tabIndex++) + '');
        });

        // wrap config functions to use controls obj
        _.each(controls, function(control) {
            _.each(control.config, function(value, key) {
                if(_.isFunction(value)) {
                    control.config[key] = _.partialRight(value, controls);
                }
            });
        });

        $f.on('submit', function() {
            var result = true;
            _.each(controls, function(control) {
                if(control.validate && _.isFunction(control.validate)) {
                    result = control.validate();
                }
                return result;
            });
            if(result) {
                // collect controls data
                var p = _.reduce(_.map(controls, function(i) {
                    return _.isFunction(i.getParams) ? i.getParams() : {};
                }), function(result, d) {
                    return _.merge(result, d);
                });

                // additional params if needed
                if(_.isUndefined(config.params.marker) && !noMarker) { // try to find marker in GET, then in cookie
                    var marker = hlf.GET('marker') || hlf.readCookie('marker') || null;
                    if(marker) {
                        config.params.marker = marker;
                    }
                }

                if(_.isUndefined(config.params.hls)) { // try to find hls in GET
                    var hls = hlf.GET('hls') || null;
                    if(hls) {
                        config.params.hls = hls;
                    }
                }

                config.onSubmit();

                p = _.merge(p, config.params);

                //don't remember why :|

                //_.each(p, function(val, key){
                //    if (!val || val == null || val == '') {
                //        delete p[key];
                //    }
                //});

                // send required goals
                hlf.goal(config.goalSubmit, {
                    params: p
                });

                var gaLinker = hlf.gaGetLinkerParam();

                window.open (
                    //todo: $f.attr('target') || config.target
                    $f.attr('action') + '/?' +
                    $.param(p) + // controls params
                    (gaLinker ? '&' + gaLinker : '') + // ga linker param
                    (config.hash ? '#' + config.hash : ''), $f.attr('target') || config.target); // hash and target (open in new window or not)
                return false;
            }
            return result;
        });

        return {
            controls: controls,

            /**
             * set/get for params
             *
             * @param name
             * @param value
             * @returns {*}
             */
            param: function (name, value) {
                if(!value) {
                    return config.params[name];
                }
                config.params[name] = value;
            }
        };

    }

})(jQuery, _, hlf);
;(function ($, _, hlf) {
    'use strict';

    hlf.ac = function (config) {

        var $c = null, // container
            $iw = null, // input wrap
            $i = null, // input
            $h = null, // hint
            $l = null, // loader
            $sc = null, // samples container
            $sl = null, // samples links
            $cl = null, //close button
            controls = {},
            goalUseInputSent = false; // flag means event 'use' sent (prevent multisent)

        config = _.defaults(config || {}, {

            url: (location.protocol == 'file:' ? 'http:' : '') + '//yasen.hotellook.com/autocomplete',
            name: 'destination', // getParams() param name in case
                                 // you select nothing in autocomplete
            className: '',
            text: '', // default input value
            type: '', // default type
            id: 0, // default id
            limit: 5, // limit for items each category
            locale: 'en-US',
            latinLocationFullName: '',
            mobileMode: hlf.config.mobileMode,
            autoFocus: false, // auto focus if field is empty
            needLocationPhotos: false,
            locationPhotoSize: '240x75',
            onlyLocations: false,

            placeholder: 'Type something....',
            hint: 'panic!', // this control always required, its hint text
            translateCategory: function(t) {
                return t;
            },
            translateHotelsCount: function(n) {
                return n + ' hotels';
            },

            goalUseInput: {}, // {ga: 'la-la-la.bla-bla', yam: 'sdasds', as: 'something'}
            goalAcSelect: {},
            goalUseSamples: {},
            goalUseClear: {},

            onSelect: function() {}, // select from autocomplete
            onSelectShowCalendar: null,
            onReset: function() {}, // fires when you type something in autocomplete
                                    // and type & id values resets
            avgPricesUrl: (location.protocol == 'file:' ? 'http:' : '') + '//yasen.hotellook.com/minprices/location_year/{id}.json',
            avgPricesCalendars: [], // names if controls
            avgPricesFormatter: function(v) {
                return '' + Math.round(v);
            },

            samplesText: 'For example: {list}',
            samplesList: [], // e.g.:
                             // [
                             //     {id: 15542, type: 'location', text: 'Paris, France', sample: 'Paris'},
                             //     {id: 15298, type: 'location', text: 'Marseille, France', sample: 'Marseille'}
                             // ]

            tplInput: hlf.getTpl('ac.input'),
            tplSamples: hlf.getTpl('ac.samples'),
            tplSamplesLink: hlf.getTpl('ac.samplesLink')

        });

        function avgPricesRequest (id) {
            if(config.avgPricesCalendars.length) {
                _.each(config.avgPricesCalendars, function(name) {
                    if(!_.isUndefined(controls[name])) {
                        controls[name].resetDetails();
                    }
                });
                $.ajax({
                    dataType: 'jsonp',
                    type: 'get',
                    cache: true,
                    url: config.avgPricesUrl.replace('{id}', id),
                    success: avgPricesRequestSuccess
                });
            }
        }

        function avgPricesRequestSuccess(data) {
            if(_.size(data)) {
                var first = null;
                _.each(config.avgPricesCalendars, function(name) {
                    if(!first) {
                        controls[name].specifyDetails(data, config.avgPricesFormatter);
                        controls[name].refresh();
                        first = controls[name];
                    } else {
                        controls[name].setDetails(_.clone(first.getDetails(), true));
                        controls[name].refresh();
                    }
                });
            }
        }

        /**
         * Get params string
         * @returns {object}
         */
        function getParams() {
            var r = {};
            switch(true) {
                case !!(config.id && config.type):
                    r[config.type + 'Id'] = config.id;
                    break;
                case !!config.text:
                    r[config.name] = config.text;
                    break;
                default: break;
            }
            return r;
        }

        function source(request, response) {
            $iw.addClass('hlf-state--loading');
            $.ajax({
                dataType: 'jsonp',
                type: 'get',
                url: config.url,
                data: {
                    lang: config.locale,
                    limit: config.limit,
                    term: request.term
                },
                jsonpCallback: 'hlf_ac_callback',
                success: function(data) {
                    var cities = _.map(data.cities, function(item) {
                        return {
                            id: item.id,
                            category: config.translateCategory('Locations'),
                            type: 'location',
                            value: item.fullname,
                            text: item.city,
                            latinLocationFullName: item.latinFullName,
                            clar: item.clar || '',
                            comment: config.translateHotelsCount(item.hotelsCount),
                            photo: config.needLocationPhotos ? 'https://photo.hotellook.com/static/cities/' + config.locationPhotoSize + '/' + item.id + '.auto' : false
                        }
                    });
                    if (!config.onlyLocations) {
                        var hotels = _.map(data.hotels, function (item) {
                            return {
                                id: item.id,
                                category: config.translateCategory('Hotels'),
                                type: 'hotel',
                                latinLocationFullName: item.latinLocationFullName,
                                value: item.name + ', ' + item.city + ', ' + item.country,
                                text: item.name,
                                clar: item.city + ', ' + item.country
                            }
                        });
                        response(_.union(cities, hotels));
                    } else {
                        response(_.union(cities));
                    }
                    $iw.removeClass('hlf-state--loading');
                },
                error: function() {
                    $iw.removeClass('hlf-state--loading');
                }
            });
        }

        function setValue(id, type, title) {
            config.id = id;
            config.type = !type ? 'location' : type;
            if (title) {
                config.text = title;
                $i.val(title);
            }
        }

        function onReset() {
            _.each(config.avgPricesCalendars, function(name) {
                controls[name].resetDetails();
            });
            config.onReset();
        }

        function select(type, id, text, locationName) {
            $iw.removeClass('hlf-state--error');
            config.type = type;
            config.id = id;
            if (locationName) {
                config.latinLocationFullName = locationName;
            }
            if(text) {
                $i.val(text);
            }
            avgPricesRequest(id);
            config.onSelect(type, id);
            if(config.onSelectShowCalendar) {
                if(!controls[config.onSelectShowCalendar].getStamp() && !config.mobileMode) {
                    controls[config.onSelectShowCalendar].show();
                }
            }
        }

        function validate() {
            if(_.size(getParams())) {
                return true;
            }
            $i.focus();
            $iw.addClass('hlf-state--error');
            return false;
        }

        return function(name, $f, c, ti) {
            if(config.id) {
                avgPricesRequest(config.id);
            }

            controls = c || {};
            config.tabIndex = ti || 0;

            $c = hlf.getContainer($f, 'ac', name);
            $c.html(config.tplInput(config));
            $iw = hlf.getEl($c, 'input-wrap');
            $i = hlf.getEl($c, 'input');
            $h = hlf.getEl($c, 'hint');
            $l = hlf.getEl($c, 'loader');
            $cl = hlf.getEl($c, 'close'); //close button
            config.className&&$iw.addClass(config.className);

            if (_.size(getParams())) {
                $iw.addClass('hlf-state--no-empty');
            };

            $i.reachAutocomplete({
                source: source,
                select: function(ev, data) {
                    select(data.item.type, data.item.id, '', data.item.latinLocationFullName);
                    hlf.goal(config.goalAcSelect, data.item);
                    hlf.goal(config.goalAcSelectType, data.item.type);
                },
                minLength: 3
            });

            $i.on('keyup', function() {
                config.text = $i.val();
                if (!(_.size(getParams())) && $iw.hasClass('hlf-state--no-empty')) {
                    $iw.removeClass('hlf-state--no-empty');
                };

            });

            $i.on('keydown', function(e) {
                // ignore enter & tab key
                if(e.keyCode !== 13 && e.keyCode !== 9) {
                    config.type = '';
                    config.id = 0;
                    onReset();
                }
                if(!goalUseInputSent) {
                    hlf.goal(config.goalUseInput);
                    goalUseInputSent = true;
                }
                $iw.removeClass('hlf-state--error');
                if ($i[0].value!='') {
                    $iw.addClass('hlf-state--no-empty');
                };
            });

            $i.on('focus', function() {
                $c.addClass('hlf-state--focus');
                $iw.addClass('hlf-state--focus');
                $iw.removeClass('hlf-state--error');
                goalUseInputSent = false;
            });

            $i.on('blur', function() {
                $c.removeClass('hlf-state--focus');
                $iw.removeClass('hlf-state--focus');
            });

            $h.on('click', function() {
                $iw.removeClass('hlf-state--error');
            });

            $cl.on('click', function(){
                $i[0].value='';
                $iw.removeClass('hlf-state--no-empty');
                config.type = '';
                config.id = 0;
                onReset();
                config.text='';
                $i.focus();
                hlf.goal(config.goalUseClear);
            });

            if(config.autoFocus && !config.text.length && !device.mobile()) {
                $i.focus();
            }

            if(config.samplesList.length) {

                var links = _.map(config.samplesList, function(i) {
                    return config.tplSamplesLink(i);
                });
                var samples = config.tplSamples(
                    _.defaults({
                        samplesText: config.samplesText.replace('{list}', links.join(', '))
                    }, config)
                );
                $c.append(samples);
                $sc = hlf.getEl($c, 'samples');
                $sl = hlf.getEl($sc, 'samples-link');

                $sl.on('click', function() {
                    var $this = $(this);
                    select($this.data('type'), $this.data('id'), $this.data('text'), $this.data('latinlocationfullname'));
                    hlf.goal(config.goalUseSamples);
                    $iw.addClass('hlf-state--no-empty');
                    return false;
                });

            }

            return {
                config: config,
                select: select,
                getParams: getParams,
                setValue: setValue,
                validate: validate,
                input: $i
            };

        };

    };

    // jsonp callback cheat
    function hlf_ac_callback() {}

    /**
     * Extension for jQuery.ui.autocomplete
     */
    $.widget('custom.reachAutocomplete', $.ui.autocomplete, {
        _create: function() {
            this._super();
            this.widget().menu('option', 'items', '> :not(.ui-autocomplete-category)');
        },
        _renderMenu: function( ul, items ) {
            ul.addClass('hlf-ac');
            var that = this,
                currentCategory = "";
            $.each( items, function( index, item ) {
                var li;
                if ( item.category != currentCategory ) {
                    ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
                    currentCategory = item.category;
                }
                li = that._renderItemData( ul, item );
                if ( item.category ) {
                    li.attr( "aria-label", item.category + " : " + item.label );
                }
            });
        },
        _renderItem: function (ul, item) {
            var label = '';
            if (item.photo)
                label += '<img src="' + item.photo + '" class="ui-menu-item-img" />';
            label += '<span class="ui-menu-item-text">' + item.text + (item.clar ? ', <span class="ui-menu-item-clar">' + item.clar + '</span>' : '') + '</span>';
            if (item.comment)
                label += '<span class="ui-menu-item-comment">' + item.comment + '</span>';
            return $("<li>")
                .append($("<a>").html(label))
                .appendTo(ul);
        }
    });

})(jQuery, _, hlf);
;
(function ($, _, hlf) {
    'use strict';

    hlf.calendar = function (config) {

        var $c = null, // container
            $iw = null, // input wrap
            $i = null, // input
            $h = null, // hint

            controls = {},
            uid = _.uniqueId('hlf.calendar'), /* Unique id for "refresh" cheating.
         * $i.datepicker('widget') returns any opened datepicker,
         * that means we need to mark this calendar with uid
         * to check uid in refresh function */
            details = {}, // day details
            range = {
                'in': null, // if you use relation calendar here is inferior date value
                'out': null // ... and here superior date value
            },
            isAutoShown = false; // it means calendar been shown automatically (not by user)

        config = _.defaults(config || {}, {

            required: false,
            className: '',
            name: 'date', // getParams() param name
            value: null, // default date Date()
            format: 'yy-mm-dd', // getParams() param value format
            min: 0, // min selectable date (in days from today)
            months: (function () {
                if (window.innerWidth <= 700) {
                    return 1
                } else {
                    return 2
                }
            })(), // num of months visible in datepicker
            locale: 'en-US',
            inline: false,
            mobileMode: hlf.config.mobileMode,
            placeholder: 'Choose date...',
            hintEmpty: 'Its required field', // hint text if required calendar field is empty
            head: null, // datepicker head text
            legend: 'Days colored by average price for the night',
            // todo hint text if relation calendar date >30 days
            // hintPeriod: 'period-panica!',

            goalSelectDate: {}, // goal on select date
            onSelect: function () {
            },

            relationCalendar: null, // name of control
            relationSuperior: false, // 1 - superior, 0 - inferior
            relationAutoSet: false,
            relationAutoShow: false,

            tplInput: hlf.getTpl('calendar.input'),
            tplHead: hlf.getTpl('calendar.head'),
            tplLegend: hlf.getTpl('calendar.legend')

        });

        function disable() {
            $i.datepicker('option', 'disabled', true);
            $iw.addClass('hlf-state--disabled');
            $iw.removeClass('hlf-state--error');
        }

        function enable() {
            $i.datepicker('option', 'disabled', false);
            $iw.removeClass('hlf-state--disabled');
        }

        /**
         * Show calendar
         * @param flag it means calendar has been shown automatically (by another control)
         */
        function show(flag) {
            isAutoShown = !!flag;
            if ( config.mobileMode === true ) {
                $i.focus();

            } else {
                setTimeout(function () { // jquery.ui.datepicker show cheat
                    $i.datepicker('show');
                }, 16);
            }

        }

        /**
         * Hide calendar
         */
        function hide() {
            $i.datepicker('hide');
        }

        /**
         * Refresh calendar
         */
        function refresh() {
            var $widget = $i.datepicker('widget');
            if ($widget.is(':visible') && $widget.attr('__cheat') == uid) { // use __cheat to refresh this own calendar
                // because in $widget may be any calendar
                $i.datepicker('refresh');
            }
        }

        /**
         * Adjust relation calendar
         */
        function relationAdjust() {
            var relation = controls[config.relationCalendar];
            // is it has value?
            if (relation && relation.getStamp() && getStamp() && getDate()) {
                // for superior relation set to 1 day more
                if (config.relationSuperior) {
                    if (relation.getStamp() <= getStamp()) {
                        relation.setAnotherDate(getDate(), 1);
                    }
                }
                // for inferior relation set to -1 day
                else {
                    if (relation.getStamp() >= getStamp()) {
                        relation.setAnotherDate(getDate(), -1);
                    }
                }
            }
        }

        /**
         * Auto set relation calendar value
         */
        function relationAutoSet() {

            var relation = controls[config.relationCalendar];
            // auto set value only if there is no value & relationAutoSet = true
            if (relation && !relation.getStamp() && config.relationAutoSet && getDate()) {
                relation.setAnotherDate(getDate(), config.relationSuperior ? 1 : -1);
            }
        }

        /**
         * Auto show relation calendar
         */
        function relationAutoShow(date) {
            var relation = controls[config.relationCalendar];
            if (relation && config.relationAutoShow && !isAutoShown) { // if this calendar has been shown by its
                relation.show(true, date);
            }
        }

        function getStamp() {
            var date=config.mobileMode?$i[0].value:$i.datepicker('getDate');

            if (date) {
                return (new Date(date)).getTime();
            }
            return null;
        }

        function getDate(i) { //argument for this function - input; if function called without argument, it uses the default input

            var input;
            input= i ? i : $i;
            var date;
            date=config.mobileMode?input[0].value:input.datepicker("getDate");
            if (date) {
                return new Date(date);
            }

            return null;
        }

        function setAnotherDate(date, modify) {
            var newDateStamp = new Date();
            newDateStamp.setTime(date.getTime() + ((modify || 0) * 86400000));
            var newDate = new Date(newDateStamp);
            if (config.mobileMode) {
                $i[0].value= dateToString(newDate)
            } else {
                $i.datepicker('setDate', newDate);
            }
            $iw.removeClass('hlf-state--error');
        }

        function validate() {
            if (!config.required || $i.datepicker('option', 'disabled') || _.size(getParams())) {
                return true;
            }
            $iw.addClass('hlf-state--error');
            $h.html(config.hintEmpty);
            return false;
        }

        /**
         * Process each date average price & calculate day details
         * todo fix it strange logic
         * @param data
         * @param formatter function
         */
        function specifyDetails(data, formatter) {
            if (!_.size(data))
                return false;
            var count = 0,
                sum = 0,
                min = 0,
                max = 0;
            _.each(data, function (v) {
                if (!min || v < min)
                    min = v;
                if (!max || v > max)
                    max = v;
                sum += v;
                count++;
            });
            // calculate average and diff with min & max
            var average = sum / count,
                diffMax = max - average,
                diffMin = average - min,
                dates = {};
            _.each(data, function (v, date) {
                // price more than average?
                var diff = v - average,
                    up = true;
                if (diff < 0) {
                    up = false;
                    diff = Math.abs(diff);
                }
                // choose closest position
                var percent = diff * 100 / (up ? diffMax : diffMin),
                    rate = 2;
                if (up) {
                    if (percent > 20)
                        rate = 3;
                    if (percent > 60)
                        rate = 4;
                } else {
                    if (percent > 20)
                        rate = 1;
                    if (percent > 60)
                        rate = 0;
                }
                dates[date] = {
                    'value': v, // average price
                    'rate': rate // rate for this price (0 - cheap, 4 - expensive)
                };
            }, this);
            setDetails({
                dates: dates,
                points: [ // price points for legend
                        average - diffMin * 0.8,
                    average,
                        average + diffMax * 0.8
                ],
                formatter: formatter || function (v) {
                    return v
                }
            });
            return true;
        }

        function setDetails(d) {
            $iw.addClass('hlf-state--detailed');
            details = d;
        }

        function getDetails() {
            return details;
        }

        function resetDetails() {
            $iw.removeClass('hlf-state--detailed');
            details = {};
        }

        /**
         * Returns cfg for each date cell in calendar
         *
         * @param date
         * @returns {*[]}
         */
        function getDayCfg(date) {
            var dateAsStr = $.datepicker.formatDate('yy-mm-dd', date),
                cfg = [true, '', ''];

            // fill cfg with date details
            if (!_.isUndefined(details.dates) && details.dates[dateAsStr]) {
                cfg[1] += ' ui-datepicker-dayType ui-datepicker-dayType--' + details.dates[dateAsStr].rate; // cell class
                cfg[2] = details.formatter(details.dates[dateAsStr].value);
            }

            // fill cfg with range details
            if (controls[config.relationCalendar]) {
                // in range
                cfg[1] += range['in'] && range['out'] && (range['in'].getTime() <= date.getTime() && date.getTime() <= range['out'].getTime()) ?
                    ' ui-datepicker-dayRange' :
                    '';
                // it is in or out date?
                cfg[1] += range['in'] && range['in'].getTime() == date.getTime() ? ' ui-datepicker-dayRange-in' : '';
                cfg[1] += range['out'] && range['out'].getTime() == date.getTime() ? ' ui-datepicker-dayRange-out' : '';
            }

            return cfg;
        }

        /**
         * On mouse hover calendar cell
         * @param e event
         * @param i datepicker instance
         */
        function dateMouseEnter(e, i) {

            var $hover = $(this),
                hoverDate = new Date($hover.data('year') + '-' + ($hover.data('month') + 1) + '-' + $hover.data('day') + ' 00:00:00');
            $hover.addClass('ui-datepicker-dayRange-hover--' + (config.relationSuperior ? 'in' : 'out'));

            // highlight range on hover
            $('[data-handler=selectDay]', i.dpDiv).each(function () {
                var $cell = $(this),
                    cellDate = new Date($cell.data('year') + '-' + ($cell.data('month') + 1) + '-' + $cell.data('day') + ' 00:00:00');
                if (config.relationSuperior ?
                    range['out'] && cellDate.getTime() >= hoverDate.getTime() && cellDate.getTime() <= range['out'].getTime() :
                    range['in'] && cellDate.getTime() <= hoverDate.getTime() && cellDate.getTime() >= range['in'].getTime()) {
                    $cell.addClass('ui-datepicker-dayRange-hover');
                }
            });
        }

        /**
         * On mouse over from calendar cell
         * @param e event
         * @param i datepicker instance
         */
        function dateMouseLeave(e, i) {
            // remove all hover classes
            $('[data-handler=selectDay]', i.dpDiv).each(function () {
                $(this).removeClass('ui-datepicker-dayRange-hover');
                $(this).removeClass('ui-datepicker-dayRange-hover--in');
                $(this).removeClass('ui-datepicker-dayRange-hover--out');
            });
        }

        function getParams() {
            var r = {};
            if ($i.datepicker('option', 'disabled')) {
                return r;
            }
            var date = getDate();
            if (date) {
                r[config.name] = $.datepicker.formatDate(config.format, date);
            }

            return r;
        }

        function dateToString(date) {
            var str;
            var tM = (date.getUTCMonth()+1)
            var tD = date.getUTCDate();
            tM<10&&(tM="0"+tM);
            tD<10&&(tD="0"+tD);
            str = date.getUTCFullYear()+"-"+tM+"-"+tD;
            return str;
        }

        // todo something wrong here
        function updateRange() {
            var rel = controls[config.relationCalendar];
            if (rel) {
                range['in'] = getDate();
                range['out'] = rel.getDate();
                if (!config.relationSuperior) {
                    var tmp = range['in'];
                    range['in'] = range['out'];
                    range['out'] = tmp;
                }
            }
        }

        function selectMonth(date, f) {
            $c = hlf.getContainer(f, 'calendar', config.relationCalendar);
            $i = hlf.getEl($c, 'input');
            $($i).datepicker( "setDate", date );
        }

        /**
         * Draw
         *
         * @param name
         * @returns {*[]}
         */
        return function (name, $f, c, ti, $pl) {

            controls = c || {};
            config.tabIndex = ti || 0;
            $c = hlf.getContainer($f, 'calendar', name);
            $c.html(config.tplInput(config));

            $iw = hlf.getEl($c, 'input-wrap');
            $i = hlf.getEl($c, 'input');
            $h = hlf.getEl($c, 'hint');
            config.className&&$iw.addClass(config.className);
            $pl = hlf.getEl($c, 'placeholder');
            if ( config.mobileMode === true && $i[0]) {
                // initialization native date input
                $i[0].type = 'date';
                $iw.addClass('html5date');

                var today=new Date,mindate=new Date;mindate.setDate(today.getDate()+config.min);
                $i[0].min =  dateToString(mindate);

            } else {
                // draw ui control
                $i.datepicker({
                    minDate: config.min,
                    numberOfMonths: config.months,
                    onSelect: function (date, e) {
                        relationAdjust();
                        relationAutoSet();
                        relationAutoShow( $.datepicker.formatDate(config.format, getDate()));
                        config.onSelect(date, $.datepicker.formatDate(config.format, getDate()), e);
                        hlf.goal(config.goalSelectDate);
                        $iw.removeClass('hlf-state--error');
                    },
                    beforeShowDay: function (date) {
                        updateRange();
                        return getDayCfg(date);
                    },
                    beforeShow: function (e, i) {
                        i.dpDiv.attr('__cheat', uid);
                        updateRange();
                    },
                    afterShow: function (i) {
                        if (config.head) {
                            i.dpDiv.prepend(config.tplHead({
                                head: config.head
                            }));
                        }
                        if (_.isArray(details.points) && details.points.length) {
                            $(i.dpDiv).append(
                                config.tplLegend({
                                    'legend': config.legend,
                                    'points': _.map(details.points, details.formatter)
                                })
                            );
                        }
                        if (controls[config.relationCalendar]) {
                            $('[data-handler=selectDay]', i.dpDiv)
                                .on('mouseenter', _.partialRight(dateMouseEnter, i))
                                .on('mouseleave', _.partialRight(dateMouseLeave, i));
                        }
                    },
                    onClose: function (date, i) {
                        $('[data-handler=selectDay]', i.dpDiv).off('mouseenter mouseleave');
                        isAutoShown = false;
                    }
                });
            }
            // maybe set a default value?
            if (_.isDate(config.value)) {
                // correct date by timezone offset
                config.value.setTime(config.value.getTime() + config.value.getTimezoneOffset() * 60 * 1000);
                $i.datepicker('setDate', config.value);
            }

            // customize datepicker with locale, unfortunately only this method works well
            if (config.locale) {
                $i.datepicker('option', $.datepicker.regional[config.locale]);
            }

            $i.on('focus', function () {
                $c.addClass('hlf-state--focus');
                $iw.addClass('hlf-state--focus');
                $iw.removeClass('hlf-state--error');
                $pl.addClass('hidden');
            });

            $i.on('blur', function () {
                $c.removeClass('hlf-state--focus');
                $iw.removeClass('hlf-state--focus');

                if (config.mobileMode === true) {
                    relationAdjust();   //check the correct date in inputs
                    relationAutoSet();
                }

            });

            $h.on('click', function () {
                $iw.removeClass('hlf-state--error');
            });

            return {
                config: config,
                disable: disable,
                enable: enable,
                refresh: refresh,
                show: show,
                hide: hide,
                getStamp: getStamp,
                getDate: getDate,
                setAnotherDate: setAnotherDate,
                getParams: getParams,
                setDetails: setDetails,
                getDetails: getDetails,
                resetDetails: resetDetails,
                specifyDetails: specifyDetails,
                validate: validate
            };

        };

    };

    /**
     * Extension for jQuery.ui.datepicker
     */
    $.datepicker._updateDatepicker_original = $.datepicker._updateDatepicker;
    $.datepicker._updateDatepicker = function (inst) {
        $.datepicker._updateDatepicker_original(inst);

        // add data-day attr to day cell
        if (typeof inst.settings.beforeShowDay == 'function') {
            // wrap beforeShowDay function and...
            inst.settings.beforeShowDay = _.wrap(inst.settings.beforeShowDay, function (func, date) {
                var dayConfig = func(date); // call user function
                dayConfig[1] += "' data-day='" + date.getDate(); // use quote cheat :)
                return dayConfig;
            });
        }

        // after show extension
        // todo fix it: works bad, calls too often
        var afterShow = this._get(inst, 'afterShow');
        if (afterShow) {
            afterShow.apply(null, [inst]);
        }

    };

    $.datepicker.regional['ru-RU'] = {clearText: 'Удалить', clearStatus: '',
        closeText: 'Закрыть', closeStatus: 'Закрыть без изменений',
        prevText: '< Предыдущие', prevStatus: 'Посмотреть предыдущие месяцы',
        nextText: 'Следующие >', nextStatus: 'Посмотреть следующие месяцы',
        currentText: 'Текущий', currentStatus: 'Посмотреть текущий месяц',
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
            'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        monthNamesShort: ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня',
            'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'],
        monthStatus: 'Посмотреть другой месяц', yearStatus: 'Посмотреть другой год',
        weekHeader: 'Sm', weekStatus: '',
        dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
        dayNamesShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        dayStatus: 'Использовать DD как первый день недели', dateStatus: 'Выбрать DD, MM d',
        dateFormat: 'D, d M', firstDay: 1,
        initStatus: 'Выбрать дату', isRTL: false
    };
    $.datepicker.regional['en-US'] = {
        closeText: 'Done',
        prevText: 'Prev',
        nextText: 'Next',
        currentText: 'Today',
        monthNames: ['January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December'],
        monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
        dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
        weekHeader: 'Wk',
        dateFormat: 'D, MM d', firstDay: 0,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.regional['en-GB'] = $.extend({}, $.datepicker.regional['en-US']);
    $.datepicker.regional['en-CA'] = $.extend({}, $.datepicker.regional['en-US']);
    $.datepicker.regional['en-IE'] = $.extend({}, $.datepicker.regional['en-US']);
    $.datepicker.regional['en-AU'] = $.extend({}, $.datepicker.regional['en-US']);
    $.datepicker.regional['fr-FR'] = {
        closeText: 'Fermer',
        prevText: 'Précédent',
        nextText: 'Suivant',
        currentText: 'Aujourd\'hlf',
        monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin',
            'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        monthNamesShort: ['janvier', 'février', 'mars', 'avril', 'mai', 'juin',
            'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'],
        dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
        dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
        dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
        weekHeader: 'Sem.',
        dateFormat: 'd M', firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.regional['es-ES'] = {
        closeText: 'Cerrar',
        prevText: '&#x3C;Ant',
        nextText: 'Sig&#x3E;',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
            'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
            'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'd MM', firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.regional['de-DE'] = {
        closeText: 'Schließen',
        prevText: '&#x3C;Zurück',
        nextText: 'Vor&#x3E;',
        currentText: 'Heute',
        monthNames: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni',
            'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
        monthNamesShort: ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun',
            'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
        dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
        dayNamesShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
        dayNamesMin: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
        weekHeader: 'KW',
        dateFormat: 'd MM', firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.regional['th-TH'] = {
        closeText: 'ปิด',
        prevText: '&#xAB;&#xA0;ย้อน',
        nextText: 'ถัดไป&#xA0;&#xBB;',
        currentText: 'วันนี้',
        monthNames: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
            'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
        monthNamesShort: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.',
            'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
        dayNames: ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'],
        dayNamesShort: ['อา.', 'จ.', 'อ.', 'พ.', 'พฤ.', 'ศ.', 'ส.'],
        dayNamesMin: ['อา.', 'จ.', 'อ.', 'พ.', 'พฤ.', 'ศ.', 'ส.'],
        weekHeader: 'Wk',
        dateFormat: 'd MM', firstDay: 0,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.regional['it-IT'] = {
        closeText: 'Chiudi',
        prevText: '&#x3C;Prec',
        nextText: 'Succ&#x3E;',
        currentText: 'Oggi',
        monthNames: ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno',
            'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'],
        monthNamesShort: ['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu',
            'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic'],
        dayNames: ['Domenica', 'Lunedì', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mer', 'Gio', 'Ven', 'Sab'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Me', 'Gi', 'Ve', 'Sa'],
        weekHeader: 'Sm',
        dateFormat: 'd MM', firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.regional['pl-PL'] = {
        closeText: 'Zamknij',
        prevText: '&#x3C;Poprzedni',
        nextText: 'Następny&#x3E;',
        currentText: 'Dziś',
        monthNames: ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec',
            'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
        monthNamesShort: ['stycznia', 'lutego', 'marca', 'kwietnia', 'maja', 'czerwca',
            'lipca', 'sierpnia', 'września', 'października', 'listopada', 'grudnia'],
        dayNames: ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota'],
        dayNamesShort: ['Nie', 'Pn', 'Wt', 'Śr', 'Czw', 'Pt', 'So'],
        dayNamesMin: ['N', 'Pn', 'Wt', 'Śr', 'Cz', 'Pt', 'So'],
        weekHeader: 'Tydz',
        dateFormat: 'd M', firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.regional['id-ID'] = {
        closeText: 'Tutup',
        prevText: '&#x3C;mundur',
        nextText: 'maju&#x3E;',
        currentText: 'hari ini',
        monthNames: ['Januari','Februari','Maret','April','Mei','Juni',
            'Juli','Agustus','September','Oktober','Nopember','Desember'],
        monthNamesShort: ['Jan','Feb','Mar','Apr','Mei','Jun',
            'Jul','Agus','Sep','Okt','Nop','Des'],
        dayNames: ['Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu'],
        dayNamesShort: ['Min','Sen','Sel','Rab','kam','Jum','Sab'],
        dayNamesMin: ['Mg','Sn','Sl','Rb','Km','jm','Sb'],
        weekHeader: 'Mg',
        dateFormat: 'dd/mm',
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };

    $.datepicker.setDefaults($.datepicker.regional['en-US']);

})(jQuery, _, hlf);
;(function ($, _, hlf) {
    'use strict';
    hlf.noDates = function (config) {

        var $c = null,
            $chw = null,
            $ch = null,

            controls = {};

        config = _.defaults(config || {}, {
            name: 'unknownDates', // getParams() param name
            text: 'Checkbox',
            goalChange: {},
            onChange: function() {}, // fires on state change
            onOn: function() {}, // fires when checkbox set on
            onOff: function() {}, // fires when checkbox set off
            calendars: [], // calendar control names list
            tplInput: hlf.getTpl('noDates.input')
        });

        /**
         * Returns (if possible) this control value as string to use in URL
         * @returns {object}
         */
        function getParams() {
            var r = {};
            if($ch.is(':checked')) {
                r[config.name] = 1;
            }
            return r;
        }

        /**
         * Draws control in DOM
         * @param name string [hlf-name] container param
         * @param $f DOM element like context, usually it's <form/> or <div/>
         * @param c list of all form controls
         */
        return function (name, $f, c, ti) {

            controls = c || {};
            config.tabIndex = ti || 0;

            $c = hlf.getContainer($f, 'noDates', name);
            $c.html(config.tplInput(config));

            $chw = hlf.getEl($c, 'noDates-input-wrap');
            $ch = hlf.getEl($c, 'noDates-input');

            $ch.on('change', function(e) {
                _.each(config.calendars, function(name) {
                    e.target.checked ? controls[name].disable() : controls[name].enable();
                });
                config.onChange(e);
                hlf.goal(config.goalChange, { // reach change goal
                    checked: e.target.checked
                });
                e.target.checked ? config.onOn(e) : config.onOff(e);
            });

            $ch.on('focus', function() {
                $chw.addClass('hlf-state--focus');
            });

            $ch.on('blur', function() {
                $chw.removeClass('hlf-state--focus');
            });

            return {
                config: config,
                getParams: getParams
            };

        };

    };
})(jQuery, _, hlf);
;(function ($, _, hlf) {
    'use strict';
    hlf.guests = function (config) {

        var $c = null, // container
            $doc = null, // document
            $g = null, // guests container
            $s = null, // summary
            $cc = null, // controls container
            $av = null, // adults value
            $at = null, // adults title
            $ai = null, // adults increment control
            $ad = null, // adults decrement control
            $cv = null, // children value
            $ct = null, // children title
            $ci = null, // children increment control
            $cd = null, // children decrement control
            $cl = null, // children list
            $clt = '', // children list title
            $chc = [], // child containers
            $chi = [], // child age increment control
            $chd = [], // child age decrement control
            $cha = [], // child age value

            controls = {};

        config = _.defaults(config || {}, {

            adults: 2, // adults default value
            adultsMax: 4,
            adultsMin: 1,
            children: [], // children age
            childrenMax: 3,
            childMaxAge: 17,
            childDefaultAge: 7,
            summary: function(adults, children) {
                return (adults + children.length);
            },
            adultsTitle: function(adults) {
                return 'Adults';
            },
            childrenTitle: function(children) {
                return 'Children';
            },
            childAge: 'Age',
            childHint: 'Check da age!',
            childrenListTitle: 'It is children list',
            childValSep: false,

            titlesPosInside: false, // are titles inside 'val' container or not
            decControlContent: '&minus;',
            incControlContent: '&plus;',
            decControlContentChld: '&minus;',
            incControlContentChld: '&plus;',

            goalOpen: {},

            tplContainer: hlf.getTpl('guests.container'),
            tplChild: hlf.getTpl('guests.child')

        });

        /**
         * Returns (if possible) this control value as string to use in URL
         * @returns {object}
         */
        function getParams() {
            var r = {
                'adults': config.adults
            };
            if(config.children.length) {
                r['children'] = config.children.join(',');
            }
            return r;
        }


        function summaryClick() {
            $g.hasClass('hlf-state--closed') ? guestsOpen() : guestsClose();
            return false;
        }

        function guestsClose() {
            $g.addClass('hlf-state--closed');
            $g.removeClass('hlf-state--focus');
            $c.removeClass('hlf-state--focus');
        }

        function guestsOpen() {
            hlf.goal(config.goalOpen);
            $g.removeClass('hlf-state--closed');
            $g.addClass('hlf-state--focus');
            $c.addClass('hlf-state--focus');
        }

        function drawChild(key) {
            if (config.children[key]===null) {
                config.children[key] = config.childDefaultAge;
            }
            $cl.append(config.tplChild({
                key: key,
                age: config.children[key],
                title: config.childAge,
                hint: config.childHint,
                childValSep: config.childValSep,
                decControlContent: config.decControlContent,
                incControlContent: config.incControlContent,
                decControlContentChld: config.decControlContentChld,
                incControlContentChld: config.incControlContentChld

            }));

            $chc[key] = hlf.getEl($cl, 'child-container', key);
            $chi[key] = hlf.getEl($chc[key], 'child-age-increment');
            $cha[key] = hlf.getEl($chc[key], 'child-age');
            $chd[key] = hlf.getEl($chc[key], 'child-age-decrement');

            $chi[key].on('click', {operation: 1}, newAge);
            $chd[key].on('click', {operation: -1}, newAge);

            function newAge(e){
                var newValue;
                newValue = config.children[key] + e.data.operation;
                if ( newValue>config.childMaxAge || newValue<0){
                    return false
                }
                config.children[key] = newValue;
                $cha[key][0].textContent = newValue;
                return false;
            }
        }

        function update() {
            $s.html(config.summary(config.adults, config.children));
            $av.html(config.adults);
            $cv.html(config.children.length);
            $at.html(config.adultsTitle(config.adults));
            $ct.html(config.childrenTitle(config.children));

            if (config.children.length){
                $cl.removeClass('hlf-state--empty')
                $clt.removeClass('hlf-state--disabled');
            } else {
                $cl.addClass('hlf-state--empty');
                $clt.addClass('hlf-state--disabled');
            }

            config.children.length == config.childrenMax
                ? $ci.addClass('hlf-state--disabled')
                : $ci.removeClass('hlf-state--disabled');

            !config.children.length
                ? $cd.addClass('hlf-state--disabled')
                : $cd.removeClass('hlf-state--disabled');

            config.adults == config.adultsMax
                ? $ai.addClass('hlf-state--disabled')
                : $ai.removeClass('hlf-state--disabled');

            config.adults == config.adultsMin
                ? $ad.addClass('hlf-state--disabled')
                : $ad.removeClass('hlf-state--disabled');

        }

        /**
         * Draw in DOM
         * @param name string [hlf-name] container param
         * @param $f DOM element like context, usually it's <form/> or <div/>
         * @param c list of all form controls
         * @param ti tabIndex value
         */
        return function (name, $f, c, ti) {

            controls = c || {};
            config.tabIndex = ti || 0;

            $doc = $(document);
            $c = hlf.getContainer($f, 'guests', name);
            $c.html(config.tplContainer(config));
            $g = hlf.getEl($c, 'guests');
            $s = hlf.getEl($c, 'summary');
            $cc = hlf.getEl($c, 'controls');
            $av = hlf.getEl($c, 'adults-val');
            $at = hlf.getEl($c, 'adults-title');
            $ad = hlf.getEl($c, 'adults-decrement');
            $ai = hlf.getEl($c, 'adults-increment');
            $cv = hlf.getEl($c, 'children-val');
            $ct = hlf.getEl($c, 'children-title');
            $cd = hlf.getEl($c, 'children-decrement');
            $ci = hlf.getEl($c, 'children-increment');
            $cl = hlf.getEl($c, 'children-list');
            $clt = hlf.getEl($c, 'children-list-title');
            config.className&&$g.addClass(config.className);
            _.each(config.children, function(v, key) {
                drawChild(key);
            });
            update();

            $doc.on('click', function(ev) { // todo check if this own block (multi controls problem)
                if(!$(ev.target).closest('[hlf-role=guests]').length) {
                    guestsClose();
                }
                ev.stopPropagation();
            });

            $s.on('click', summaryClick);

            $s.on('keydown', function(e) {
                switch(e.keyCode) {
                    case 38:
                        guestsClose();
                        break;
                    case 40:
                        guestsOpen();
                        break;
                    default: break;
                }
            });

            $s.on('focus', function() {
                $s.addClass('hlf-state--focus');
                $c.addClass('hlf-state--focus');
            });

            $s.on('blur', function() {
                $s.removeClass('hlf-state--focus');
                $c.removeClass('hlf-state--focus');
            });

            $ai.on('click', function() {
                if(config.adults == config.adultsMax) {
                    return false;
                }
                config.adults++;
                update();
                return false;
            });

            $ad.on('click', function() {
                if(config.adults == config.adultsMin) {
                    return false;
                }
                config.adults--;
                update();
                return false;
            });

            $ci.on('click', function() {
                if(config.children.length == config.childrenMax) {
                    return false;
                }
                config.children.push(null);
                drawChild(config.children.length - 1);
                update();
                return false;
            });

            $cd.on('click', function() {
                if(config.children.length == 0) {
                    return false;
                }
                $chc[config.children.length - 1].remove();
                config.children.pop();
                $chc.pop();
                $chi.pop();
                update();
                return false;
            });

            return {
                config: config,
                getParams: getParams,
            };

        };

    };
})(jQuery, _, hlf);
;(function ($, _, hlf) {
    'use strict';

    hlf.submit = function (config) {

        var $c = null, // container
            $b = null, // button

            controls = {};

        config = _.defaults(config || {}, {

            text: 'Submit',

            goalClick: {}, // same as everywhere

            tplButton: hlf.getTpl('submit.button')

        });

        return function (name, $f, c, ti) {

            controls = c || {};
            config.tabIndex = ti || 0;

            $c = hlf.getContainer($f, 'submit', name);
            $c.html(config.tplButton(config));

            $b = hlf.getEl($c, 'button');
            config.className&&$b.addClass(config.className);
            $b.on('click', function() {
                hlf.goal(config.goalClick);
            });

            return {
                config: config
            };

        };

    };

})(jQuery, _, hlf);
/*! nouislider - 8.0.2 - 2015-07-06 13:22:09 */

!function(a){if("function"==typeof define&&define.amd)define([],a);else if("object"==typeof exports){var b=require("fs");module.exports=a(),module.exports.css=function(){return b.readFileSync(__dirname+"/nouislider.min.css","utf8")}}else window.noUiSlider=a()}(function(){"use strict";function a(a){return a.filter(function(a){return this[a]?!1:this[a]=!0},{})}function b(a,b){return Math.round(a/b)*b}function c(a){var b=a.getBoundingClientRect(),c=a.ownerDocument,d=c.defaultView||c.parentWindow,e=c.documentElement,f=d.pageXOffset;return/webkit.*Chrome.*Mobile/i.test(navigator.userAgent)&&(f=0),{top:b.top+d.pageYOffset-e.clientTop,left:b.left+f-e.clientLeft}}function d(a){return"number"==typeof a&&!isNaN(a)&&isFinite(a)}function e(a){var b=Math.pow(10,7);return Number((Math.round(a*b)/b).toFixed(7))}function f(a,b,c){j(a,b),setTimeout(function(){k(a,b)},c)}function g(a){return Math.max(Math.min(a,100),0)}function h(a){return Array.isArray(a)?a:[a]}function i(a){var b=a.split(".");return b.length>1?b[1].length:0}function j(a,b){a.classList?a.classList.add(b):a.className+=" "+b}function k(a,b){a.classList?a.classList.remove(b):a.className=a.className.replace(new RegExp("(^|\\b)"+b.split(" ").join("|")+"(\\b|$)","gi")," ")}function l(a,b){a.classList?a.classList.contains(b):new RegExp("(^| )"+b+"( |$)","gi").test(a.className)}function m(a,b){return 100/(b-a)}function n(a,b){return 100*b/(a[1]-a[0])}function o(a,b){return n(a,a[0]<0?b+Math.abs(a[0]):b-a[0])}function p(a,b){return b*(a[1]-a[0])/100+a[0]}function q(a,b){for(var c=1;a>=b[c];)c+=1;return c}function r(a,b,c){if(c>=a.slice(-1)[0])return 100;var d,e,f,g,h=q(c,a);return d=a[h-1],e=a[h],f=b[h-1],g=b[h],f+o([d,e],c)/m(f,g)}function s(a,b,c){if(c>=100)return a.slice(-1)[0];var d,e,f,g,h=q(c,b);return d=a[h-1],e=a[h],f=b[h-1],g=b[h],p([d,e],(c-f)*m(f,g))}function t(a,c,d,e){if(100===e)return e;var f,g,h=q(e,a);return d?(f=a[h-1],g=a[h],e-f>(g-f)/2?g:f):c[h-1]?a[h-1]+b(e-a[h-1],c[h-1]):e}function u(a,b,c){var e;if("number"==typeof b&&(b=[b]),"[object Array]"!==Object.prototype.toString.call(b))throw new Error("noUiSlider: 'range' contains invalid value.");if(e="min"===a?0:"max"===a?100:parseFloat(a),!d(e)||!d(b[0]))throw new Error("noUiSlider: 'range' value isn't numeric.");c.xPct.push(e),c.xVal.push(b[0]),e?c.xSteps.push(isNaN(b[1])?!1:b[1]):isNaN(b[1])||(c.xSteps[0]=b[1])}function v(a,b,c){return b?void(c.xSteps[a]=n([c.xVal[a],c.xVal[a+1]],b)/m(c.xPct[a],c.xPct[a+1])):!0}function w(a,b,c,d){this.xPct=[],this.xVal=[],this.xSteps=[d||!1],this.xNumSteps=[!1],this.snap=b,this.direction=c;var e,f=[];for(e in a)a.hasOwnProperty(e)&&f.push([a[e],e]);for(f.sort(function(a,b){return a[0]-b[0]}),e=0;e<f.length;e++)u(f[e][1],f[e][0],this);for(this.xNumSteps=this.xSteps.slice(0),e=0;e<this.xNumSteps.length;e++)v(e,this.xNumSteps[e],this)}function x(a,b){if(!d(b))throw new Error("noUiSlider: 'step' is not numeric.");a.singleStep=b}function y(a,b){if("object"!=typeof b||Array.isArray(b))throw new Error("noUiSlider: 'range' is not an object.");if(void 0===b.min||void 0===b.max)throw new Error("noUiSlider: Missing 'min' or 'max' in 'range'.");a.spectrum=new w(b,a.snap,a.dir,a.singleStep)}function z(a,b){if(b=h(b),!Array.isArray(b)||!b.length||b.length>2)throw new Error("noUiSlider: 'start' option is incorrect.");a.handles=b.length,a.start=b}function A(a,b){if(a.snap=b,"boolean"!=typeof b)throw new Error("noUiSlider: 'snap' option must be a boolean.")}function B(a,b){if(a.animate=b,"boolean"!=typeof b)throw new Error("noUiSlider: 'animate' option must be a boolean.")}function C(a,b){if("lower"===b&&1===a.handles)a.connect=1;else if("upper"===b&&1===a.handles)a.connect=2;else if(b===!0&&2===a.handles)a.connect=3;else{if(b!==!1)throw new Error("noUiSlider: 'connect' option doesn't match handle count.");a.connect=0}}function D(a,b){switch(b){case"horizontal":a.ort=0;break;case"vertical":a.ort=1;break;default:throw new Error("noUiSlider: 'orientation' option is invalid.")}}function E(a,b){if(!d(b))throw new Error("noUiSlider: 'margin' option must be numeric.");if(a.margin=a.spectrum.getMargin(b),!a.margin)throw new Error("noUiSlider: 'margin' option is only supported on linear sliders.")}function F(a,b){if(!d(b))throw new Error("noUiSlider: 'limit' option must be numeric.");if(a.limit=a.spectrum.getMargin(b),!a.limit)throw new Error("noUiSlider: 'limit' option is only supported on linear sliders.")}function G(a,b){switch(b){case"ltr":a.dir=0;break;case"rtl":a.dir=1,a.connect=[0,2,1,3][a.connect];break;default:throw new Error("noUiSlider: 'direction' option was not recognized.")}}function H(a,b){if("string"!=typeof b)throw new Error("noUiSlider: 'behaviour' must be a string containing options.");var c=b.indexOf("tap")>=0,d=b.indexOf("drag")>=0,e=b.indexOf("fixed")>=0,f=b.indexOf("snap")>=0;a.events={tap:c||f,drag:d,fixed:e,snap:f}}function I(a,b){if(a.format=b,"function"==typeof b.to&&"function"==typeof b.from)return!0;throw new Error("noUiSlider: 'format' requires 'to' and 'from' methods.")}function J(a){var b,c={margin:0,limit:0,animate:!0,format:U};b={step:{r:!1,t:x},start:{r:!0,t:z},connect:{r:!0,t:C},direction:{r:!0,t:G},snap:{r:!1,t:A},animate:{r:!1,t:B},range:{r:!0,t:y},orientation:{r:!1,t:D},margin:{r:!1,t:E},limit:{r:!1,t:F},behaviour:{r:!0,t:H},format:{r:!1,t:I}};var d={connect:!1,direction:"ltr",behaviour:"tap",orientation:"horizontal"};return Object.keys(d).forEach(function(b){void 0===a[b]&&(a[b]=d[b])}),Object.keys(b).forEach(function(d){var e=b[d];if(void 0===a[d]){if(e.r)throw new Error("noUiSlider: '"+d+"' is required.");return!0}e.t(c,a[d])}),c.pips=a.pips,c.style=c.ort?"top":"left",c}function K(a,b,c){var d=a+b[0],e=a+b[1];return c?(0>d&&(e+=Math.abs(d)),e>100&&(d-=e-100),[g(d),g(e)]):[d,e]}function L(a){a.preventDefault();var b,c,d=0===a.type.indexOf("touch"),e=0===a.type.indexOf("mouse"),f=0===a.type.indexOf("pointer"),g=a;return 0===a.type.indexOf("MSPointer")&&(f=!0),d&&(b=a.changedTouches[0].pageX,c=a.changedTouches[0].pageY),(e||f)&&(b=a.clientX+window.pageXOffset,c=a.clientY+window.pageYOffset),g.points=[b,c],g.cursor=e||f,g}function M(a,b){var c=document.createElement("div"),d=document.createElement("div"),e=["-lower","-upper"];return a&&e.reverse(),j(d,T[3]),j(d,T[3]+e[b]),j(c,T[2]),c.appendChild(d),c}function N(a,b,c){switch(a){case 1:j(b,T[7]),j(c[0],T[6]);break;case 3:j(c[1],T[6]);case 2:j(c[0],T[7]);case 0:j(b,T[6])}}function O(a,b,c){var d,e=[];for(d=0;a>d;d+=1)e.push(c.appendChild(M(b,d)));return e}function P(a,b,c){j(c,T[0]),j(c,T[8+a]),j(c,T[4+b]);var d=document.createElement("div");return j(d,T[1]),c.appendChild(d),d}function Q(b,d){function e(a,b,c){if("range"===a||"steps"===a)return M.xVal;if("count"===a){var d,e=100/(b-1),f=0;for(b=[];(d=f++*e)<=100;)b.push(d);a="positions"}return"positions"===a?b.map(function(a){return M.fromStepping(c?M.getStep(a):a)}):"values"===a?c?b.map(function(a){return M.fromStepping(M.getStep(M.toStepping(a)))}):b:void 0}function m(b,c,d){var e=M.direction,f={},g=M.xVal[0],h=M.xVal[M.xVal.length-1],i=!1,j=!1,k=0;return M.direction=0,d=a(d.slice().sort(function(a,b){return a-b})),d[0]!==g&&(d.unshift(g),i=!0),d[d.length-1]!==h&&(d.push(h),j=!0),d.forEach(function(a,e){var g,h,l,m,n,o,p,q,r,s,t=a,u=d[e+1];if("steps"===c&&(g=M.xNumSteps[e]),g||(g=u-t),t!==!1&&void 0!==u)for(h=t;u>=h;h+=g){for(m=M.toStepping(h),n=m-k,q=n/b,r=Math.round(q),s=n/r,l=1;r>=l;l+=1)o=k+l*s,f[o.toFixed(5)]=["x",0];p=d.indexOf(h)>-1?1:"steps"===c?2:0,!e&&i&&(p=0),h===u&&j||(f[m.toFixed(5)]=[h,p]),k=m}}),M.direction=e,f}function n(a,b,c){function e(a){return["-normal","-large","-sub"][a]}function f(a,b,c){return'class="'+b+" "+b+"-"+h+" "+b+e(c[1])+'" style="'+d.style+": "+a+'%"'}function g(a,d){M.direction&&(a=100-a),d[1]=d[1]&&b?b(d[0],d[1]):d[1],i.innerHTML+="<div "+f(a,"noUi-marker",d)+"></div>",d[1]&&(i.innerHTML+="<div "+f(a,"noUi-value",d)+">"+c.to(d[0])+"</div>")}var h=["horizontal","vertical"][d.ort],i=document.createElement("div");return j(i,"noUi-pips"),j(i,"noUi-pips-"+h),Object.keys(a).forEach(function(b){g(b,a[b])}),i}function o(a){var b=a.mode,c=a.density||1,d=a.filter||!1,f=a.values||!1,g=a.stepped||!1,h=e(b,f,g),i=m(c,b,h),j=a.format||{to:Math.round};return I.appendChild(n(i,d,j))}function p(){return G["offset"+["Width","Height"][d.ort]]}function q(a,b){void 0!==b&&(b=Math.abs(b-d.dir)),Object.keys(R).forEach(function(c){var d=c.split(".")[0];a===d&&R[c].forEach(function(a){a(h(B()),b,r(Array.prototype.slice.call(Q)))})})}function r(a){return 1===a.length?a[0]:d.dir?a.reverse():a}function s(a,b,c,e){var f=function(b){return I.hasAttribute("disabled")?!1:l(I,T[14])?!1:(b=L(b),a===S.start&&void 0!==b.buttons&&b.buttons>1?!1:(b.calcPoint=b.points[d.ort],void c(b,e)))},g=[];return a.split(" ").forEach(function(a){b.addEventListener(a,f,!1),g.push([a,f])}),g}function t(a,b){var c,d,e=b.handles||H,f=!1,g=100*(a.calcPoint-b.start)/p(),h=e[0]===H[0]?0:1;if(c=K(g,b.positions,e.length>1),f=y(e[0],c[h],1===e.length),e.length>1){if(f=y(e[1],c[h?0:1],!1)||f)for(d=0;d<b.handles.length;d++)q("slide",d)}else f&&q("slide",h)}function u(a,b){var c=G.getElementsByClassName(T[15]),d=b.handles[0]===H[0]?0:1;c.length&&k(c[0],T[15]),a.cursor&&(document.body.style.cursor="",document.body.removeEventListener("selectstart",document.body.noUiListener));var e=document.documentElement;e.noUiListeners.forEach(function(a){e.removeEventListener(a[0],a[1])}),k(I,T[12]),q("set",d),q("change",d)}function v(a,b){var c=document.documentElement;if(1===b.handles.length&&(j(b.handles[0].children[0],T[15]),b.handles[0].hasAttribute("disabled")))return!1;a.stopPropagation();var d=s(S.move,c,t,{start:a.calcPoint,handles:b.handles,positions:[J[0],J[H.length-1]]}),e=s(S.end,c,u,{handles:b.handles});if(c.noUiListeners=d.concat(e),a.cursor){document.body.style.cursor=getComputedStyle(a.target).cursor,H.length>1&&j(I,T[12]);var f=function(){return!1};document.body.noUiListener=f,document.body.addEventListener("selectstart",f,!1)}}function w(a){var b,e,g=a.calcPoint,h=0;return a.stopPropagation(),H.forEach(function(a){h+=c(a)[d.style]}),b=h/2>g||1===H.length?0:1,g-=c(G)[d.style],e=100*g/p(),d.events.snap||f(I,T[14],300),H[b].hasAttribute("disabled")?!1:(y(H[b],e),q("slide",b),q("set",b),q("change",b),void(d.events.snap&&v(a,{handles:[H[h]]})))}function x(a){var b,c;if(!a.fixed)for(b=0;b<H.length;b+=1)s(S.start,H[b].children[0],v,{handles:[H[b]]});a.tap&&s(S.start,G,w,{handles:H}),a.drag&&(c=[G.getElementsByClassName(T[7])[0]],j(c[0],T[10]),a.fixed&&c.push(H[c[0]===H[0]?1:0].children[0]),c.forEach(function(a){s(S.start,a,v,{handles:H})}))}function y(a,b,c){var e=a!==H[0]?1:0,f=J[0]+d.margin,h=J[1]-d.margin,i=J[0]+d.limit,l=J[1]-d.limit;return H.length>1&&(b=e?Math.max(b,f):Math.min(b,h)),c!==!1&&d.limit&&H.length>1&&(b=e?Math.min(b,i):Math.max(b,l)),b=M.getStep(b),b=g(parseFloat(b.toFixed(7))),b===J[e]?!1:(a.style[d.style]=b+"%",a.previousSibling||(k(a,T[17]),b>50&&j(a,T[17])),J[e]=b,Q[e]=M.fromStepping(b),q("update",e),!0)}function z(a,b){var c,e,f;for(d.limit&&(a+=1),c=0;a>c;c+=1)e=c%2,f=b[e],null!==f&&f!==!1&&("number"==typeof f&&(f=String(f)),f=d.format.from(f),(f===!1||isNaN(f)||y(H[e],M.toStepping(f),c===3-d.dir)===!1)&&q("update",e))}function A(a){var b,c,e=h(a);for(d.dir&&d.handles>1&&e.reverse(),d.animate&&-1!==J[0]&&f(I,T[14],300),b=H.length>1?3:1,1===e.length&&(b=1),z(b,e),c=0;c<H.length;c++)q("set",c)}function B(){var a,b=[];for(a=0;a<d.handles;a+=1)b[a]=d.format.to(Q[a]);return r(b)}function C(){T.forEach(function(a){a&&k(I,a)}),I.innerHTML="",delete I.noUiSlider}function D(){var a=J.map(function(a,b){var c=M.getApplicableStep(a),d=i(String(c[2])),e=Q[b],f=100===a?null:c[2],g=Number((e-c[2]).toFixed(d)),h=0===a?null:g>=c[1]?c[2]:c[0]||!1;return[h,f]});return r(a)}function E(a,b){R[a]=R[a]||[],R[a].push(b),"update"===a.split(".")[0]&&H.forEach(function(a,b){q("update",b)})}function F(a){var b=a.split(".")[0],c=a.substring(b.length);Object.keys(R).forEach(function(a){var d=a.split(".")[0],e=a.substring(d.length);b&&b!==d||c&&c!==e||delete R[a]})}var G,H,I=b,J=[-1,-1],M=d.spectrum,Q=[],R={};if(I.noUiSlider)throw new Error("Slider was already initialized.");return G=P(d.dir,d.ort,I),H=O(d.handles,d.dir,G),N(d.connect,I,H),x(d.events),d.pips&&o(d.pips),{destroy:C,steps:D,on:E,off:F,get:B,set:A}}function R(a,b){if(!a.nodeName)throw new Error("noUiSlider.create requires a single element.");var c=J(b,a),d=Q(a,c);d.set(c.start),a.noUiSlider=d}var S=window.navigator.pointerEnabled?{start:"pointerdown",move:"pointermove",end:"pointerup"}:window.navigator.msPointerEnabled?{start:"MSPointerDown",move:"MSPointerMove",end:"MSPointerUp"}:{start:"mousedown touchstart",move:"mousemove touchmove",end:"mouseup touchend"},T=["noUi-target","noUi-base","noUi-origin","noUi-handle","noUi-horizontal","noUi-vertical","noUi-background","noUi-connect","noUi-ltr","noUi-rtl","noUi-dragable","","noUi-state-drag","","noUi-state-tap","noUi-active","","noUi-stacking"];w.prototype.getMargin=function(a){return 2===this.xPct.length?n(this.xVal,a):!1},w.prototype.toStepping=function(a){return a=r(this.xVal,this.xPct,a),this.direction&&(a=100-a),a},w.prototype.fromStepping=function(a){return this.direction&&(a=100-a),e(s(this.xVal,this.xPct,a))},w.prototype.getStep=function(a){return this.direction&&(a=100-a),a=t(this.xPct,this.xSteps,this.snap,a),this.direction&&(a=100-a),a},w.prototype.getApplicableStep=function(a){var b=q(a,this.xPct),c=100===a?2:1;return[this.xNumSteps[b-2],this.xVal[b-c],this.xNumSteps[b-c]]},w.prototype.convert=function(a){return this.getStep(this.toStepping(a))};var U={to:function(a){return a.toFixed(2)},from:Number};return{create:R}});
(function(){function r(b){return b.split("").reverse().join("")}function s(b,f,c){if((b[f]||b[c])&&b[f]===b[c])throw Error(f);}function v(b,f,c,d,e,p,q,k,l,h,n,a){q=a;var m,g=n="";p&&(a=p(a));if("number"!==typeof a||!isFinite(a))return!1;b&&0===parseFloat(a.toFixed(b))&&(a=0);0>a&&(m=!0,a=Math.abs(a));b&&(p=Math.pow(10,b),a=(Math.round(a*p)/p).toFixed(b));a=a.toString();-1!==a.indexOf(".")&&(b=a.split("."),a=b[0],c&&(n=c+b[1]));f&&(a=r(a).match(/.{1,3}/g),a=r(a.join(r(f))));m&&k&&(g+=k);d&&(g+=d);
m&&l&&(g+=l);g=g+a+n;e&&(g+=e);h&&(g=h(g,q));return g}function w(b,f,c,d,e,h,q,k,l,r,n,a){var m;b="";n&&(a=n(a));if(!a||"string"!==typeof a)return!1;k&&a.substring(0,k.length)===k&&(a=a.replace(k,""),m=!0);d&&a.substring(0,d.length)===d&&(a=a.replace(d,""));l&&a.substring(0,l.length)===l&&(a=a.replace(l,""),m=!0);e&&a.slice(-1*e.length)===e&&(a=a.slice(0,-1*e.length));f&&(a=a.split(f).join(""));c&&(a=a.replace(c,"."));m&&(b+="-");b=Number((b+a).replace(/[^0-9\.\-.]/g,""));q&&(b=q(b));return"number"===
typeof b&&isFinite(b)?b:!1}function x(b){var f,c,d,e={};for(f=0;f<h.length;f+=1)c=h[f],d=b[c],void 0===d?e[c]="negative"!==c||e.negativeBefore?"mark"===c&&"."!==e.thousand?".":!1:"-":"decimals"===c?0<d&&8>d&&(e[c]=d):"encoder"===c||"decoder"===c||"edit"===c||"undo"===c?"function"===typeof d&&(e[c]=d):"string"===typeof d&&(e[c]=d);s(e,"mark","thousand");s(e,"prefix","negative");s(e,"prefix","negativeBefore");return e}function u(b,f,c){var d,e=[];for(d=0;d<h.length;d+=1)e.push(b[h[d]]);e.push(c);return f.apply("",
e)}function t(b){if(!(this instanceof t))return new t(b);"object"===typeof b&&(b=x(b),this.to=function(f){return u(b,v,f)},this.from=function(f){return u(b,w,f)})}var h="decimals thousand mark prefix postfix encoder decoder negativeBefore negative edit undo".split(" ");window.wNumb=t})();

!function(a,b,c,d){function e(b,c){this.settings=null,this.options=a.extend({},e.Defaults,c),this.$element=a(b),this.drag=a.extend({},m),this.state=a.extend({},n),this.e=a.extend({},o),this._plugins={},this._supress={},this._current=null,this._speed=null,this._coordinates=[],this._breakpoint=null,this._width=null,this._items=[],this._clones=[],this._mergers=[],this._invalidated={},this._pipe=[],a.each(e.Plugins,a.proxy(function(a,b){this._plugins[a[0].toLowerCase()+a.slice(1)]=new b(this)},this)),a.each(e.Pipe,a.proxy(function(b,c){this._pipe.push({filter:c.filter,run:a.proxy(c.run,this)})},this)),this.setup(),this.initialize()}function f(a){if(a.touches!==d)return{x:a.touches[0].pageX,y:a.touches[0].pageY};if(a.touches===d){if(a.pageX!==d)return{x:a.pageX,y:a.pageY};if(a.pageX===d)return{x:a.clientX,y:a.clientY}}}function g(a){var b,d,e=c.createElement("div"),f=a;for(b in f)if(d=f[b],"undefined"!=typeof e.style[d])return e=null,[d,b];return[!1]}function h(){return g(["transition","WebkitTransition","MozTransition","OTransition"])[1]}function i(){return g(["transform","WebkitTransform","MozTransform","OTransform","msTransform"])[0]}function j(){return g(["perspective","webkitPerspective","MozPerspective","OPerspective","MsPerspective"])[0]}function k(){return"ontouchstart"in b||!!navigator.msMaxTouchPoints}function l(){return b.navigator.msPointerEnabled}var m,n,o;m={start:0,startX:0,startY:0,current:0,currentX:0,currentY:0,offsetX:0,offsetY:0,distance:null,startTime:0,endTime:0,updatedX:0,targetEl:null},n={isTouch:!1,isScrolling:!1,isSwiping:!1,direction:!1,inMotion:!1},o={_onDragStart:null,_onDragMove:null,_onDragEnd:null,_transitionEnd:null,_resizer:null,_responsiveCall:null,_goToLoop:null,_checkVisibile:null},e.Defaults={items:3,loop:!1,center:!1,mouseDrag:!0,touchDrag:!0,pullDrag:!0,freeDrag:!1,margin:0,stagePadding:0,merge:!1,mergeFit:!0,autoWidth:!1,startPosition:0,rtl:!1,smartSpeed:250,fluidSpeed:!1,dragEndSpeed:!1,responsive:{},responsiveRefreshRate:200,responsiveBaseElement:b,responsiveClass:!1,fallbackEasing:"swing",info:!1,nestedItemSelector:!1,itemElement:"div",stageElement:"div",themeClass:"owl-theme",baseClass:"owl-carousel",itemClass:"owl-item",centerClass:"center",activeClass:"active"},e.Width={Default:"default",Inner:"inner",Outer:"outer"},e.Plugins={},e.Pipe=[{filter:["width","items","settings"],run:function(a){a.current=this._items&&this._items[this.relative(this._current)]}},{filter:["items","settings"],run:function(){var a=this._clones,b=this.$stage.children(".cloned");(b.length!==a.length||!this.settings.loop&&a.length>0)&&(this.$stage.children(".cloned").remove(),this._clones=[])}},{filter:["items","settings"],run:function(){var a,b,c=this._clones,d=this._items,e=this.settings.loop?c.length-Math.max(2*this.settings.items,4):0;for(a=0,b=Math.abs(e/2);b>a;a++)e>0?(this.$stage.children().eq(d.length+c.length-1).remove(),c.pop(),this.$stage.children().eq(0).remove(),c.pop()):(c.push(c.length/2),this.$stage.append(d[c[c.length-1]].clone().addClass("cloned")),c.push(d.length-1-(c.length-1)/2),this.$stage.prepend(d[c[c.length-1]].clone().addClass("cloned")))}},{filter:["width","items","settings"],run:function(){var a,b,c,d=this.settings.rtl?1:-1,e=(this.width()/this.settings.items).toFixed(3),f=0;for(this._coordinates=[],b=0,c=this._clones.length+this._items.length;c>b;b++)a=this._mergers[this.relative(b)],a=this.settings.mergeFit&&Math.min(a,this.settings.items)||a,f+=(this.settings.autoWidth?this._items[this.relative(b)].width()+this.settings.margin:e*a)*d,this._coordinates.push(f)}},{filter:["width","items","settings"],run:function(){var b,c,d=(this.width()/this.settings.items).toFixed(3),e={width:Math.abs(this._coordinates[this._coordinates.length-1])+2*this.settings.stagePadding,"padding-left":this.settings.stagePadding||"","padding-right":this.settings.stagePadding||""};if(this.$stage.css(e),e={width:this.settings.autoWidth?"auto":d-this.settings.margin},e[this.settings.rtl?"margin-left":"margin-right"]=this.settings.margin,!this.settings.autoWidth&&a.grep(this._mergers,function(a){return a>1}).length>0)for(b=0,c=this._coordinates.length;c>b;b++)e.width=Math.abs(this._coordinates[b])-Math.abs(this._coordinates[b-1]||0)-this.settings.margin,this.$stage.children().eq(b).css(e);else this.$stage.children().css(e)}},{filter:["width","items","settings"],run:function(a){a.current&&this.reset(this.$stage.children().index(a.current))}},{filter:["position"],run:function(){this.animate(this.coordinates(this._current))}},{filter:["width","position","items","settings"],run:function(){var a,b,c,d,e=this.settings.rtl?1:-1,f=2*this.settings.stagePadding,g=this.coordinates(this.current())+f,h=g+this.width()*e,i=[];for(c=0,d=this._coordinates.length;d>c;c++)a=this._coordinates[c-1]||0,b=Math.abs(this._coordinates[c])+f*e,(this.op(a,"<=",g)&&this.op(a,">",h)||this.op(b,"<",g)&&this.op(b,">",h))&&i.push(c);this.$stage.children("."+this.settings.activeClass).removeClass(this.settings.activeClass),this.$stage.children(":eq("+i.join("), :eq(")+")").addClass(this.settings.activeClass),this.settings.center&&(this.$stage.children("."+this.settings.centerClass).removeClass(this.settings.centerClass),this.$stage.children().eq(this.current()).addClass(this.settings.centerClass))}}],e.prototype.initialize=function(){if(this.trigger("initialize"),this.$element.addClass(this.settings.baseClass).addClass(this.settings.themeClass).toggleClass("owl-rtl",this.settings.rtl),this.browserSupport(),this.settings.autoWidth&&this.state.imagesLoaded!==!0){var b,c,e;if(b=this.$element.find("img"),c=this.settings.nestedItemSelector?"."+this.settings.nestedItemSelector:d,e=this.$element.children(c).width(),b.length&&0>=e)return this.preloadAutoWidthImages(b),!1}this.$element.addClass("owl-loading"),this.$stage=a("<"+this.settings.stageElement+' class="owl-stage"/>').wrap('<div class="owl-stage-outer">'),this.$element.append(this.$stage.parent()),this.replace(this.$element.children().not(this.$stage.parent())),this._width=this.$element.width(),this.refresh(),this.$element.removeClass("owl-loading").addClass("owl-loaded"),this.eventsCall(),this.internalEvents(),this.addTriggerableEvents(),this.trigger("initialized")},e.prototype.setup=function(){var b=this.viewport(),c=this.options.responsive,d=-1,e=null;c?(a.each(c,function(a){b>=a&&a>d&&(d=Number(a))}),e=a.extend({},this.options,c[d]),delete e.responsive,e.responsiveClass&&this.$element.attr("class",function(a,b){return b.replace(/\b owl-responsive-\S+/g,"")}).addClass("owl-responsive-"+d)):e=a.extend({},this.options),(null===this.settings||this._breakpoint!==d)&&(this.trigger("change",{property:{name:"settings",value:e}}),this._breakpoint=d,this.settings=e,this.invalidate("settings"),this.trigger("changed",{property:{name:"settings",value:this.settings}}))},e.prototype.optionsLogic=function(){this.$element.toggleClass("owl-center",this.settings.center),this.settings.loop&&this._items.length<this.settings.items&&(this.settings.loop=!1),this.settings.autoWidth&&(this.settings.stagePadding=!1,this.settings.merge=!1)},e.prototype.prepare=function(b){var c=this.trigger("prepare",{content:b});return c.data||(c.data=a("<"+this.settings.itemElement+"/>").addClass(this.settings.itemClass).append(b)),this.trigger("prepared",{content:c.data}),c.data},e.prototype.update=function(){for(var b=0,c=this._pipe.length,d=a.proxy(function(a){return this[a]},this._invalidated),e={};c>b;)(this._invalidated.all||a.grep(this._pipe[b].filter,d).length>0)&&this._pipe[b].run(e),b++;this._invalidated={}},e.prototype.width=function(a){switch(a=a||e.Width.Default){case e.Width.Inner:case e.Width.Outer:return this._width;default:return this._width-2*this.settings.stagePadding+this.settings.margin}},e.prototype.refresh=function(){if(0===this._items.length)return!1;(new Date).getTime();this.trigger("refresh"),this.setup(),this.optionsLogic(),this.$stage.addClass("owl-refresh"),this.update(),this.$stage.removeClass("owl-refresh"),this.state.orientation=b.orientation,this.watchVisibility(),this.trigger("refreshed")},e.prototype.eventsCall=function(){this.e._onDragStart=a.proxy(function(a){this.onDragStart(a)},this),this.e._onDragMove=a.proxy(function(a){this.onDragMove(a)},this),this.e._onDragEnd=a.proxy(function(a){this.onDragEnd(a)},this),this.e._onResize=a.proxy(function(a){this.onResize(a)},this),this.e._transitionEnd=a.proxy(function(a){this.transitionEnd(a)},this),this.e._preventClick=a.proxy(function(a){this.preventClick(a)},this)},e.prototype.onThrottledResize=function(){b.clearTimeout(this.resizeTimer),this.resizeTimer=b.setTimeout(this.e._onResize,this.settings.responsiveRefreshRate)},e.prototype.onResize=function(){return this._items.length?this._width===this.$element.width()?!1:this.trigger("resize").isDefaultPrevented()?!1:(this._width=this.$element.width(),this.invalidate("width"),this.refresh(),void this.trigger("resized")):!1},e.prototype.eventsRouter=function(a){var b=a.type;"mousedown"===b||"touchstart"===b?this.onDragStart(a):"mousemove"===b||"touchmove"===b?this.onDragMove(a):"mouseup"===b||"touchend"===b?this.onDragEnd(a):"touchcancel"===b&&this.onDragEnd(a)},e.prototype.internalEvents=function(){var c=(k(),l());this.settings.mouseDrag?(this.$stage.on("mousedown",a.proxy(function(a){this.eventsRouter(a)},this)),this.$stage.on("dragstart",function(){return!1}),this.$stage.get(0).onselectstart=function(){return!1}):this.$element.addClass("owl-text-select-on"),this.settings.touchDrag&&!c&&this.$stage.on("touchstart touchcancel",a.proxy(function(a){this.eventsRouter(a)},this)),this.transitionEndVendor&&this.on(this.$stage.get(0),this.transitionEndVendor,this.e._transitionEnd,!1),this.settings.responsive!==!1&&this.on(b,"resize",a.proxy(this.onThrottledResize,this))},e.prototype.onDragStart=function(d){var e,g,h,i;if(e=d.originalEvent||d||b.event,3===e.which||this.state.isTouch)return!1;if("mousedown"===e.type&&this.$stage.addClass("owl-grab"),this.trigger("drag"),this.drag.startTime=(new Date).getTime(),this.speed(0),this.state.isTouch=!0,this.state.isScrolling=!1,this.state.isSwiping=!1,this.drag.distance=0,g=f(e).x,h=f(e).y,this.drag.offsetX=this.$stage.position().left,this.drag.offsetY=this.$stage.position().top,this.settings.rtl&&(this.drag.offsetX=this.$stage.position().left+this.$stage.width()-this.width()+this.settings.margin),this.state.inMotion&&this.support3d)i=this.getTransformProperty(),this.drag.offsetX=i,this.animate(i),this.state.inMotion=!0;else if(this.state.inMotion&&!this.support3d)return this.state.inMotion=!1,!1;this.drag.startX=g-this.drag.offsetX,this.drag.startY=h-this.drag.offsetY,this.drag.start=g-this.drag.startX,this.drag.targetEl=e.target||e.srcElement,this.drag.updatedX=this.drag.start,("IMG"===this.drag.targetEl.tagName||"A"===this.drag.targetEl.tagName)&&(this.drag.targetEl.draggable=!1),a(c).on("mousemove.owl.dragEvents mouseup.owl.dragEvents touchmove.owl.dragEvents touchend.owl.dragEvents",a.proxy(function(a){this.eventsRouter(a)},this))},e.prototype.onDragMove=function(a){var c,e,g,h,i,j;this.state.isTouch&&(this.state.isScrolling||(c=a.originalEvent||a||b.event,e=f(c).x,g=f(c).y,this.drag.currentX=e-this.drag.startX,this.drag.currentY=g-this.drag.startY,this.drag.distance=this.drag.currentX-this.drag.offsetX,this.drag.distance<0?this.state.direction=this.settings.rtl?"right":"left":this.drag.distance>0&&(this.state.direction=this.settings.rtl?"left":"right"),this.settings.loop?this.op(this.drag.currentX,">",this.coordinates(this.minimum()))&&"right"===this.state.direction?this.drag.currentX-=(this.settings.center&&this.coordinates(0))-this.coordinates(this._items.length):this.op(this.drag.currentX,"<",this.coordinates(this.maximum()))&&"left"===this.state.direction&&(this.drag.currentX+=(this.settings.center&&this.coordinates(0))-this.coordinates(this._items.length)):(h=this.coordinates(this.settings.rtl?this.maximum():this.minimum()),i=this.coordinates(this.settings.rtl?this.minimum():this.maximum()),j=this.settings.pullDrag?this.drag.distance/5:0,this.drag.currentX=Math.max(Math.min(this.drag.currentX,h+j),i+j)),(this.drag.distance>8||this.drag.distance<-8)&&(c.preventDefault!==d?c.preventDefault():c.returnValue=!1,this.state.isSwiping=!0),this.drag.updatedX=this.drag.currentX,(this.drag.currentY>16||this.drag.currentY<-16)&&this.state.isSwiping===!1&&(this.state.isScrolling=!0,this.drag.updatedX=this.drag.start),this.animate(this.drag.updatedX)))},e.prototype.onDragEnd=function(b){var d,e,f;if(this.state.isTouch){if("mouseup"===b.type&&this.$stage.removeClass("owl-grab"),this.trigger("dragged"),this.drag.targetEl.removeAttribute("draggable"),this.state.isTouch=!1,this.state.isScrolling=!1,this.state.isSwiping=!1,0===this.drag.distance&&this.state.inMotion!==!0)return this.state.inMotion=!1,!1;this.drag.endTime=(new Date).getTime(),d=this.drag.endTime-this.drag.startTime,e=Math.abs(this.drag.distance),(e>3||d>300)&&this.removeClick(this.drag.targetEl),f=this.closest(this.drag.updatedX),this.speed(this.settings.dragEndSpeed||this.settings.smartSpeed),this.current(f),this.invalidate("position"),this.update(),this.settings.pullDrag||this.drag.updatedX!==this.coordinates(f)||this.transitionEnd(),this.drag.distance=0,a(c).off(".owl.dragEvents")}},e.prototype.removeClick=function(c){this.drag.targetEl=c,a(c).on("click.preventClick",this.e._preventClick),b.setTimeout(function(){a(c).off("click.preventClick")},300)},e.prototype.preventClick=function(b){b.preventDefault?b.preventDefault():b.returnValue=!1,b.stopPropagation&&b.stopPropagation(),a(b.target).off("click.preventClick")},e.prototype.getTransformProperty=function(){var a,c;return a=b.getComputedStyle(this.$stage.get(0),null).getPropertyValue(this.vendorName+"transform"),a=a.replace(/matrix(3d)?\(|\)/g,"").split(","),c=16===a.length,c!==!0?a[4]:a[12]},e.prototype.closest=function(b){var c=-1,d=30,e=this.width(),f=this.coordinates();return this.settings.freeDrag||a.each(f,a.proxy(function(a,g){return b>g-d&&g+d>b?c=a:this.op(b,"<",g)&&this.op(b,">",f[a+1]||g-e)&&(c="left"===this.state.direction?a+1:a),-1===c},this)),this.settings.loop||(this.op(b,">",f[this.minimum()])?c=b=this.minimum():this.op(b,"<",f[this.maximum()])&&(c=b=this.maximum())),c},e.prototype.animate=function(b){this.trigger("translate"),this.state.inMotion=this.speed()>0,this.support3d?this.$stage.css({transform:"translate3d("+b+"px,0px, 0px)",transition:this.speed()/1e3+"s"}):this.state.isTouch?this.$stage.css({left:b+"px"}):this.$stage.animate({left:b},this.speed()/1e3,this.settings.fallbackEasing,a.proxy(function(){this.state.inMotion&&this.transitionEnd()},this))},e.prototype.current=function(a){if(a===d)return this._current;if(0===this._items.length)return d;if(a=this.normalize(a),this._current!==a){var b=this.trigger("change",{property:{name:"position",value:a}});b.data!==d&&(a=this.normalize(b.data)),this._current=a,this.invalidate("position"),this.trigger("changed",{property:{name:"position",value:this._current}})}return this._current},e.prototype.invalidate=function(a){this._invalidated[a]=!0},e.prototype.reset=function(a){a=this.normalize(a),a!==d&&(this._speed=0,this._current=a,this.suppress(["translate","translated"]),this.animate(this.coordinates(a)),this.release(["translate","translated"]))},e.prototype.normalize=function(b,c){var e=c?this._items.length:this._items.length+this._clones.length;return!a.isNumeric(b)||1>e?d:b=this._clones.length?(b%e+e)%e:Math.max(this.minimum(c),Math.min(this.maximum(c),b))},e.prototype.relative=function(a){return a=this.normalize(a),a-=this._clones.length/2,this.normalize(a,!0)},e.prototype.maximum=function(a){var b,c,d,e=0,f=this.settings;if(a)return this._items.length-1;if(!f.loop&&f.center)b=this._items.length-1;else if(f.loop||f.center)if(f.loop||f.center)b=this._items.length+f.items;else{if(!f.autoWidth&&!f.merge)throw"Can not detect maximum absolute position.";for(revert=f.rtl?1:-1,c=this.$stage.width()-this.$element.width();(d=this.coordinates(e))&&!(d*revert>=c);)b=++e}else b=this._items.length-f.items;return b},e.prototype.minimum=function(a){return a?0:this._clones.length/2},e.prototype.items=function(a){return a===d?this._items.slice():(a=this.normalize(a,!0),this._items[a])},e.prototype.mergers=function(a){return a===d?this._mergers.slice():(a=this.normalize(a,!0),this._mergers[a])},e.prototype.clones=function(b){var c=this._clones.length/2,e=c+this._items.length,f=function(a){return a%2===0?e+a/2:c-(a+1)/2};return b===d?a.map(this._clones,function(a,b){return f(b)}):a.map(this._clones,function(a,c){return a===b?f(c):null})},e.prototype.speed=function(a){return a!==d&&(this._speed=a),this._speed},e.prototype.coordinates=function(b){var c=null;return b===d?a.map(this._coordinates,a.proxy(function(a,b){return this.coordinates(b)},this)):(this.settings.center?(c=this._coordinates[b],c+=(this.width()-c+(this._coordinates[b-1]||0))/2*(this.settings.rtl?-1:1)):c=this._coordinates[b-1]||0,c)},e.prototype.duration=function(a,b,c){return Math.min(Math.max(Math.abs(b-a),1),6)*Math.abs(c||this.settings.smartSpeed)},e.prototype.to=function(c,d){if(this.settings.loop){var e=c-this.relative(this.current()),f=this.current(),g=this.current(),h=this.current()+e,i=0>g-h?!0:!1,j=this._clones.length+this._items.length;h<this.settings.items&&i===!1?(f=g+this._items.length,this.reset(f)):h>=j-this.settings.items&&i===!0&&(f=g-this._items.length,this.reset(f)),b.clearTimeout(this.e._goToLoop),this.e._goToLoop=b.setTimeout(a.proxy(function(){this.speed(this.duration(this.current(),f+e,d)),this.current(f+e),this.update()},this),30)}else this.speed(this.duration(this.current(),c,d)),this.current(c),this.update()},e.prototype.next=function(a){a=a||!1,this.to(this.relative(this.current())+1,a)},e.prototype.prev=function(a){a=a||!1,this.to(this.relative(this.current())-1,a)},e.prototype.transitionEnd=function(a){return a!==d&&(a.stopPropagation(),(a.target||a.srcElement||a.originalTarget)!==this.$stage.get(0))?!1:(this.state.inMotion=!1,void this.trigger("translated"))},e.prototype.viewport=function(){var d;if(this.options.responsiveBaseElement!==b)d=a(this.options.responsiveBaseElement).width();else if(b.innerWidth)d=b.innerWidth;else{if(!c.documentElement||!c.documentElement.clientWidth)throw"Can not detect viewport width.";d=c.documentElement.clientWidth}return d},e.prototype.replace=function(b){this.$stage.empty(),this._items=[],b&&(b=b instanceof jQuery?b:a(b)),this.settings.nestedItemSelector&&(b=b.find("."+this.settings.nestedItemSelector)),b.filter(function(){return 1===this.nodeType}).each(a.proxy(function(a,b){b=this.prepare(b),this.$stage.append(b),this._items.push(b),this._mergers.push(1*b.find("[data-merge]").andSelf("[data-merge]").attr("data-merge")||1)},this)),this.reset(a.isNumeric(this.settings.startPosition)?this.settings.startPosition:0),this.invalidate("items")},e.prototype.add=function(a,b){b=b===d?this._items.length:this.normalize(b,!0),this.trigger("add",{content:a,position:b}),0===this._items.length||b===this._items.length?(this.$stage.append(a),this._items.push(a),this._mergers.push(1*a.find("[data-merge]").andSelf("[data-merge]").attr("data-merge")||1)):(this._items[b].before(a),this._items.splice(b,0,a),this._mergers.splice(b,0,1*a.find("[data-merge]").andSelf("[data-merge]").attr("data-merge")||1)),this.invalidate("items"),this.trigger("added",{content:a,position:b})},e.prototype.remove=function(a){a=this.normalize(a,!0),a!==d&&(this.trigger("remove",{content:this._items[a],position:a}),this._items[a].remove(),this._items.splice(a,1),this._mergers.splice(a,1),this.invalidate("items"),this.trigger("removed",{content:null,position:a}))},e.prototype.addTriggerableEvents=function(){var b=a.proxy(function(b,c){return a.proxy(function(a){a.relatedTarget!==this&&(this.suppress([c]),b.apply(this,[].slice.call(arguments,1)),this.release([c]))},this)},this);a.each({next:this.next,prev:this.prev,to:this.to,destroy:this.destroy,refresh:this.refresh,replace:this.replace,add:this.add,remove:this.remove},a.proxy(function(a,c){this.$element.on(a+".owl.carousel",b(c,a+".owl.carousel"))},this))},e.prototype.watchVisibility=function(){function c(a){return a.offsetWidth>0&&a.offsetHeight>0}function d(){c(this.$element.get(0))&&(this.$element.removeClass("owl-hidden"),this.refresh(),b.clearInterval(this.e._checkVisibile))}c(this.$element.get(0))||(this.$element.addClass("owl-hidden"),b.clearInterval(this.e._checkVisibile),this.e._checkVisibile=b.setInterval(a.proxy(d,this),500))},e.prototype.preloadAutoWidthImages=function(b){var c,d,e,f;c=0,d=this,b.each(function(g,h){e=a(h),f=new Image,f.onload=function(){c++,e.attr("src",f.src),e.css("opacity",1),c>=b.length&&(d.state.imagesLoaded=!0,d.initialize())},f.src=e.attr("src")||e.attr("data-src")||e.attr("data-src-retina")})},e.prototype.destroy=function(){this.$element.hasClass(this.settings.themeClass)&&this.$element.removeClass(this.settings.themeClass),this.settings.responsive!==!1&&a(b).off("resize.owl.carousel"),this.transitionEndVendor&&this.off(this.$stage.get(0),this.transitionEndVendor,this.e._transitionEnd);for(var d in this._plugins)this._plugins[d].destroy();(this.settings.mouseDrag||this.settings.touchDrag)&&(this.$stage.off("mousedown touchstart touchcancel"),a(c).off(".owl.dragEvents"),this.$stage.get(0).onselectstart=function(){},this.$stage.off("dragstart",function(){return!1})),this.$element.off(".owl"),this.$stage.children(".cloned").remove(),this.e=null,this.$element.removeData("owlCarousel"),this.$stage.children().contents().unwrap(),this.$stage.children().unwrap(),this.$stage.unwrap()},e.prototype.op=function(a,b,c){var d=this.settings.rtl;switch(b){case"<":return d?a>c:c>a;case">":return d?c>a:a>c;case">=":return d?c>=a:a>=c;case"<=":return d?a>=c:c>=a}},e.prototype.on=function(a,b,c,d){a.addEventListener?a.addEventListener(b,c,d):a.attachEvent&&a.attachEvent("on"+b,c)},e.prototype.off=function(a,b,c,d){a.removeEventListener?a.removeEventListener(b,c,d):a.detachEvent&&a.detachEvent("on"+b,c)},e.prototype.trigger=function(b,c,d){var e={item:{count:this._items.length,index:this.current()}},f=a.camelCase(a.grep(["on",b,d],function(a){return a}).join("-").toLowerCase()),g=a.Event([b,"owl",d||"carousel"].join(".").toLowerCase(),a.extend({relatedTarget:this},e,c));return this._supress[b]||(a.each(this._plugins,function(a,b){b.onTrigger&&b.onTrigger(g)}),this.$element.trigger(g),this.settings&&"function"==typeof this.settings[f]&&this.settings[f].apply(this,g)),g},e.prototype.suppress=function(b){a.each(b,a.proxy(function(a,b){this._supress[b]=!0},this))},e.prototype.release=function(b){a.each(b,a.proxy(function(a,b){delete this._supress[b]},this))},e.prototype.browserSupport=function(){if(this.support3d=j(),this.support3d){this.transformVendor=i();var a=["transitionend","webkitTransitionEnd","transitionend","oTransitionEnd"];this.transitionEndVendor=a[h()],this.vendorName=this.transformVendor.replace(/Transform/i,""),this.vendorName=""!==this.vendorName?"-"+this.vendorName.toLowerCase()+"-":""}this.state.orientation=b.orientation},a.fn.owlCarousel=function(b){return this.each(function(){a(this).data("owlCarousel")||a(this).data("owlCarousel",new e(this,b))})},a.fn.owlCarousel.Constructor=e}(window.Zepto||window.jQuery,window,document),function(a,b){var c=function(b){this._core=b,this._loaded=[],this._handlers={"initialized.owl.carousel change.owl.carousel":a.proxy(function(b){if(b.namespace&&this._core.settings&&this._core.settings.lazyLoad&&(b.property&&"position"==b.property.name||"initialized"==b.type))for(var c=this._core.settings,d=c.center&&Math.ceil(c.items/2)||c.items,e=c.center&&-1*d||0,f=(b.property&&b.property.value||this._core.current())+e,g=this._core.clones().length,h=a.proxy(function(a,b){this.load(b)},this);e++<d;)this.load(g/2+this._core.relative(f)),g&&a.each(this._core.clones(this._core.relative(f++)),h)},this)},this._core.options=a.extend({},c.Defaults,this._core.options),this._core.$element.on(this._handlers)};c.Defaults={lazyLoad:!1},c.prototype.load=function(c){var d=this._core.$stage.children().eq(c),e=d&&d.find(".owl-lazy");!e||a.inArray(d.get(0),this._loaded)>-1||(e.each(a.proxy(function(c,d){var e,f=a(d),g=b.devicePixelRatio>1&&f.attr("data-src-retina")||f.attr("data-src");this._core.trigger("load",{element:f,url:g},"lazy"),f.is("img")?f.one("load.owl.lazy",a.proxy(function(){f.css("opacity",1),this._core.trigger("loaded",{element:f,url:g},"lazy")},this)).attr("src",g):(e=new Image,e.onload=a.proxy(function(){f.css({"background-image":"url("+g+")",opacity:"1"}),this._core.trigger("loaded",{element:f,url:g},"lazy")},this),e.src=g)},this)),this._loaded.push(d.get(0)))},c.prototype.destroy=function(){var a,b;for(a in this.handlers)this._core.$element.off(a,this.handlers[a]);for(b in Object.getOwnPropertyNames(this))"function"!=typeof this[b]&&(this[b]=null)},a.fn.owlCarousel.Constructor.Plugins.Lazy=c}(window.Zepto||window.jQuery,window,document),function(a){var b=function(c){this._core=c,this._handlers={"initialized.owl.carousel":a.proxy(function(){this._core.settings.autoHeight&&this.update()},this),"changed.owl.carousel":a.proxy(function(a){this._core.settings.autoHeight&&"position"==a.property.name&&this.update()},this),"loaded.owl.lazy":a.proxy(function(a){this._core.settings.autoHeight&&a.element.closest("."+this._core.settings.itemClass)===this._core.$stage.children().eq(this._core.current())&&this.update()},this)},this._core.options=a.extend({},b.Defaults,this._core.options),this._core.$element.on(this._handlers)};b.Defaults={autoHeight:!1,autoHeightClass:"owl-height"},b.prototype.update=function(){this._core.$stage.parent().height(this._core.$stage.children().eq(this._core.current()).height()).addClass(this._core.settings.autoHeightClass)},b.prototype.destroy=function(){var a,b;for(a in this._handlers)this._core.$element.off(a,this._handlers[a]);for(b in Object.getOwnPropertyNames(this))"function"!=typeof this[b]&&(this[b]=null)},a.fn.owlCarousel.Constructor.Plugins.AutoHeight=b}(window.Zepto||window.jQuery,window,document),function(a,b,c){var d=function(b){this._core=b,this._videos={},this._playing=null,this._fullscreen=!1,this._handlers={"resize.owl.carousel":a.proxy(function(a){this._core.settings.video&&!this.isInFullScreen()&&a.preventDefault()},this),"refresh.owl.carousel changed.owl.carousel":a.proxy(function(){this._playing&&this.stop()},this),"prepared.owl.carousel":a.proxy(function(b){var c=a(b.content).find(".owl-video");c.length&&(c.css("display","none"),this.fetch(c,a(b.content)))},this)},this._core.options=a.extend({},d.Defaults,this._core.options),this._core.$element.on(this._handlers),this._core.$element.on("click.owl.video",".owl-video-play-icon",a.proxy(function(a){this.play(a)},this))};d.Defaults={video:!1,videoHeight:!1,videoWidth:!1},d.prototype.fetch=function(a,b){var c=a.attr("data-vimeo-id")?"vimeo":"youtube",d=a.attr("data-vimeo-id")||a.attr("data-youtube-id"),e=a.attr("data-width")||this._core.settings.videoWidth,f=a.attr("data-height")||this._core.settings.videoHeight,g=a.attr("href");if(!g)throw new Error("Missing video URL.");if(d=g.match(/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/),d[3].indexOf("youtu")>-1)c="youtube";else{if(!(d[3].indexOf("vimeo")>-1))throw new Error("Video URL not supported.");c="vimeo"}d=d[6],this._videos[g]={type:c,id:d,width:e,height:f},b.attr("data-video",g),this.thumbnail(a,this._videos[g])},d.prototype.thumbnail=function(b,c){var d,e,f,g=c.width&&c.height?'style="width:'+c.width+"px;height:"+c.height+'px;"':"",h=b.find("img"),i="src",j="",k=this._core.settings,l=function(a){e='<div class="owl-video-play-icon"></div>',d=k.lazyLoad?'<div class="owl-video-tn '+j+'" '+i+'="'+a+'"></div>':'<div class="owl-video-tn" style="opacity:1;background-image:url('+a+')"></div>',b.after(d),b.after(e)};return b.wrap('<div class="owl-video-wrapper"'+g+"></div>"),this._core.settings.lazyLoad&&(i="data-src",j="owl-lazy"),h.length?(l(h.attr(i)),h.remove(),!1):void("youtube"===c.type?(f="http://img.youtube.com/vi/"+c.id+"/hqdefault.jpg",l(f)):"vimeo"===c.type&&a.ajax({type:"GET",url:"http://vimeo.com/api/v2/video/"+c.id+".json",jsonp:"callback",dataType:"jsonp",success:function(a){f=a[0].thumbnail_large,l(f)}}))},d.prototype.stop=function(){this._core.trigger("stop",null,"video"),this._playing.find(".owl-video-frame").remove(),this._playing.removeClass("owl-video-playing"),this._playing=null},d.prototype.play=function(b){this._core.trigger("play",null,"video"),this._playing&&this.stop();var c,d,e=a(b.target||b.srcElement),f=e.closest("."+this._core.settings.itemClass),g=this._videos[f.attr("data-video")],h=g.width||"100%",i=g.height||this._core.$stage.height();"youtube"===g.type?c='<iframe width="'+h+'" height="'+i+'" src="http://www.youtube.com/embed/'+g.id+"?autoplay=1&v="+g.id+'" frameborder="0" allowfullscreen></iframe>':"vimeo"===g.type&&(c='<iframe src="http://player.vimeo.com/video/'+g.id+'?autoplay=1" width="'+h+'" height="'+i+'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'),f.addClass("owl-video-playing"),this._playing=f,d=a('<div style="height:'+i+"px; width:"+h+'px" class="owl-video-frame">'+c+"</div>"),e.after(d)},d.prototype.isInFullScreen=function(){var d=c.fullscreenElement||c.mozFullScreenElement||c.webkitFullscreenElement;return d&&a(d).parent().hasClass("owl-video-frame")&&(this._core.speed(0),this._fullscreen=!0),d&&this._fullscreen&&this._playing?!1:this._fullscreen?(this._fullscreen=!1,!1):this._playing&&this._core.state.orientation!==b.orientation?(this._core.state.orientation=b.orientation,!1):!0},d.prototype.destroy=function(){var a,b;this._core.$element.off("click.owl.video");for(a in this._handlers)this._core.$element.off(a,this._handlers[a]);for(b in Object.getOwnPropertyNames(this))"function"!=typeof this[b]&&(this[b]=null)},a.fn.owlCarousel.Constructor.Plugins.Video=d}(window.Zepto||window.jQuery,window,document),function(a,b,c,d){var e=function(b){this.core=b,this.core.options=a.extend({},e.Defaults,this.core.options),this.swapping=!0,this.previous=d,this.next=d,this.handlers={"change.owl.carousel":a.proxy(function(a){"position"==a.property.name&&(this.previous=this.core.current(),this.next=a.property.value)},this),"drag.owl.carousel dragged.owl.carousel translated.owl.carousel":a.proxy(function(a){this.swapping="translated"==a.type},this),"translate.owl.carousel":a.proxy(function(){this.swapping&&(this.core.options.animateOut||this.core.options.animateIn)&&this.swap()},this)},this.core.$element.on(this.handlers)};e.Defaults={animateOut:!1,animateIn:!1},e.prototype.swap=function(){if(1===this.core.settings.items&&this.core.support3d){this.core.speed(0);var b,c=a.proxy(this.clear,this),d=this.core.$stage.children().eq(this.previous),e=this.core.$stage.children().eq(this.next),f=this.core.settings.animateIn,g=this.core.settings.animateOut;this.core.current()!==this.previous&&(g&&(b=this.core.coordinates(this.previous)-this.core.coordinates(this.next),d.css({left:b+"px"}).addClass("animated owl-animated-out").addClass(g).one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend",c)),f&&e.addClass("animated owl-animated-in").addClass(f).one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend",c))}},e.prototype.clear=function(b){a(b.target).css({left:""}).removeClass("animated owl-animated-out owl-animated-in").removeClass(this.core.settings.animateIn).removeClass(this.core.settings.animateOut),this.core.transitionEnd()},e.prototype.destroy=function(){var a,b;for(a in this.handlers)this.core.$element.off(a,this.handlers[a]);for(b in Object.getOwnPropertyNames(this))"function"!=typeof this[b]&&(this[b]=null)},a.fn.owlCarousel.Constructor.Plugins.Animate=e}(window.Zepto||window.jQuery,window,document),function(a,b,c){var d=function(b){this.core=b,this.core.options=a.extend({},d.Defaults,this.core.options),this.handlers={"translated.owl.carousel refreshed.owl.carousel":a.proxy(function(){this.autoplay()
},this),"play.owl.autoplay":a.proxy(function(a,b,c){this.play(b,c)},this),"stop.owl.autoplay":a.proxy(function(){this.stop()},this),"mouseover.owl.autoplay":a.proxy(function(){this.core.settings.autoplayHoverPause&&this.pause()},this),"mouseleave.owl.autoplay":a.proxy(function(){this.core.settings.autoplayHoverPause&&this.autoplay()},this)},this.core.$element.on(this.handlers)};d.Defaults={autoplay:!1,autoplayTimeout:5e3,autoplayHoverPause:!1,autoplaySpeed:!1},d.prototype.autoplay=function(){this.core.settings.autoplay&&!this.core.state.videoPlay?(b.clearInterval(this.interval),this.interval=b.setInterval(a.proxy(function(){this.play()},this),this.core.settings.autoplayTimeout)):b.clearInterval(this.interval)},d.prototype.play=function(){return c.hidden===!0||this.core.state.isTouch||this.core.state.isScrolling||this.core.state.isSwiping||this.core.state.inMotion?void 0:this.core.settings.autoplay===!1?void b.clearInterval(this.interval):void this.core.next(this.core.settings.autoplaySpeed)},d.prototype.stop=function(){b.clearInterval(this.interval)},d.prototype.pause=function(){b.clearInterval(this.interval)},d.prototype.destroy=function(){var a,c;b.clearInterval(this.interval);for(a in this.handlers)this.core.$element.off(a,this.handlers[a]);for(c in Object.getOwnPropertyNames(this))"function"!=typeof this[c]&&(this[c]=null)},a.fn.owlCarousel.Constructor.Plugins.autoplay=d}(window.Zepto||window.jQuery,window,document),function(a){"use strict";var b=function(c){this._core=c,this._initialized=!1,this._pages=[],this._controls={},this._templates=[],this.$element=this._core.$element,this._overrides={next:this._core.next,prev:this._core.prev,to:this._core.to},this._handlers={"prepared.owl.carousel":a.proxy(function(b){this._core.settings.dotsData&&this._templates.push(a(b.content).find("[data-dot]").andSelf("[data-dot]").attr("data-dot"))},this),"add.owl.carousel":a.proxy(function(b){this._core.settings.dotsData&&this._templates.splice(b.position,0,a(b.content).find("[data-dot]").andSelf("[data-dot]").attr("data-dot"))},this),"remove.owl.carousel prepared.owl.carousel":a.proxy(function(a){this._core.settings.dotsData&&this._templates.splice(a.position,1)},this),"change.owl.carousel":a.proxy(function(a){if("position"==a.property.name&&!this._core.state.revert&&!this._core.settings.loop&&this._core.settings.navRewind){var b=this._core.current(),c=this._core.maximum(),d=this._core.minimum();a.data=a.property.value>c?b>=c?d:c:a.property.value<d?c:a.property.value}},this),"changed.owl.carousel":a.proxy(function(a){"position"==a.property.name&&this.draw()},this),"refreshed.owl.carousel":a.proxy(function(){this._initialized||(this.initialize(),this._initialized=!0),this._core.trigger("refresh",null,"navigation"),this.update(),this.draw(),this._core.trigger("refreshed",null,"navigation")},this)},this._core.options=a.extend({},b.Defaults,this._core.options),this.$element.on(this._handlers)};b.Defaults={nav:!1,navRewind:!0,navText:["prev","next"],navSpeed:!1,navElement:"div",navContainer:!1,navContainerClass:"owl-nav",navClass:["owl-prev","owl-next"],slideBy:1,dotClass:"owl-dot",dotsClass:"owl-dots",dots:!0,dotsEach:!1,dotData:!1,dotsSpeed:!1,dotsContainer:!1,controlsClass:"owl-controls"},b.prototype.initialize=function(){var b,c,d=this._core.settings;d.dotsData||(this._templates=[a("<div>").addClass(d.dotClass).append(a("<span>")).prop("outerHTML")]),d.navContainer&&d.dotsContainer||(this._controls.$container=a("<div>").addClass(d.controlsClass).appendTo(this.$element)),this._controls.$indicators=d.dotsContainer?a(d.dotsContainer):a("<div>").hide().addClass(d.dotsClass).appendTo(this._controls.$container),this._controls.$indicators.on("click","div",a.proxy(function(b){var c=a(b.target).parent().is(this._controls.$indicators)?a(b.target).index():a(b.target).parent().index();b.preventDefault(),this.to(c,d.dotsSpeed)},this)),b=d.navContainer?a(d.navContainer):a("<div>").addClass(d.navContainerClass).prependTo(this._controls.$container),this._controls.$next=a("<"+d.navElement+">"),this._controls.$previous=this._controls.$next.clone(),this._controls.$previous.addClass(d.navClass[0]).html(d.navText[0]).hide().prependTo(b).on("click",a.proxy(function(){this.prev(d.navSpeed)},this)),this._controls.$next.addClass(d.navClass[1]).html(d.navText[1]).hide().appendTo(b).on("click",a.proxy(function(){this.next(d.navSpeed)},this));for(c in this._overrides)this._core[c]=a.proxy(this[c],this)},b.prototype.destroy=function(){var a,b,c,d;for(a in this._handlers)this.$element.off(a,this._handlers[a]);for(b in this._controls)this._controls[b].remove();for(d in this.overides)this._core[d]=this._overrides[d];for(c in Object.getOwnPropertyNames(this))"function"!=typeof this[c]&&(this[c]=null)},b.prototype.update=function(){var a,b,c,d=this._core.settings,e=this._core.clones().length/2,f=e+this._core.items().length,g=d.center||d.autoWidth||d.dotData?1:d.dotsEach||d.items;if("page"!==d.slideBy&&(d.slideBy=Math.min(d.slideBy,d.items)),d.dots||"page"==d.slideBy)for(this._pages=[],a=e,b=0,c=0;f>a;a++)(b>=g||0===b)&&(this._pages.push({start:a-e,end:a-e+g-1}),b=0,++c),b+=this._core.mergers(this._core.relative(a))},b.prototype.draw=function(){var b,c,d="",e=this._core.settings,f=(this._core.$stage.children(),this._core.relative(this._core.current()));if(!e.nav||e.loop||e.navRewind||(this._controls.$previous.toggleClass("disabled",0>=f),this._controls.$next.toggleClass("disabled",f>=this._core.maximum())),this._controls.$previous.toggle(e.nav),this._controls.$next.toggle(e.nav),e.dots){if(b=this._pages.length-this._controls.$indicators.children().length,e.dotData&&0!==b){for(c=0;c<this._controls.$indicators.children().length;c++)d+=this._templates[this._core.relative(c)];this._controls.$indicators.html(d)}else b>0?(d=new Array(b+1).join(this._templates[0]),this._controls.$indicators.append(d)):0>b&&this._controls.$indicators.children().slice(b).remove();this._controls.$indicators.find(".active").removeClass("active"),this._controls.$indicators.children().eq(a.inArray(this.current(),this._pages)).addClass("active")}this._controls.$indicators.toggle(e.dots)},b.prototype.onTrigger=function(b){var c=this._core.settings;b.page={index:a.inArray(this.current(),this._pages),count:this._pages.length,size:c&&(c.center||c.autoWidth||c.dotData?1:c.dotsEach||c.items)}},b.prototype.current=function(){var b=this._core.relative(this._core.current());return a.grep(this._pages,function(a){return a.start<=b&&a.end>=b}).pop()},b.prototype.getPosition=function(b){var c,d,e=this._core.settings;return"page"==e.slideBy?(c=a.inArray(this.current(),this._pages),d=this._pages.length,b?++c:--c,c=this._pages[(c%d+d)%d].start):(c=this._core.relative(this._core.current()),d=this._core.items().length,b?c+=e.slideBy:c-=e.slideBy),c},b.prototype.next=function(b){a.proxy(this._overrides.to,this._core)(this.getPosition(!0),b)},b.prototype.prev=function(b){a.proxy(this._overrides.to,this._core)(this.getPosition(!1),b)},b.prototype.to=function(b,c,d){var e;d?a.proxy(this._overrides.to,this._core)(b,c):(e=this._pages.length,a.proxy(this._overrides.to,this._core)(this._pages[(b%e+e)%e].start,c))},a.fn.owlCarousel.Constructor.Plugins.Navigation=b}(window.Zepto||window.jQuery,window,document),function(a,b){"use strict";var c=function(d){this._core=d,this._hashes={},this.$element=this._core.$element,this._handlers={"initialized.owl.carousel":a.proxy(function(){"URLHash"==this._core.settings.startPosition&&a(b).trigger("hashchange.owl.navigation")},this),"prepared.owl.carousel":a.proxy(function(b){var c=a(b.content).find("[data-hash]").andSelf("[data-hash]").attr("data-hash");this._hashes[c]=b.content},this)},this._core.options=a.extend({},c.Defaults,this._core.options),this.$element.on(this._handlers),a(b).on("hashchange.owl.navigation",a.proxy(function(){var a=b.location.hash.substring(1),c=this._core.$stage.children(),d=this._hashes[a]&&c.index(this._hashes[a])||0;return a?void this._core.to(d,!1,!0):!1},this))};c.Defaults={URLhashListener:!1},c.prototype.destroy=function(){var c,d;a(b).off("hashchange.owl.navigation");for(c in this._handlers)this._core.$element.off(c,this._handlers[c]);for(d in Object.getOwnPropertyNames(this))"function"!=typeof this[d]&&(this[d]=null)},a.fn.owlCarousel.Constructor.Plugins.Hash=c}(window.Zepto||window.jQuery,window,document);
var ballon = Class({
    constructor: function () {
        this.$el1 = $('[data-role=boy-ballon1]');
        this.$el2 = $('[data-role=boy-ballon2]');

        this.$el1Text = $('[data-role=ballon-text1]');
        this.$el2Text = $('[data-role=ballon-text2]');
        this.loadingPhrase =
        {
            1: 'По вашим параметрам мы нашли один самый лучший отель.',
            2: 'По вашим параметрам мы нашли два самых лучших отеля. Выбирайте!',
            3: 'По вашим параметрам мы нашли три самых лучших отеля. Выбирайте!'
        };
        this.init();
    },

    events: function () {
        var _this = this;
        wizard.e.on ('slideChange', _.bind(function (data) {
            _.defer(function () {
                _this.insertText(_this.getPhrases(data.nextSlide));
            });
        }, this));

        wizard.e.on ('hotelsAppended', _.bind(function (data) {
            _.defer(_.bind(function () {
                _this.insertText([_this.loadingPhrase[data.cnt]]);
            }), this);
        }, this));

        wizard.e.on ('needProposal', _.bind(function () {
            this.$el1.addClass ('hidden');
            this.$el2.addClass ('hidden');
        }, this));
    },

    init: function () {
        this.events();
    },

    getPhrases: function (num) {
        var phrases1 = [
            'Я подберу отели специально для вашего путешествия',
            'Выберите дату заезда в отель',
            'А теперь — дату выезда',
            'Продолжим',
            '<span>' + window.searchForm.controls.guests.config.adultsTitle(window.searchForm.controls.guests.config.adults) + ', ' +
            window.searchForm.controls.guests.config.childrenTitle(window.searchForm.controls.guests.config.children) + '</span>.<br/> Круто, погнали дальше.',
            'Отлично! ' + (wizard.filter.typeTrip == 1 ? 'Бизнес-трип' : 'Отдых и туризм') + ', записал',
            '',
            'Секундочку. Подбираем отели <br/>по вашим параметрам...'
        ];
        var phrases2 = [
            '',
            '',
            'Мы посчитаем количество ночей',
            'Теперь нужно узнать, сколько человек едет в путешествие',
            'Теперь — вопросы про отель',
            'Нужно уточнить ещё пару деталей',
            'И последний шаг',
            ''
        ];

        return [phrases1[num], phrases2[num]];
    },

    insertText: function (phrases) {
        if (phrases[0]) {
            this.$el1Text.html(phrases[0]);
        }
        if (phrases[1]) {
            this.$el2Text.html(phrases[1]);
        }
    }
});
var ballonMob = Class({
    constructor: function () {
        this.$el1 = $('[data-role=boy-ballon1]');
        this.$el1Text = $('[data-role=ballon-text1]');

        this.phrases = [
            'Куда хотите поехать?',
            'Выберите дату заезда',
            'А теперь — дату выезда',
            'Супер. Сколько ждать гостей?',
            'Выберите тип своего путешествия',
            'Что обязательно должно быть в отеле?',
            'И последний шаг — какой у вас бюджет?',
            'Секундочку. Подбираем отели <br/>по вашим параметрам...'
        ];
        this.loadingPhrase =
        {
            1: 'По вашим параметрам мы нашли один самый лучший отель.',
            2: 'По вашим параметрам мы нашли два самых лучших отеля. Выбирайте!',
            3: 'По вашим параметрам мы нашли три самых лучших отеля. Выбирайте!'
        };
        this.init();
    },

    events: function () {
        var _this = this;
        wizard.e.on ('slideChange', _.bind(function (data) {
            _.defer(function () {
                _this.insertText(_this.phrases[data.nextSlide]);
            });
        }, this));

        wizard.e.on ('hotelsAppended', _.bind(function (data) {
            _.defer(_.bind(function () {
                _this.insertText(_this.loadingPhrase[data.cnt]);
            }), this);
        }, this));

        wizard.e.on ('needProposal', _.bind(function () {
            this.$el1.addClass ('hidden');
        }, this));
    },

    init: function () {
        this.events();
    },

    insertText: function (text) {
        this.$el1Text.html(text);
    }
});
var chbxLinks = Class({
    constructor: function () {
        this.init();
    },

    events: function () {
        $('[data-role=chbx-link]').on ('click', function () {

            var $this = $(this),
                $name = $this.data('name');

            if (_.includes(wizard.filter.amenities, $name)) {
                $this
                    .removeClass('active')
                    .data('active', 0);

                var index = wizard.filter.amenities.indexOf($name);
                if (index > -1) {
                    wizard.filter.amenities.splice(index, 1);
                }
            } else {
                $this
                    .addClass('active')
                    .data('active', 1);

                wizard.filter.amenities.push($name);
            }
            return false;
        });
    },

    init: function () {
        this.events();
    }
});
var decorations = Class({

    constructor: function () {
        this.init();
        this.elPageWrap = $('[data-role=page-wrap]');
    },

    events: function () {
        wizard.e.on ('slideChange', _.bind(function (data) {
            this.bodyClassChange(data.nextSlide);
            if (data.nextSlide != 7) {
                this.wrapClassRemove('page-wrap_hotels-compact page-wrap_boy-hidden page-wrap_hotels-expand');
            }
        }, this));

        wizard.e.on ('hotelsAppended', _.bind(function () {

            this.wrapClassAdd('page-wrap_hotels-compact');

            setTimeout(_.bind(function () {
                this.wrapClassAdd('page-wrap_boy-hidden');
            }, this), 150);

            var _this = this;

            $('[data-role=hotel]').bind ('click', function () {
                var $this = $(this);
                if ($this.data('index') != 2) {
                    $('[data-role=hotels]').prepend ($this);
                }
                _this.wrapClassRemove('page-wrap_hotels-compact page-wrap_boy-hidden');
                _this.wrapClassAdd('page-wrap_hotels-expand');

                _this.photoSlideInit();

                $('[data-role=hotel]').unbind ('click');

                hlf.goal({
                    ga: 'hl_wizard_hotel_expand.click',
                    yam: 'hl_wizard_hotel_expand-click',
                    as: 'hl_wizard_hotel_expand-click'
                }, {type: $this.data('type')});

                woopra.track('wizard_hotel_expand', {
                    type: $this.data('type')
                });


                $('[data-role=book-btn]').on ('click', _.bind(function() {
                    hlf.goal({
                        ga: 'hl_wizard_book_btn.click',
                        yam: 'hl_wizard_book_btn-click',
                        as: 'hl_wizard_book_btn-click'
                    }, {type: $this.data('type')});

                    woopra.track('wizard_book_btn', {
                        type: $this.data('type')
                    });

                    return true;
                }, this));

                $('[data-role=map-link]').on ('click', _.bind(function() {
                    hlf.goal({
                        ga: 'hl_wizard_map.click',
                        yam: 'hl_wizard_map-click',
                        as: 'hl_wizard_map-click'
                    });

                    woopra.track('hl_wizard_map_click');

                    return true;
                }, this));

                return false;
            });
        }, this));
    },

    photoSlideInit: function () {
        $('[data-role=photo-list]').each (function () {
            var $this = $(this);

            $this.owlCarousel ({
                items: 1,
                loop: true,
                nav: true,
                navContainer: $this,
                navText: [
                    '<svg class="icon icon_chevron"><use xlink:href="#icon_chevronL"></use></svg>',
                    '<svg class="icon icon_chevron"><use xlink:href="#icon_chevronR"></use></svg>'
                ],
                navClass: [
                    'hotel-photo-list-nav hotel-photo-list-nav_prev',
                    'hotel-photo-list-nav hotel-photo-list-nav_next'
                ],
                dots: false
            });
            $this.on('changed.owl.carousel', function() {
                hlf.goal({
                    ga: 'hl_wizard_hotel_photos.change',
                    yam: 'hl_wizard_hotel_photos-change',
                    as: 'hl_wizard_hotel_photos-change'
                });

                woopra.track('wizard_hotel_photos_change');
            });
        });
    },

    init: function () {
        this.events();
    },

    bodyClassChange: function (nextSlide) {
        $('body')
            .attr('class','')
            .addClass('body_' + nextSlide);
    },

    wrapClassRemove: function (classes) {
        this.elPageWrap.removeClass(classes);
    },

    wrapClassAdd: function (classes) {
        this.elPageWrap.addClass(classes);
    }
});
FormSearch = function (context) {

    window.searchForm = hlf.form ({
        id: context,
        controls: {
            destination: hlf.ac ({
                type: 'location',
                locale: 'ru-ru',
                autoFocus: false,
                limit: 4,
                needLocationPhotos: window.isMobile ? false : true,
                locationPhotoSize: wizard.retina ? '240x100' : '120x50',
                onlyLocations: true,
                className: 'page-location ui-front' + (window.isMobile ? ' page-location_mobile' : ''),
                placeholder: 'Название города',
                goalUseInput: {
                    ga: 'hl_wizard_search_form.goalUseInput',
                    yam: 'hl_wizard_search_form-goalUseInput',
                    as: 'hl_wizard_search_form-goalUseInput'
                },
                goalAcSelect: {
                    ga: 'hl_wizard_search_form.goalAcSelect',
                    yam: 'hl_wizard_search_form-goalAcSelect',
                    as: 'hl_wizard_search_form-goalAcSelect'
                },
                goalAcSelectType: {
                    ga: 'hl_wizard_search_form.goalAcSelectCategory',
                    yam: 'hl_wizard_search_form-goalAcSelectCategory',
                    as: 'hl_wizard_search_form-goalAcSelectCategory'
                },
                onSelect: function (data, locationId) {
                    wizard.e.trigger ('acSelect', [locationId]);
                    woopra.track('wizard_ac_select', {
                        locationId: locationId
                    });
                }
            }),

            checkIn: hlf.calendar ({
                required: true,
                name: 'checkIn',
                relationCalendar: 'checkOut',
                relationSuperior: true,
                relationAutoSet: true,
                relationAutoShow: true,
                inline: true,
                locale: 'ru-RU',
                mobileMode: false,
                months: 6,
                min: -1,
                goalSelectDate: {
                    ga: 'hl_wizard_search_form.checkInSelectDate',
                    yam: 'hl_wizard_search_form-checkInSelectDate',
                    as: 'hl_wizard_search_form-checkInSelectDate'
                },
                onSelect: function (date, formatDate) {
                    wizard.e.trigger ('calendarCheckInSelect', [window.searchForm.controls.checkIn.getParams().checkIn]);
                    woopra.track('wizard_checkin_select', {
                        date: formatDate
                    });
                }
            }),

            checkOut: hlf.calendar({
                required: true,
                name: 'checkOut',
                relationCalendar: 'checkIn',
                relationSuperior: false,
                relationAutoSet: true,
                relationAutoShow: true,
                locale: 'ru-RU',
                mobileMode: false,
                months: 6,
                inline: true,
                min: 0,
                goalSelectDate: {
                    ga: 'hl_wizard_search_form.checkOutSelectDate',
                    yam: 'hl_wizard_search_form-checkOutSelectDate',
                    as: 'hl_wizard_search_form-checkOutSelectDate'
                },
                onSelect: function (date, formatDate) {
                    wizard.e.trigger ('calendarCheckOutSelect', [window.searchForm.controls.checkOut.getParams().checkOut]);
                    wizard.request.nights = days(new Date(window.searchForm.controls.checkIn.getParams().checkIn), new Date(window.searchForm.controls.checkOut.getParams().checkOut));
                    woopra.track('wizard_checkout_select', {
                        date: formatDate
                    });
                }
            }),

            guests: hlf.guests({
                adults:  2,
                children: [],
                titlesPosInside: true,
                adultsTitle: function(adults) {
                    var text = adults + ' Взрослый';
                    if (adults > 1) {
                        text = adults + ' Взрослых'
                    }
                    return text;
                },
                childrenTitle: function(children) {
                    var text = 'Без детей';
                    var length = children.length;

                    if (length == 1) {
                        text = length + ' Ребёнок'
                    }
                    if (length > 1) {
                        text = length + ' Детей'
                    }
                    return text;
                },
                summary: function () {
                    return ' ';
                },
                childAge: '',
                childrenListTitle: 'Укажите возраст детей (от 0 до 17 лет)',
                childValSep: window.isMobile ? false : true,
                decControlContent: '<svg class="icon icon_minusSmall"><use xlink:href="#icon_minusSmall"></use></svg>',
                incControlContent: '<svg class="icon icon_plusSmall"><use xlink:href="#icon_plusSmall"></use></svg>',
                decControlContentChld: '<svg class="icon icon_minusSmall"><use xlink:href="#icon_minusSmall"></use></svg>',
                incControlContentChld: '<svg class="icon icon_plusSmall"><use xlink:href="#icon_plusSmall"></use></svg>',
                goalOpen: {
                    ga: 'hl_wizard_search_form.guests',
                    yam: 'hl_wizard_search_form-guests',
                    as: 'hl_wizard_search_form-guests'
                }
            })
        }
    });

};

function days (checkIn, checkOut) {
    return Math.round(Math.abs((checkIn - checkOut)/24*60*60*1000));
};
var priceRange = Class({
    constructor: function () {
        this.el = document.getElementById('range');
        this.skipValues = [
            document.getElementById('range-min'),
            document.getElementById('range-max')
        ];

        this.init();
    },

    events: function () {
        this.el.noUiSlider.on('update', _.bind(function( values, handle ) {
            this.skipValues[handle].innerHTML = values[handle];
        },this));

        $('[data-role=price-next-btn]').on ('click', _.bind(function() {
            wizard.filter.priceRange[0] = parseInt(this.el.noUiSlider.get()[0].replace(/ /g,''));
            wizard.filter.priceRange[1] = parseInt(this.el.noUiSlider.get()[1].replace(/ /g,''));
            wizard.e.trigger ('priceNextBtn-Click');

            hlf.goal({
                ga: 'hl_wizard_price_range.choice',
                yam: 'hl_wizard_price_range-choice',
                as: 'hl_wizard_price_range-choice'
            },{priceRange: wizard.filter.priceRange});

            woopra.track('wizard_price_range', {
                priceRangeMin: wizard.filter.priceRange[0],
                priceRangeMax: wizard.filter.priceRange[1]
            });

            return false;
        }, this));
    },

    init: function () {
        noUiSlider.create(this.el, {
            connect: true,
            start: [wizard.filter.priceRange[0], wizard.filter.priceRange[1]],
            range: {
                'min': [ wizard.filter.priceRange[0] ],
                '50%': [ Math.ceil((wizard.filter.priceRange[1] - wizard.filter.priceRange[0]) / 25 + wizard.filter.priceRange[0]) ],
                '75%': [ Math.ceil((wizard.filter.priceRange[1] - wizard.filter.priceRange[0]) / 10 + wizard.filter.priceRange[0]) ],
                'max': [ wizard.filter.priceRange[1] ]
            },
            step: 1,
            format: wNumb ({
                decimals: false,
                mark: false,
                thousand: ' ',
                postfix: ' руб.'
            })
        });
        this.events();
    }
});
var progressBar = Class({
    constructor: function () {
        this.$el = $('[data-role=progressBar]');
        this.init();
    },

    events: function () {
        var _this = this;
        wizard.e.on ('slideChange', _.bind(function (data) {
            _.defer(_.bind(function () {
                _this.$el
                    .attr('class','')
                    .addClass('progressBar progressBar_' + data.nextSlide);
            }), this);
        }, this));
    },

    init: function () {
        this.events();
    }
});
var radioLinks = Class({
    constructor: function () {
        this.init();
    },

    events: function () {
        $('[data-role=radio-link]').on ('click', function () {

            var $this = $(this),
                $parent = $this.parents('[data-role=radio-links-group]'),
                $state = $this.data('active'),
                $value = $this.data('value');

            if (!$state) {
                wizard.filter.typeTrip = $value;

                $parent.find('[data-role=radio-link]')
                    .removeClass('active')
                    .data('active', 0);

                $this
                    .addClass('active')
                    .data('active', 1);
            } else {
                $this
                    .removeClass('active')
                    .data('active', 0);

                wizard.filter.typeTrip = 0;
            }
            wizard.e.trigger ('radioLink-Click');

            hlf.goal({
                ga: 'hl_wizard_trip_type.choice',
                yam: 'hl_wizard_trip_type-choice',
                as: 'hl_wizard_trip_type-choice'
            },{type: $value == 1 ? 'BusinessTrip' : 'Travel'});

            woopra.track('wizard_trip_type', {
                type: $value == 1 ? 'BusinessTrip' : 'Travel'
            });

            return false;
        });
    },

    init: function () {
        this.events();
    }
});
var searchInit = Class({
    constructor: function () {
        this.tpls = {
            'hotel': 'wizard-hotel'
        };
        this.overlayEl = $('[data-role=overlay]');
        this.wrapperEl = $('[data-role=wrapper]');
        this.popupEl = $('[data-role=popup]');
        this.init();
    },

    events: function () {
        wizard.e.on ('nextIsFinalSlide', _.bind(function () {
            $('[data-place=hotels]').html('');
            this.searchRequest();
            woopra.track('wizard_search_init', {
                request: wizard.request
            });
        }, this));

        $('[data-role=popup-close]').on ('click', _.bind(function() {
            this.popupHide('proposal-btn');
        }, this));
    },

    init: function () {
        this.events();
    },

    searchRequest: function () {
        window.time = Math.floor(Date.now() / 1000);
        $.ajax({
            url: 'https://yasen.hotellook.com/adaptors/s_search.json?singleHotel=1',
            data: {
                locationId: wizard.request.locationId,
                checkIn: wizard.request.checkIn,
                checkOut: wizard.request.checkOut,
                adults: wizard.request.adults,
                children: wizard.request.children,
                currency: 'rub',
                async: ''
            },
            dataType: 'jsonp',
            success: _.bind(function(data) {
                if (data) {
                    var time = Math.floor(Date.now() / 1000) - window.time;
                    var sortedByPrice = _.clone(this.sortByPrice(data), true);
                    this.loadHotels(_.keys(sortedByPrice), sortedByPrice);

                    hlf.goal({
                        ga: 'hl_wizard_rooms.loadTime',
                        yam: 'hl_wizard_rooms-loadTime',
                        as: 'hl_wizard_rooms-loadTime'
                    }, {timeSec: time});

                    woopra.track('wizard_rooms_loadTime', {
                        timeSec: time
                    });
                } else {
                    this.popup();
                }
            }, this),
            error: _.bind(function () {
                this.popup();
            }, this)
        });
    },

    loadHotels: function (ids, rooms) {
        window.time2 = Math.floor(Date.now() / 1000);
        $.ajax({
            url: 'https://yasen.hotellook.com/wizard/hotels.json',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify({
                ids: ids,
                currency: 'rub',
                lang: 'ru'
            }),
            success: _.bind(function(data) {
                if (_.size(data)) {
                    var time2 = Math.floor(Date.now() / 1000) - window.time2;
                    var hotels = _.clone(data, true);

                    //do some things with hotels
                    this.hotelModify (hotels, rooms);
                } else {
                    this.popup();
                }
            }, this),
            error: _.bind(function () {
                this.popup();
            }, this)
        });
    },

    hotelModify: function (hotels, rooms) {
        _.each(hotels, function(hotelObj) {
            if (rooms[hotelObj.id]) {
                var room = rooms[hotelObj.id][0];

                //add price of best room to hotel object
                hotelObj.price = Math.round(room.price);

                //add room id to hotel object
                hotelObj.roomId = room.roomId;

                //add gate id to hotel object
                hotelObj.gateId = room.gateId;

                if (_.isUndefined(hotelObj.amenitiesV2.Hotel)) {
                    hotelObj.amenitiesV2.Hotel = {};
                }

                //add breakfast amenity if it's in relative best room
                if (_.size(room.options) && room.options.breakfast) {
                    hotelObj.amenitiesV2.Hotel[_.size(hotelObj.amenitiesV2.Hotel)] = {
                        "name": "Breakfast"
                    }
                }
            }
        });
        this.runFilter(hotels);
    },

    runFilter: function (hotels) {
        this.filter(hotels);
    },

    filter: function (hotels) {
        var hotelsFilteredPrice = [],
            hotelsFilteredAmenities = [],
            hotelsFilteredStars = [];

        //filter by price
        _.each(hotels, function(hotelObj, key) {
            if (hotelObj.price >= wizard.filter.priceRange[0] && hotelObj.price <= wizard.filter.priceRange[1]) {
                hotelsFilteredPrice.push(key);
            }
        });

        //filter by amenities if they are
        if (wizard.filter.amenities.length) {
            var passFlag = {};
            _.each(hotels, function(hotelObj, key) {
                passFlag[key] = 0;

                if (!_.isUndefined(hotelObj.amenitiesV2) && !_.isUndefined(hotelObj.amenitiesV2.Hotel) && _.size(hotelObj.amenitiesV2.Hotel)) {
                    _.each(wizard.filter.amenities, function(amenity) {
                        _.each(hotelObj.amenitiesV2.Hotel, function(hotel_amenity) {
                            if (amenity == _.capitalize(_.words(hotel_amenity.name)[0])) {
                                passFlag[key]++;
                            }
                        });
                    });
                    if (passFlag[key] && passFlag[key] == wizard.filter.amenities.length) {
                        hotelsFilteredAmenities.push(key);
                    }
                }
            });
        } else {
            hotelsFilteredAmenities = _.keys(hotels);
        }

        //filter by stars >= 2
        _.each(hotels, function(hotelObj, key) {
            if (hotelObj.stars && hotelObj.stars >= 2) {
                hotelsFilteredStars.push(key);
            }
        });

        this.intersection(hotels, hotelsFilteredPrice,hotelsFilteredAmenities,hotelsFilteredStars);
    },

    intersection: function (hotels, price, amenities, stars) {
        var intersection = _.intersection(price, amenities, stars),
            mostHotels = {},
            mostHotelsIds = [];

        if (!intersection.length) {
            var max = 200000,
                min = 700;

            if (wizard.filter.amenities.length) {

                wizard.filter.droped.amenitiesDroped.push(_.last(wizard.filter.amenities));

                wizard.filter.amenities.pop();
                this.runFilter(hotels);
                return false;
            }

            if (wizard.filter.priceRange[1] < max) {

                var STEP = 400;

                wizard.filter.droped.priceDropedMax = wizard.filter.droped.priceDropedMax + STEP;

                wizard.filter.priceRange[1] = wizard.filter.priceRange[1] + STEP;
                this.runFilter(hotels);
                return false;
            }

            if (wizard.filter.priceRange[0] > min) {
                var STEP = (wizard.filter.priceRange[0] - 200) < 0 ? 0 : 200;

                wizard.filter.droped.priceDropedMin = wizard.filter.droped.priceDropedMin - STEP;

                wizard.filter.priceRange[0] = wizard.filter.priceRange[0] - STEP;
                this.runFilter(hotels);
                return false;
            }

            this.popup();
        } else {

            mostHotels[0] = hotels[this.getCheapestHotel(hotels, intersection)] || {}; //cheapest
            if (intersection.length > 1) {
                mostHotels[1] = hotels[this.getHighRatingHotel(hotels, intersection, mostHotels[0])] || {}; //highRatingHotel
            }
            if (intersection.length > 2) {
                mostHotels[2] = hotels[this.getBestHotel(hotels, intersection, mostHotels[0], mostHotels[1])] || {}; //bestHotel
            }

            _.each(mostHotels, function (hotel) {
                mostHotelsIds.push(hotel.id);
            });

            $.ajax({
                url: 'https://yasen.hotellook.com/content/hotels.json?ids=' + mostHotelsIds + '&currency=rub&lang=ru',
                dataType: 'jsonp',
                jsonpCallback: 'loadHotels_success_callback',
                cache: true,
                async: true,
                success: _.bind(function (data) {
                    if (_.size(data)) {
                        var mostHotelsDescr = _.clone(data);
                        if (wizard.filter.droped.amenitiesDroped.length || wizard.filter.droped.priceDropedMax || wizard.filter.droped.priceDropedMin) {
                            this.popup(true);
                            wizard.e.trigger ('needProposal');
                        } else {
                            hlf.goal({
                                ga: 'hl_wizard_hotels.found',
                                yam: 'hl_wizard_hotels-found',
                                as: 'hl_wizard_hotels-found'
                            });

                            woopra.track('wizard_hotels_found');
                        }
                        this.drawHotels(mostHotels, mostHotelsDescr);
                    } else {
                        this.popup();
                    }
                }, this),
                error: _.bind(function () {
                    console.log('error');
                    this.popup();
                }, this)
            });
        }
    },

    popup: function (filterDroped) {
        var html = '<p>К сожалению, мы не нашли свободных номеров, <br/>по вашим параметрам.</p>';

        if (filterDroped) {

            $('[data-role=popup]').addClass('popup_proposal');

            html += '<div>Но мы сможем вам кое-что предложить, если ';

            if (wizard.filter.droped.priceDropedMax || wizard.filter.droped.priceDropedMin) {
                html += 'увеличим ценовой диапазон <br/>на <b>' + (wizard.filter.droped.priceDropedMax + wizard.filter.droped.priceDropedMin) + ' рублей </b>';
            }

            if ((wizard.filter.droped.priceDropedMax || wizard.filter.droped.priceDropedMin) && wizard.filter.droped.amenitiesDroped.length) {
                html += ' и ';
            }

            if (wizard.filter.droped.amenitiesDroped.length) {
                html += 'уберём следующие услуги: <br/><div class="popup-amenities"><ul class="amenities-list amenities-list_compact">';

                _.each(wizard.filter.droped.amenitiesDroped, function (amenity) {
                    html += '<li class="amenities-list-item"><svg class="icon icon_am icon_am' + amenity + '"><use xlink:href="#icon_am' + amenity + '"></use></svg></li>';
                });

                html += '</ul></div></div>';
            }

            hlf.goal({
                ga: 'hl_wizard_hotels.proposal',
                yam: 'hl_wizard_hotels-proposal',
                as: 'hl_wizard_hotels-proposal'
            });

            woopra.track('wizard_hotels_proposal');
        } else {
            hlf.goal({
                ga: 'hl_wizard_hotels.notFound',
                yam: 'hl_wizard_hotels-notFound',
                as: 'hl_wizard_hotels-notFound'
            });

            woopra.track('wizard_hotels_notFound');
        }

        $('[data-role=popup]').prepend (html);

        this.popupShow ();
    },

    popupShow: function () {
        this.wrapperEl.addClass ('blured');
        this.popupEl.show();
        this.overlayEl.show();

        this.overlayEl.on ('click', _.bind (function () {
            this.popupHide('overlay');
        }, this));

    },

    popupHide: function (place) {
        this.wrapperEl.removeClass ('blured');
        this.popupEl.hide();
        this.overlayEl.hide();

        hlf.goal({
            ga: 'hl_wizard_popup.hide',
            yam: 'hl_wizard_popup-hide',
            as: 'hl_wizard_popup-hide'
        }, {place: place});

        woopra.track('wizard_popup_hide', {
            place: place
        });
    },

    getCheapestHotel: function (hotels, intersection) {
        var ids = _.clone(intersection, true),
            mostHotelId = ids[0];

        _.each (ids, function(id) {
            if (hotels[id].price < hotels[mostHotelId].price) {
                mostHotelId = id;
            } else if (hotels[id].price == hotels[mostHotelId].price) {
                if (hotels[id].popularity > hotels[mostHotelId].popularity) {
                    mostHotelId = id;
                }
            }
        });

        return mostHotelId;
    },

    getHighRatingHotel: function (hotels, intersection, cheapestHotel) {
        //todo: replace stars with raiting
        var ids = _.clone(intersection, true),
            mostHotelId = ids[0];

        if (mostHotelId == cheapestHotel.id) {
            mostHotelId = ids[1];
        }

        _.each (ids, function(id) {
            if (hotels[id].stars && id != cheapestHotel.id) {
                if (hotels[id].stars > hotels[mostHotelId].stars) {
                    mostHotelId = id;
                } else if (hotels[id].stars == hotels[mostHotelId].stars) {
                    if (hotels[id].price > hotels[mostHotelId].price) {
                        mostHotelId = id;
                    }
                }
            }
        });

        return mostHotelId;
    },

    getBestHotel: function (hotels, intersection, cheapestHotel, highRatingHotel) {
        var ids = _.clone(intersection, true),
            mostHotelId = ids[0];

        if (mostHotelId == cheapestHotel.id) {
            mostHotelId = ids[1];
        }

        if (mostHotelId == highRatingHotel.id) {
            mostHotelId = ids[2];
        }

        _.each (ids, function(id) {
            if (hotels[id].popularity && id != cheapestHotel.id && id != highRatingHotel.id) {
                if (hotels[id].popularity > hotels[mostHotelId].popularity) {
                    mostHotelId = id;
                } else if (hotels[id].popularity == hotels[mostHotelId].popularity) {
                    if (hotels[id].price > hotels[mostHotelId].price) {
                        mostHotelId = id;
                    }
                }
            }
        });

        return mostHotelId;
    },

    drawHotels: function (mostHotels, mostHotelsDescr) {
        var hotels = {},
            data = {},
            name = {},
            description = {},
            amenities_hotel = {},
            amenities_room = {},
            hotels_html = '';

        for (var i = _.size(mostHotels) - 1; i >= 0 ; i--) {
            if (_.size(mostHotels[i].amenitiesV2.Hotel)) {
                amenities_hotel[i] = _.clone(mostHotels[i].amenitiesV2.Hotel, true);

                _.each(amenities_hotel[i], function(amenity) {
                    amenity.short = _.capitalize(_.words(amenity.name)[0]);
                });
            }

            if (_.size(mostHotels[i].amenitiesV2.Room)) {
                amenities_room[i] = _.clone(mostHotels[i].amenitiesV2.Room, true);

                _.each(amenities_room[i], function(amenity) {
                    amenity.short = _.capitalize(_.words(amenity.name)[0]);
                });
            }

            var hotelTypeText = '',
                hotelType = '';

            switch (i) {
                case 0:
                    hotelTypeText = 'Самый дешёвый за';
                    hotelType = 'cheapest';
                    break;
                case 1:
                    hotelTypeText = 'Самый лучший за';
                    hotelType = 'best';
                    break;
                case 2:
                    hotelTypeText = 'С высоким рейтингом за';
                    hotelType = 'highRating';
                    break;
                default:
                    hotelTypeText = 'Самый дешёвый за';
                    hotelType = 'cheapest';
            }

            if (mostHotels[i].localized) {
                if (mostHotels[i].localized['ru'] && mostHotels[i].localized['ru'].name) {
                    name[i] = mostHotels[i].localized['ru'].name;
                } else if (mostHotels[i].localized['en'] && mostHotels[i].localized['en'].name) {
                    name[i] = mostHotels[i].localized['en'].name;
                } else {
                    name[i] = '';
                }
            } else {
                name[i] = '';
            }

            if (mostHotelsDescr[mostHotels[i].id].hotel.localized) {
                if (mostHotelsDescr[mostHotels[i].id].hotel.localized['ru'] && mostHotelsDescr[mostHotels[i].id].hotel.localized['ru'].description) {
                    description[i] = mostHotelsDescr[mostHotels[i].id].hotel.localized['ru'].description;
                } else if (mostHotelsDescr[mostHotels[i].id].hotel.localized['en'] && mostHotelsDescr[mostHotels[i].id].hotel.localized['en'].description) {
                    description[i] = mostHotelsDescr[mostHotels[i].id].hotel.localized['en'].description;
                } else {
                    description[i] = '';
                }
            } else {
                description[i] = '';
            }

            var imgW = window.isMobile ? 600 : 1200,
                imgH = window.isMobile ? 360 : 520,
                lat = mostHotels[i].location.lat ? mostHotels[i].location.lat : 0,
                lon = mostHotels[i].location.lon ? mostHotels[i].location.lon : 0,
                mapLink = 'https://maps.google.com/?q=' + lat + ',' + lon;

            if (device.ios() || device.android()) {
                mapLink = 'geo:' + lat + ',' + lon;
            }

            data[i] = {
                'index': i,
                'hotelTypeText': hotelTypeText,
                'hotelType': hotelType,
                'description': description[i],
                'minPrice': (mostHotels[i].price + '').replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1 "),
                'stars': mostHotels[i].stars ? mostHotels[i].stars : 0,
                'name': name[i],
                'lat': lat,
                'lon': lon,
                'mapLink': mapLink,
                'rating': mostHotels[i].rating ? mostHotels[i].rating / 10 : false,
                'id': mostHotels[i].id,
                'imgW': imgW,
                'imgH': imgH,
                'photoList': this.renderPhotosList(mostHotels[i].id,mostHotels[i].photoCount > 7 ? 7 : mostHotels[i].photoCount, imgW, imgH),
                'mapW': window.isMobile ? 300 : 600,
                'mapH': window.isMobile ? 105 : 150,
                'distance': mostHotels[i].distance ? Math.round(mostHotels[i].distance * 10) / 10 : 0,
                'searchLink': 'https://search.hotellook.com/r?locationId=' + wizard.request.locationId +
                                '&hotelId=' + mostHotels[i].id +
                                '&gateId=' + mostHotels[i].gateId +
                                '&roomId=' + mostHotels[i].roomId +
                                '&checkIn=' + wizard.request.checkIn +
                                '&checkOut=' + wizard.request.checkOut +
                                '&adults=' + wizard.request.adults +
                                '&children=' + wizard.request.children +
                                '&source=' + 'wizard_hotel_page',
                'checkInHour': mostHotels[i].checkIn ? mostHotels[i].checkIn : '',
                'checkOutHour': mostHotels[i].checkOut ? mostHotels[i].checkOut : '',
                'tyReview': (mostHotels[i].trustyou && mostHotels[i].trustyou.hotelSummaryPrimary && mostHotels[i].trustyou.hotelSummaryPrimary.text) ? mostHotels[i].trustyou.hotelSummaryPrimary.text : '',
                'hotel_amenities': _.size(amenities_hotel[i]) ? amenities_hotel[i] : false,
                'room_amenities': _.size(amenities_room[i]) ? amenities_room[i] : false
            };
            hotels[i] = window.isMobile ? _.template($('#tpl_hotel').html()) : _.template($('#tpl_hotel_desktop').html());
            hotels_html += hotels[i](data[i]);
        }

        $('[data-place=hotels]').html(hotels_html);
        wizard.e.trigger ('hotelsAppended', [{cnt:_.size(mostHotels)}]);
    },

    renderPhotosList: function (hotelId, photoCount,imgW, imgH) {
        var photos = [];
        for (var i = 1; i < photoCount; i++) {
            photos.push({
                'i': i,
                'src': 'https://photo.hotellook.com/image_v2/sprite/h' + hotelId + '/' + imgW + 'x' + imgH + '/' + photoCount + '/' + imgW + 'x' + imgH + '.auto'
            });
        }
        return photos;
    },

    sortByPrice: function (rooms) {
        _.each(rooms, function(value) {
            value.sort(function (a, b) {
                var r = a.price - b.price;
                return r;
            });
        });

        return rooms;
    }
});

function loadHotels_success_callback() {}
var content_slider = Class({

    constructor: function (el) {
        this.el = el;
        this.init();
    },

    events: function () {

        $(window.searchForm.controls.destination.input).on ('focus', function () {
            _.defer(function () {
                $('html,body').scrollTop($('#destination').offset().top - 10);
            })
        });

        // slick event before change slide
        $('.content-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
            wizard.e.trigger ('slideChange', [{nextSlide: nextSlide}]);
            if (nextSlide == 7) {
                wizard.e.trigger ('nextIsFinalSlide');
            }
        });

        this.el.on('afterChange', _.bind(function(event, slick, currentSlide){
            if (currentSlide == 7) {
                wizard.e.trigger ('finalSlideLoaded');
                wizard.finalSlideLoaded = true;
            }

            if (!window.isMobile) {
                $.datepicker.setDefaults($.datepicker.regional['ru-RU']);

                switch (currentSlide - 1) {
                    case 0:
                        this.insertTextToTimline(0, window.searchForm.controls.destination.config.text.split(',')[0]);
                        break;
                    case 1:
                        this.insertTextToTimline(1,
                            $.datepicker.formatDate('d M', new Date(window.searchForm.controls.checkIn.getParams().checkIn)) +
                            '<br/>' +
                            $.datepicker.formatDate('DD', new Date(window.searchForm.controls.checkIn.getParams().checkIn))
                        );
                        break;
                    case 2:
                        this.insertTextToTimline(2,
                            $.datepicker.formatDate('d M', new Date(window.searchForm.controls.checkOut.getParams().checkOut)) +
                            '<br/>' +
                            $.datepicker.formatDate('DD', new Date(window.searchForm.controls.checkOut.getParams().checkOut))
                        );
                        break;
                    case 3:
                        var html = window.searchForm.controls.guests.config.adultsTitle(window.searchForm.controls.guests.config.adults)
                            + '<br/>'
                            + window.searchForm.controls.guests.config.childrenTitle(window.searchForm.controls.guests.config.children);
                        this.insertTextToTimline(3, html);
                        break;
                    case 4:
                        var html = 'Не выбрано';
                        if (wizard.filter.typeTrip == 1) {
                            html = 'Бизнес-трип';
                        } else if (wizard.filter.typeTrip == 2) {
                            html = 'Отдых и туризм';
                        }
                        this.insertTextToTimline(4, html);
                        break;
                    case 5:
                        var text = wizard.filter.amenities.length ? 'Выбрано ' + wizard.filter.amenities.length : 'Не выбраны';
                        this.insertTextToTimline(5, text);
                        break;
                    case 6:
                        var html = 'от ' + wizard.filter.priceRange[0] + ' руб.<br/>до ' + wizard.filter.priceRange[1] + ' руб.';
                        this.insertTextToTimline(6, html);
                        break;
                    case 7:
                        this.insertTextToTimline(7, wizard.filter.typeTrip);
                        break;
                }
            }
        }, this));

        wizard.e.on ('acSelect', _.bind(function (locationId) {
            this.slideNext();

            window.searchForm.controls.destination.input.blur();
            wizard.request.locationId = locationId;
        }, this));

        var _this = this;

        $('[data-role=city-popular]').on ('click', function () {
           var $this = $(this),
               $id = $this.data('id'),
               $text = $this.data('text');

            _this.slideNext();

            window.searchForm.controls.destination.input.val($text);
            window.searchForm.controls.destination.config.id = $id;
            window.searchForm.controls.destination.config.text = $text;
            wizard.request.locationId = $id;

            hlf.goal({
                ga: 'hl_wizard_popular_city.click',
                yam: 'hl_wizard_popular_city-click',
                as: 'hl_wizard_popular_city-click'
            },{id: $id});

            woopra.track('wizard_popular_city', {
                id: $id
            });

            return false;
        });

        wizard.e.on ('calendarCheckInSelect', _.bind(function (date) {
            wizard.request.checkIn = date;
            this.slideNext();

            setTimeout(_.bind(function () {
                $('.hasDatepicker').datepicker('refresh');
            }, this), 400);

        }, this));

        wizard.e.on ('calendarCheckOutSelect', _.bind(function (date) {
            wizard.request.checkOut = date;
            this.slideNext();

            setTimeout(_.bind(function () {
                $('.hasDatepicker').datepicker('refresh');
            }, this), 400);
        }, this));

        wizard.e.on ('timelineClick', _.bind(function (data) {
            this.slideGoTo(data.slideNext);
        }, this));

        wizard.e.on ('radioLink-Click', _.bind(function () {
            this.slideNext();
        }, this));

        wizard.e.on ('priceNextBtn-Click', _.bind(function () {
            this.slideNext();
        }, this));

        $('[data-role=guests-next-btn]').on ('click', _.bind(function() {
            wizard.request.adults = window.searchForm.controls.guests.config.adults;
            wizard.request.children = window.searchForm.controls.guests.config.children;
            this.slideNext();

            wizard.e.trigger ('guestsSelect');

            hlf.goal({
                ga: 'hl_wizard_guests_btn.click',
                yam: 'hl_wizard_guests_btn-click',
                as: 'hl_wizard_guests_btn-click'
            });

            woopra.track('hl_wizard_guests_btn_click');

            return false;
        }, this));

        $('[data-role=type-next-btn]').on ('click', _.bind(function() {
            this.slideNext();
            return false;
        }, this));

        $('[data-role=amenities-next-btn]').on ('click', _.bind(function() {
            this.slideNext();

            hlf.goal({
                ga: 'hl_wizard_amenities.choice',
                yam: 'hl_wizard_amenities-choice',
                as: 'hl_wizard_amenities-choice'
            },{amenities: wizard.filter.amenities.length ? wizard.filter.amenities : 0});

            woopra.track('wizard_amenities', {
                amenities: wizard.filter.amenities.length ? wizard.filter.amenities : 0
            });

            return false;
        }, this));

        $('[data-role=reload-btn]').on ('click', _.bind(function() {
            var $place = $(this).data('place');
            location.reload();

            hlf.goal({
                ga: 'hl_wizard_reload_btn.click',
                yam: 'hl_wizard_reload_btn-click',
                as: 'hl_wizard_reload_btn-click'
            }, {place: $place});

            woopra.track('wizard_reload_btn', {
                place: $place
            });

            return false;
        }, this));
    },

    insertTextToTimline: function (num, html) {
        $('[data-value=' + num + ']')
            .addClass ('passed')
            .find ('[data-role=tl-text]').html (html);

        setTimeout(function () {
            $('[data-value=' + num + ']').addClass ('textShow');
        }, 300);
    },

    init: function () {
        this.events();

        var slick_element = this.el;

        _.defer (function () {
            slick_element.slick({
                dots: false,
                accessibility: false,
                infinite: false,
                speed: 400,
                mobileFirst: false,
                swipe: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                touchThreshold: 8,
                cssEase: 'ease-in-out',
                touchMove: false,
                waitForAnimate: false,
                fade: false,
                arrows: false
            });
        });

    },

    slideNext: function () {
        this.el.slick('slickNext');
    },

    slideGoTo: function (slideNext) {
        this.el.slick('slickGoTo', slideNext);
    }
});
window.Sharing = {
    f: function(url) {
        socialGoal('facebook');
        return this._share("https://www.facebook.com/sharer.php?app_id=955357184504374&u=" + (encodeURIComponent(url)));
    },
    t: function(url) {
        socialGoal('twitter');
        return this._share("http://twitter.com/intent/tweet?url=" + (encodeURIComponent(url)));
    },
    v: function(url) {
        socialGoal('vkontakte');
        return this._share('http://vkontakte.ru/share.php?' + ("url=" + (encodeURIComponent(url))));
    },
    _share: function(url, callback) {
        var self;
        self = this;
        this.window = window.open(url, 'Sharing', 'width=534,height=279');
        clearInterval(this.interval);
        return this.interval = setInterval((function() {
            if (self.window.closed) {
                clearInterval(self.interval);
                if (callback) {
                    return callback();
                }
            }
        }), 500);
    }
};

function socialGoal (social) {
    hlf.goal({
        ga: 'hl_wizard_social_btn.click',
        yam: 'hl_wizard_social_btn-click',
        as: 'hl_wizard_social_btn-click'
    }, {social: social});

    woopra.track('hl_wizard_social_btn_click', {
        social: social
    });
};

$(document).ready(function () {
    $('.social a').each (function () {

        var container = $(this);

        window.socialAnimation = function(container) {
            var global_frames, run;

            global_frames = [
                {
                    elem: container.find('.b-share-icon_vkontakte'),
                    method: 'addClass',
                    value: 'animating',
                    delay: 500
                }, {
                    elem: container.find('.b-share-icon_facebook'),
                    method: 'addClass',
                    value: 'animating',
                    delay: 500
                }, {
                    elem: container.find('.b-share-icon_twitter'),
                    method: 'addClass',
                    value: 'animating',
                    delay: 500
                }, {
                    elem: container.find('.b-share-icon_vkontakte'),
                    method: 'removeClass',
                    value: 'animating',
                    delay: 500
                }, {
                    elem: container.find('.b-share-icon_facebook'),
                    method: 'removeClass',
                    value: 'animating',
                    delay: 500
                }, {
                    elem: container.find('.b-share-icon_twitter'),
                    method: 'removeClass',
                    value: 'animating',
                    delay: 500
                }
            ];
            run = function(global_frames) {
                if ($('html').hasClass('no-isdesktop')) return false;
                var frame;
                if (!(global_frames.length > 0)) {
                    return;
                }
                frame = global_frames.shift();
                frame.elem[frame.method](frame.value);
                return setTimeout((function() {
                    return run(global_frames);
                }), frame.delay);
            };
            run(global_frames);
        };
        return setInterval((function() {
            socialAnimation(container);
        }), 4000);
    });
});
var timeline = Class({
    constructor: function () {
        this.$timelineParent = $('[data-role=time-line]');
        this.init();
    },

    events: function () {
        wizard.e.on ('slideChange', _.bind(function (data) {
            var _this = this;
            _.defer(_.bind(function () {
                var $nextDot = _this.$timelineParent.find('[data-value="' + data.nextSlide + '"]');

                _this.$timelineParent.find('.timeline-item_active').removeClass('timeline-item_active');
                $nextDot.parent().addClass('timeline-item_active');
            }), this);
        }, this));

        $('[data-role=timeline-dot]').on ('click', function() {
            var $this = $(this),
                $value = $this.data('value'),
                $name = $this.data('name');

            wizard.e.trigger ('timelineClick', [{'slideNext': $value}]);

            hlf.goal({
                ga: 'hl_wizard_timeline_link.click',
                yam: 'hl_wizard_timeline_link-click',
                as: 'hl_wizard_timeline_link-click'
            }, {name: $name});

            woopra.track('wizard_timeline_link_click', {
                name: $name
            });

            return false;
        });
    },

    init: function () {
        this.events();
    }
});
window.wizard = new function () {
    this.retina = window.devicePixelRatio > 1 ? true : false;
    this.init = function () {
        this.e = new EventEmitter();
    };
    this.currentSlide = 0;
    this.request = {
        locationId: 0,
        checkIn: '',
        checkOut: '',
        adults: 2,
        children: 0,
        nights: 0
    };
    this.filter = {
        typeTrip: 0,
        amenities: [],
        priceRange:
            {
                0: 700,
                1: 200000
            },
        droped: {
            amenitiesDroped: [],
            priceDropedMax: 0,
            priceDropedMin: 0
        }
    };
    this.slideLocked = true;
    this.hotels = {};
    this.finalSlideLoaded = false;
};

$(document).ready(function () {
    $(function() {
        FastClick.attach(document.body);
    });

    window.isMobile = !device.desktop();
    wizard.init();

    FormSearch('search-form');

    new content_slider($('.content-slider'));
    new timeline();
    new progressBar();

    if (window.isMobile) {
        new ballonMob();
    } else {
        new ballon();
    }

    new radioLinks();
    new chbxLinks();
    new priceRange();
    new searchInit();
    new decorations();
});